function display(g)
%GD/DISPLAY display a gd

% Version 1.0 - July 1998
% Part of Snag toolbox - Signal and Noise for Gravitational Antennas
% Copyright (C) 1998  Sergio Frasca - sergio.frasca@roma1.infn.it
% Department of Physics - Universita` "La Sapienza" - Rome

st=sprintf('%d',g.n);
in=sprintf('%d',g.ini);
dx=sprintf('%d',g.dx);
ty=sprintf('%d',g.type);
con=' ';
if isa(g.cont,'double')
    if g.cont ~= 0
        con=sprintf(' cont= %f',g.cont);
    end
end
    
disp([' gd ',inputname(1),' -> n=',st,' ini=',in,' dx=',dx,...
   ' type=',ty,con,' -> ',g.capt])

if isstruct(g.cont)
    str=' BSD par: ';
    ic=0;
    if isfield(g.cont,'t0')
        str1=sprintf('t0 = %f ',g.cont.t0);
        str=[str str1];
        ic=ic+1;
    end
    if isfield(g.cont,'inifr')
        str1=sprintf('inifr = %f ',g.cont.inifr);
        str=[str str1];
        ic=ic+1;
    end
    if isfield(g.cont,'bandw')
        str1=sprintf('bandw = %f ',g.cont.bandw);
        str=[str str1];
        ic=ic+1;
    end
    if ic == 3
        disp(str)
    end
end