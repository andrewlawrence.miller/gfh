% snag_constant

c_=2.997924580E8
G_=6.67259E-11         % Gravitational constant N*m^2/kg^2
N_A_=6.0221367E23      % Avogadro number mol^-1
h_=6.6260755E-34       % Planck constant J*s
k_=1.380658E-23        % Boltzmann constant J/K
ev_=1.60217733E-19

ly_=9.4605E15          % light year m
parsec_=3.0856E16
au_=1.495985E11        % astronomical unit m
sid_d_=86164.09074
sid_y_=3.1558149984D7

sol_m_=1.9891E30
sol_r_=6.955E8
sol_lum_=3.846E26     % watt
sol_app_=0.5358       % mean apparent diameter; degrees

earth_m_=5.9736E24
earth_r_=6.37817E6
AU_=149597870691      % Astronomical unit
earth_aph_=152097701000
earth_perih_=147098074000

moon_m_=7.347637E22
moon_r_=1.73710E6

deg2rad_=pi/180
rad2deg_=1/deg2rad_