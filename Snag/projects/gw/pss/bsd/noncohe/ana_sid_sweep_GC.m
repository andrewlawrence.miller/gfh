function out=ana_sid_sweep_GC(fr1,bw)
% fr1 initial frequency
% bw if present, new format (typically 10, 100,...)

if exist('bw','var')
    sidsL=sprintf('sidsGC_%04d_%04d_%d_L',fr1,fr1+10,bw);
    sidsH=sprintf('sidsGC_%04d_%04d_%d_H',fr1,fr1+10,bw);
else
    sidsL=sprintf('sidsGC_%04d_%04d_L',fr1,fr1+10);
    sidsH=sprintf('sidsGC_%04d_%04d_H',fr1,fr1+10);
end

eval(['load(''' sidsL ''');'])
frL=sidsGCL.fr;
sidratL=sidsGCL.sidrat;
% [raL,rpL]=v_ranking(sidratL);
out.sidratL=sidratL;

eval(['load(''' sidsH ''');'])
frH=sidsGCH.fr;
sidratH=sidsGCH.sidrat;
% [raH,rpH]=v_ranking(sidratH);
out.sidratH=sidratH;

figure,plot(frL,sidratL),grid on,title('L')
figure,plot(frH,sidratH),grid on,title('H')
figure,plot(frL,sidratL.*sidratH),grid on,title('L*H')

mul=mean(sidratL);
muh=mean(sidratH);
mulh=mean(sidratL.*sidratH);
sigl=std(sidratL);
sigh=std(sidratH);
siglh=std(sidratL.*sidratH);
mal=max(sidratL);
mah=max(sidratH);
malh=max(sidratL.*sidratH);

fprintf(' %f - %f Hz\n',fr1,fr1+10)
fprintf(' mul,std,snr= %f  %f  %f \n',mul,sigl,mal/sigl)
fprintf(' muh,std,snr= %f  %f  %f \n',muh,sigh,mah/sigh)
fprintf(' mulh,std,snr= %f  %f  %f \n',mulh,siglh,malh/siglh)

[hiL,xhL]=hist(sidratL,500);
figure,semilogy(xhL,hiL),grid on,title('L')

[hiH,xhH]=hist(sidratH,500);
figure,semilogy(xhH,hiH),grid on,title('H')

[hiLH,xhLH]=hist(sidratL.*sidratH,500);
figure,semilogy(xhLH,hiLH),grid on,title('LH')

pL=sort_p_rank(sidratL);
pH=sort_p_rank(sidratH);
P=1./(pL.*pH);

figure,semilogy(frL,1./pL,'x'),grid on,hold on,plot(frs,1./pH,'gx'),plot(frL,P,'ro'),
title([sidsL ' - ' sidsH]),xlabel('frequency'),ylabel('Ranking')
    
out.mul=mul;