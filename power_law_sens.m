function [h0min,dmax] = power_law_sens(f0,mc,TFFT,tobs,n,run,Gamma,CR_thr,theta_thr)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
cc=constants;
if ~exist('run','var')
    run='advL';
end
if ~exist('mc','var')
    mc=1e-3;
end
if ~exist('tobs','var')
    tobs=525600*60;
end
if ~exist('Gamma','var')
    Gamma=0.9;
end
if ~exist('theta_thr','var')
    theta_thr=2.5;
end
if ~exist('CR_thr','var')
    CR_thr=5;
end

times=0:TFFT:tobs;
if n==11/3
    k=calc_k(mc);
else
    k=fdot0/f0^n;
end
pow=n-1;
const=k.*pow.*f0.^pow;
fsss=f0*(1-const.*times).^(-1./pow);

% fisco=calc_f_isco(Mc);
% 
% fsss=fsss(fsss<fisco);
% if isempty(fsss)
%     continue;
% end

[ det_freq,hnoise ] = load_sens_curve(run);


for r=1:length(fsss) % for each frequency find the index of the nearest bin of the noise curve 
    [wal index(r)]=min(abs(fsss(r)-det_freq));
end    


hnoise_band=hnoise(index);
clear index
hnoise_band=hnoise_band';
Sn=hnoise_band.*hnoise_band; % detector noise power spectrum

N=round(tobs/TFFT);
p0=exp(-theta_thr)-exp(-2*theta_thr)+1/3*exp(-3*theta_thr);             
p1=exp(-theta_thr)-2*exp(-2*theta_thr)+exp(-3*theta_thr);

if n==11/3
    Factor=sum(1./Sn.*fsss.^(4/3)); %%%%%%%%% was 4/6.. a bug!
elseif n==5
    Factor=sum(1./Sn.*fsss.^(4));
elseif n==7
    Factor=sum(1./Sn.*fsss.^(6));
end
fact1=4.02/theta_thr^(1/2);
fact2=sqrt(N/TFFT)*Factor^(-1/2);
fact3=(p0*(1-p0)/(N*p1^2))^(1/4);
fact4=sqrt(CR_thr-sqrt(2)*erfcinv(2*Gamma));

if n==11/3
    h0min=f0^(2/3)*fact1*fact2*fact3*fact4;
elseif n==5
    h0min=f0^2*fact1*fact2*fact3*fact4;
elseif n==7
    h0min=f0^3*fact1*fact2*fact3*fact4;
end
%     

dmax=calc_chirp_d(h0min,mc,f0)*cc.units.kpc_to_m;

end