function [amps, fsss] = mini_emri_fevol(sour,maxf,tt,ts,fs)
%ts,fs: loaded table of full time/frequency evolution from ~10 Hz to fisco
%%% this function extracts only the times tt and necessary frequencies
%%% tt: the analysis times, in chunks of TFFT


f0 = sour.f0;
% [~,b] = min(abs(fs-f0));
% [~,c] = min(abs(fs-maxf));

fs_full_range = fs(fs>=f0 & fs<=maxf);
ts_full_range = ts(fs>=f0 & fs<=maxf);
% fs_full_range = fs(b:c);
% ts_full_range = ts(b:c);
ts_to_query = max(ts_full_range)-tt;

fsss = interp1(ts_full_range,fs_full_range,ts_to_query);


%%%% same as non-rel case
h0=sour.h0;
amps=h0*fsss.^(2/3)/f0^(2/3);
amps=amps*1e20;

end