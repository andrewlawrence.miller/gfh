(* ::Package:: *)

(* ::Input::Initialization:: *)
SetDirectory[NotebookDirectory[]];


(* ::Section::Initialization:: *)
(*Misc*)


(* ::Subsection::Initialization::Closed:: *)
(*Plot Basics*)


(* ::Input::Initialization:: *)
myRGB[r_,g_,b_]:=RGBColor[N[r/255],N[g/255],N[b/255]];


(* ::Input::Initialization:: *)
LabelInset[label_, color_, x_, y_, fontsize_, rotate_, scale_, 
   LeftRightCenter_] := 
  Inset[Style[Rotate[Text[label], rotate], color, Italic, Bold, 
    FontSize -> fontsize], Scaled[{x, y}], LeftRightCenter, 
   scale, {1, 0}];


(* ::Input::Initialization:: *)
LabelInsetNoBKG[label_, color_, x_, y_, fontsize_, rotate_, scale_] :=
  Inset[Style[Rotate[Text[label], rotate], color, Italic, Bold, 
   FontSize -> fontsize], Scaled[{x, y}], Center, scale, {1, 0}, 
  Background -> None, BaseStyle -> {Background -> None}];


(* ::Section::Initialization:: *)
(*EMRI*)


(* ::Subsection::Initialization::Closed:: *)
(*Basics*)


(* ::Input::Initialization:: *)
$Experiments={"BBO","DECIGO","UDECIGO","ALIA","LISA-C1","LISA-C2","LISA-C3","LISA-C4"};


(* ::Input::Initialization:: *)
PlotStyleSpin={
{Red},
{Blue,Dashed},
{Green},
{Cyan,Dashed},
{Magenta},
{Green,Dashed},
{Pink},
{Brown,Dashed},
{Black},
{Gray,Dashed}
};
OneYearSI=365 24 3600;
OneMonthSI=30*24*3600;
OneDaySI=24*3600;
OneHourSI=3600.;
MsolarGram=1.9885 10^33;
(*There are some duplicate dat in eLISA, remove them before interpolation*)
DropDuplicate[exp_]:=Module[
{out,first},
first=exp[[1,1]];
out={exp[[1]]};
Do[
If[exp[[n,1]]=!=first,
AppendTo[out,exp[[n]]];
first=exp[[n,1]];
];
,
{n,2,Length[exp]}
];
Return[out]
];


(* ::Subsection::Initialization::Closed:: *)
(*g(m), Subscript[\!\(\*OverscriptBox[\(r\), \(~\)]\), isco](a),Subscript[\!\(\*OverscriptBox[\(\[CapitalOmega]\), \(~\)]\), isco](a) ,\!\(\*OverscriptBox[\(r\), \(~\)]\)(a,\!\(\*OverscriptBox[\(\[CapitalOmega]\), \(~\)]\)/Subscript[\!\(\*OverscriptBox[\(\[CapitalOmega]\), \(~\)]\), isco]), \!\(\*OverscriptBox[\(\[CapitalOmega]\), \(~\)]\) Subscript[\!\(\*OverscriptBox[\(\(/\)\(\[CapitalOmega]\)\), \(~\)]\), isco](a,\!\(\*OverscriptBox[\(r\), \(~\)]\)/Subscript[\!\(\*OverscriptBox[\(r\), \(~\)]\), isco])  *)


(* ::Input::Initialization:: *)
G=6.67384 10^-11;
c=3 10^8;
onepc=3.08567758149 10^16;
rEarthSgrA=8122onepc;
Gpc=10^9*3.08567758149 10^16;
Msolar=1.9885 10^30;
Rsolar=6.957 10^8;
g[m_]:=((m+1)(m+2)(2m+1)! m^(2m))/((m-1)(2^m m!(2m+1)!!)^2);
(*Subscript[Overscript[r, ~], isco](a)*)
rTildeISCO[a_]:=Module[
{Z1,Z2},
(*Eq.3.20*)
Z1=1+(1-a^2)^(1/3) ((1+a)^(1/3)+(1-a)^(1/3));
Z2=Sqrt[3a^2+Z1^2];
3+Z2-Sign[a]Sqrt[(3-Z1)(3+Z1+2Z2)]
];
(*input: mSMBH in Msolar*)
rSI[mSMBH_,rTilde_]:=rTilde*mSMBH*Msolar*G/c^2;
(*input: mass in Msolar*)
rTidal[mSMBH_,mBS_,cBS_]:=1/cBS (mSMBH*mBS^2)^(1/3) Msolar G/c^2;
(*r(Subscript[m, BS],Subscript[C, BS]), Subscript[m, BS] unit Msolar*)
rMC[mBS_,cBS_]:=(mBS*Msolar)/cBS G/c^2;
(*Subscript[Overscript[\[CapitalOmega], ~], isco](a)*)
OmegaTildeISCO[a_]:=1/(rTildeISCO[a]^(3/2)+a);
(*Overscript[r, ~](a,Overscript[\[CapitalOmega], ~]/Subscript[Overscript[\[CapitalOmega], ~], isco])*)
rTilde[a_,OmegaTildeOverISCO_]:=(1/(OmegaTildeOverISCO*OmegaTildeISCO[a])-a)^(2/3);
(*Overscript[\[CapitalOmega], ~]/Subscript[Overscript[\[CapitalOmega], ~], isco](a,Overscript[r, ~]/Subscript[Overscript[r, ~], isco])*)
OmegaTildeOverISCO[a_,rTildeOverrISCO_]:=With[{rtisco=rTildeISCO[a],rr=rTildeOverrISCO},(rtisco^(3/2)+a)/(rr^(3/2) rtisco^(3/2)+a)];


(* ::Input::Initialization:: *)
rTildeOverrISCOList={1,1.001,1.002,1.005,1.01,1.02,1.05,1.1,1.2,1.3,1.4,1.7,2,2.5,3,4,5,6,7,8,9,10};


(* ::Input::Initialization:: *)
FrequencyAtNISCO[a_,M_,m_][rTildeOverrISCO_]:=(m OmegaTildeOverISCO[a,rTildeOverrISCO]OmegaTildeISCO[a])/((2 \[Pi] M G)/c^3);


(* ::Subsection::Initialization::Closed:: *)
(*\!\(\*OverscriptBox[\(\[ScriptCapitalE]\), \(.\)]\)*)


(* ::Subsubsection::Initialization::Closed:: *)
(*r, a*)


(* ::Input::Initialization:: *)
MBHSpinList={-0.99,-0.9,-0.5,0,0.2,0.5,0.8,0.9,0.99,0.999};
rTildeOverISCOList={1,1.001,1.002,1.005,1.01,1.02,1.05,1.1,1.2,1.3,1.4,1.7,2,2.5,3,4,5,6,7,8,9,10};


(* ::Input::Initialization:: *)
Do[OmegaTildeMim[a]=OmegaTildeOverISCO[a,10]*OmegaTildeISCO[a],{a,MBHSpinList}];
Do[OmegaTildeOverISCOMim[a]=OmegaTildeOverISCO[a,10],{a,MBHSpinList}];


(* ::Subsubsection::Initialization::Closed:: *)
(*Table    I:  \!\(\*OverscriptBox[\(\[CapitalOmega]\), \(~\)]\)/Subscript[\!\(\*OverscriptBox[\(\[CapitalOmega]\), \(~\)]\), isco] as function of a and \!\(\*OverscriptBox[\(r\), \(~\)]\)/Subscript[\!\(\*OverscriptBox[\(r\), \(~\)]\), isco]: Agree*)


(* ::Text::Initialization:: *)
(*Table of \!\(\*OverscriptBox[\(\[CapitalOmega]\), \(~\)]\)/Subscript[\!\(\*OverscriptBox[\(\[CapitalOmega]\), \(~\)]\), isco] as function of a and \!\(\*OverscriptBox[\(r\), \(~\)]\)/Subscript[\!\(\*OverscriptBox[\(r\), \(~\)]\), isco].*)


(* ::Input::Initialization:: *)
(*Export["Figures/OmegaOverISCO.png",DataInput["\[CapitalOmega]/Subscript[\[CapitalOmega], isco]"]];*)


(* ::Input::Initialization:: *)
DataInput["\[CapitalOmega]/\!\(\*SubscriptBox[\(\[CapitalOmega]\), \(isco\)]\)"]=Module[
{rTildeOverrISCOList,aList,nRow,MainTable,FirstRow,row,NoFirstRow,Table1},
rTildeOverrISCOList={1,1.001,1.002,1.005,1.01,1.02,1.05,1.1,1.2,1.3,1.4,1.7,2,2.5,3,4,5,6,7,8,9,10};
nRow=Length[rTildeOverrISCOList];
aList={-0.99,-0.9,-0.5,0,0.2,0.5,0.8,0.9,0.99,0.999};
MainTable=Table[N@OmegaTildeOverISCO[a,rTildeOverrISCO]
,
{rTildeOverrISCO,rTildeOverrISCOList},
{a,{-0.99,-0.9,-0.5,0,0.2,0.5,0.8,0.9,0.99,0.999}}
];
FirstRow=PrependTo[aList,"r/\!\(\*SubscriptBox[\(r\), \(isco\)]\)"];(*add the (1,1) component of the table*)
NoFirstRow=
Table[
(*add for each row of MainTable the corresponding value of r/Subscript[r, isco]*)
Prepend[MainTable[[row]],rTildeOverrISCOList[[row]]]
,{row,1,nRow}
];
(*add first row*)
Table1=PrependTo[NoFirstRow,FirstRow];
Grid[Table1,Frame->All]
];


(* ::Subsubsection::Initialization::Closed:: *)
(*Table  II:  \!\(\*OverscriptBox[\(\[ScriptCapitalE]\), \(.\)]\)                as function of a and \!\(\*OverscriptBox[\(r\), \(~\)]\)/Subscript[\!\(\*OverscriptBox[\(r\), \(~\)]\), isco]*)


(* ::Input::Initialization:: *)
DataInput["d\[ScriptCapitalE]dt"]=DataInput["\!\(\*OverscriptBox[\(\[ScriptCapitalE]\), \(.\)]\)"];
DataInput["\!\(\*OverscriptBox[\(\[ScriptCapitalE]\), \(.\)]\)"]={
 {"r/\!\(\*SubscriptBox[\(r\), \(isco\)]\)", -0.99, -0.9, -0.5, 0, 0.2, 0.5, 0.8, 0.9, 0.99, 0.999},
 {1.000, 1.240, 1.233, 1.197, 1.143, 1.114, 1.053, 0.9144, 0.7895, 0.4148, 0.2022},
 {1.001, 1.239, 1.232, 1.196, 1.142, 1.114, 1.053, 0.9140, 0.7894, 0.4154, 0.2032},
 {1.002, 1.238, 1.231, 1.196, 1.141, 1.113, 1.052, 0.9137, 0.7894, 0.4160, 0.2041},
 {1.005, 1.235, 1.228, 1.193, 1.139, 1.111, 1.050, 0.9126, 0.7891, 0.4177, 0.2069},
 {1.01, 1.231, 1.224, 1.189, 1.135, 1.107, 1.047, 0.9109, 0.7887, 0.4207, 0.2116},
 {1.02, 1.222, 1.215, 1.181, 1.127, 1.100, 1.041, 0.9076, 0.7880, 0.4263, 0.2208},
 {1.05, 1.198, 1.192, 1.159, 1.108, 1.081, 1.025, 0.8988, 0.7867, 0.4434, 0.2473},
 {1.1, 1.165, 1.159, 1.128, 1.080, 1.055, 1.002, 0.8876, 0.7859, 0.4701, 0.2881},
 {1.2, 1.115, 1.110, 1.082, 1.039, 1.017, 0.9706, 0.8726, 0.7882, 0.5182, 0.3581},
 {1.3, 1.081, 1.075, 1.051, 1.012, 0.9913, 0.9493, 0.8638, 0.7920, 0.5587, 0.4160},
 {1.4, 1.055, 1.051, 1.028, 0.9919, 0.9733, 0.9348, 0.8583, 0.7960, 0.5930, 0.4648},
 {1.7, 1.011, 1.007, 0.9888, 0.9591, 0.9435, 0.9119, 0.8524, 0.8075, 0.6665, 0.5723},
 {2.0, 0.9893, 0.9862, 0.9705, 0.9448, 0.9312, 0.9034, 0.8530, 0.8171, 0.7117, 0.6411},
 {2.5, 0.9734, 0.9709, 0.9580, 0.9363, 0.9248, 0.9012, 0.8589, 0.8302, 0.7556, 0.7089},
 {3, 0.9674, 0.9653, 0.9542, 0.9352, 0.9250, 0.9040, 0.8662, 0.8415, 0.7813, 0.7469},
 {4, 0.9651, 0.9634, 0.9546, 0.9391, 0.9306, 0.9129, 0.8807, 0.8597, 0.8121, 0.7882},
 {5, 0.9665, 0.9651, 0.9577, 0.9448, 0.9371, 0.9216, 0.8930, 0.8742, 0.8320, 0.8118},
 {6, 0.9687, 0.9675, 0.9611, 0.9490, 0.9430, 0.9291, 0.9031, 0.8858, 0.8469, 0.8286},
 {7, 0.9709, 0.9699, 0.9641, 0.9533, 0.9480, 0.9354, 0.9116, 0.8955, 0.8589, 0.8416},
 {8, 0.9730, 0.9720, 0.9669, 0.9588, 0.9522, 0.9407, 0.9186, 0.9036, 0.8689, 0.8524},
 {9, 0.9749, 0.9740, 0.9693, 0.9607, 0.9558, 0.9452, 0.9246, 0.9105, 0.8774, 0.8616},
 {10, 0.9765, 0.9757, 0.9714, 0.9616, 0.9589, 0.9491, 0.9298, 0.9164, 0.8847, 0.8695}
};
(*
saved copy in case above one is modified incidentally;
DataInput["Overscript[\[Epsilon], .]"]="r/Subscript[r, isco]"	-0.99	-0.9	-0.5	0	0.2	0.5	0.8	0.9	0.99	0.999
1.000	1.240	1.233	1.197	1.143	1.114	1.053	0.9144	0.7895	0.4148	0.2022
1.001	1.239	1.232	1.196	1.142	1.114	1.053	0.9140	0.7894	0.4154	0.2032
1.002	1.238	1.231	1.196	1.141	1.113	1.052	0.9137	0.7894	0.4160	0.2041
1.005	1.235	1.228	1.193	1.139	1.111	1.050	0.9126	0.7891	0.4177	0.2069
1.01	1.231	1.224	1.189	1.135	1.107	1.047	0.9109	0.7887	0.4207	0.2116
1.02	1.222	1.215	1.181	1.127	1.100	1.041	0.9076	0.7880	0.4263	0.2208
1.05	1.198	1.192	1.159	1.108	1.081	1.025	0.8988	0.7867	0.4434	0.2473
1.1	1.165	1.159	1.128	1.080	1.055	1.002	0.8876	0.7859	0.4701	0.2881
1.2	1.115	1.110	1.082	1.039	1.017	0.9706	0.8726	0.7882	0.5182	0.3581
1.3	1.081	1.075	1.051	1.012	0.9913	0.9493	0.8638	0.7920	0.5587	0.4160
1.4	1.055	1.051	1.028	0.9919	0.9733	0.9348	0.8583	0.7960	0.5930	0.4648
1.7	1.011	1.007	0.9888	0.9591	0.9435	0.9119	0.8524	0.8075	0.6665	0.5723
2.0	0.9893	0.9862	0.9705	0.9448	0.9312	0.9034	0.8530	0.8171	0.7117	0.6411
2.5	0.9734	0.9709	0.9580	0.9363	0.9248	0.9012	0.8589	0.8302	0.7556	0.7089
3	0.9674	0.9653	0.9542	0.9352	0.9250	0.9040	0.8662	0.8415	0.7813	0.7469
4	0.9651	0.9634	0.9546	0.9391	0.9306	0.9129	0.8807	0.8597	0.8121	0.7882
5	0.9665	0.9651	0.9577	0.9448	0.9371	0.9216	0.8930	0.8742	0.8320	0.8118
6	0.9687	0.9675	0.9611	0.9490	0.9430	0.9291	0.9031	0.8858	0.8469	0.8286
7	0.9709	0.9699	0.9641	0.9533	0.9480	0.9354	0.9116	0.8955	0.8589	0.8416
8	0.9730	0.9720	0.9669	0.9588	0.9522	0.9407	0.9186	0.9036	0.8689	0.8524
9	0.9749	0.9740	0.9693	0.9607	0.9558	0.9452	0.9246	0.9105	0.8774	0.8616
10	0.9765	0.9757	0.9714	0.9616	0.9589	0.9491	0.9298	0.9164	0.8847	0.8695

;
*)


(* ::Subsubsection::Initialization::Closed:: *)
(*Table III:  Subscript[\!\(\*OverscriptBox[\(\[ScriptCapitalE]\), \(.\)]\), \[Infinity]1]           as function of a and \!\(\*OverscriptBox[\(r\), \(~\)]\)/Subscript[\!\(\*OverscriptBox[\(r\), \(~\)]\), isco]*)


(* ::Input::Initialization:: *)
DataInput["d\[ScriptCapitalE]dt\[Infinity]1"]=DataInput["\!\(\*SubscriptBox[OverscriptBox[\(\[Epsilon]\), \(.\)], \(\[Infinity]1\)]\)"];
DataInput["\!\(\*SubscriptBox[OverscriptBox[\(\[Epsilon]\), \(.\)], \(\[Infinity]1\)]\)"]={
 {"r/\!\(\*SubscriptBox[\(r\), \(isco\)]\)", -0.99, -0.9, -0.5, 0, 0.2, 0.5, 0.8, 0.9, 0.99, 0.999},
 {1.000, 3.013, 2.854, 2.157, 1.320, 1.002, 0.5530, 0.1669, 0.06573, 0.002762, 1.071 10^-4},
 {1.001, 3.010, 2.851, 2.156, 1.319, 1.001, 0.5529, 0.1670, 0.06584, 0.002783, 1.095 10^-4},
 {1.002, 3.007, 2.849, 2.154, 1.318, 1.001, 0.5528, 0.1671, 0.06595, 0.002805, 1.119 10^-4},
 {1.005, 2.998, 2.840, 2.148, 1.316, 0.9990, 0.5525, 0.1675, 0.06628, 0.002869, 1.194 10^-4},
 {1.01, 2.984, 2.827, 2.139, 1.312, 0.9964, 0.5520, 0.1680, 0.06683, 0.002979, 1.326 10^-4},
 {1.02, 2.955, 2.800, 2.121, 1.303, 0.9915, 0.5510, 0.1692, 0.06793, 0.003204, 1.619 10^-4},
 {1.05, 2.876, 2.727, 2.071, 1.280, 0.9779, 0.5487, 0.1729, 0.07126, 0.003934, 2.747 10^-4},
 {1.1, 2.760, 2.619, 1.999, 1.248, 0.9588, 0.5462, 0.1790, 0.07688, 0.005322, 5.565 10^-4},
 {1.2, 2.575, 2.448, 1.885, 1.198, 0.9305, 0.5448, 0.1916, 0.08821, 0.008679, 0.001525},
 {1.3, 2.434, 2.316, 1.798, 1.161, 0.9111, 0.5465, 0.2040, 0.09951, 0.01272, 0.003073},
 {1.4, 2.321, 2.213, 1.730, 1.133, 0.8973, 0.5498, 0.2162, 0.1107, 0.01733, 0.005180},
 {1.7, 2.085, 1.994, 1.588, 1.077, 0.8719, 0.5623, 0.2506, 0.1421, 0.03343, 0.01435},
 {2.0, 1.940, 1.860, 1.503, 1.049, 0.8631, 0.5786, 0.2799, 0.1715, 0.05138, 0.02648},
 {2.5, 1.787, 1.719, 1.416, 1.023, 0.8599, 0.6045, 0.3237, 0.2151, 0.08223, 0.04991},
 {3, 1.689, 1.629, 1.361, 1.010, 0.8621, 0.6272, 0.3606, 0.2527, 0.1122, 0.07456},
 {4, 1.567, 1.518, 1.295, 0.9987, 0.8704, 0.6638, 0.4191, 0.3143, 0.1664, 0.1222},
 {5, 1.493, 1.450, 1.255, 0.9921, 0.8789, 0.6918, 0.4638, 0.3627, 0.2128, 0.1650},
 {6, 1.441, 1.403, 1.228, 0.9923, 0.8865, 0.7139, 0.4994, 0.4020, 0.2525, 0.2028},
 {7, 1.403, 1.368, 1.208, 0.9865, 0.8930, 0.7319, 0.5287, 0.4346, 0.2869, 0.2362},
 {8, 1.373, 1.341, 1.193, 0.9829, 0.8987, 0.7469, 0.5533, 0.4624, 0.3168, 0.2657},
 {9, 1.349, 1.319, 1.180, 0.9887, 0.9035, 0.7596, 0.5743, 0.4863, 0.3433, 0.2922},
 {10, 1.329, 1.301, 1.170, 1.005, 0.9078, 0.7706, 0.5925, 0.5072, 0.3669, 0.3159}
};
(*
DataInput["Subscript[Overscript[\[Epsilon], .], \[Infinity]1]"]="r/Subscript[r, isco]"	-0.99	-0.9	-0.5	0	0.2	0.5	0.8	0.9	0.99	0.999
1.000	3.013	2.854	2.157	1.320	1.002	0.5530	0.1669	0.06573	0.002762	1.071 10^-4
1.001	3.010	2.851	2.156	1.319	1.001	0.5529	0.1670	0.06584	0.002783	1.095 10^-4
1.002	3.007	2.849	2.154	1.318	1.001	0.5528	0.1671	0.06595	0.002805	1.119 10^-4
1.005	2.998	2.840	2.148	1.316	0.9990	0.5525	0.1675	0.06628	0.002869	1.194 10^-4
1.01	2.984	2.827	2.139	1.312	0.9964	0.5520	0.1680	0.06683	0.002979	1.326 10^-4
1.02	2.955	2.800	2.121	1.303	0.9915	0.5510	0.1692	0.06793	0.003204	1.619 10^-4
1.05	2.876	2.727	2.071	1.280	0.9779	0.5487	0.1729	0.07126	0.003934	2.747 10^-4
1.1	2.760	2.619	1.999	1.248	0.9588	0.5462	0.1790	0.07688	0.005322	5.565 10^-4
1.2	2.575	2.448	1.885	1.198	0.9305	0.5448	0.1916	0.08821	0.008679	0.001525
1.3	2.434	2.316	1.798	1.161	0.9111	0.5465	0.2040	0.09951	0.01272	0.003073
1.4	2.321	2.213	1.730	1.133	0.8973	0.5498	0.2162	0.1107	0.01733	0.005180
1.7	2.085	1.994	1.588	1.077	0.8719	0.5623	0.2506	0.1421	0.03343	0.01435
2.0	1.940	1.860	1.503	1.049	0.8631	0.5786	0.2799	0.1715	0.05138	0.02648
2.5	1.787	1.719	1.416	1.023	0.8599	0.6045	0.3237	0.2151	0.08223	0.04991
3	1.689	1.629	1.361	1.010	0.8621	0.6272	0.3606	0.2527	0.1122	0.07456
4	1.567	1.518	1.295	0.9987	0.8704	0.6638	0.4191	0.3143	0.1664	0.1222
5	1.493	1.450	1.255	0.9921	0.8789	0.6918	0.4638	0.3627	0.2128	0.1650
6	1.441	1.403	1.228	0.9923	0.8865	0.7139	0.4994	0.4020	0.2525	0.2028
7	1.403	1.368	1.208	0.9865	0.8930	0.7319	0.5287	0.4346	0.2869	0.2362
8	1.373	1.341	1.193	0.9829	0.8987	0.7469	0.5533	0.4624	0.3168	0.2657
9	1.349	1.319	1.180	0.9887	0.9035	0.7596	0.5743	0.4863	0.3433	0.2922
10	1.329	1.301	1.170	1.005	0.9078	0.7706	0.5925	0.5072	0.3669	0.3159

;
*)


(* ::Subsubsection::Initialization::Closed:: *)
(*Table IV:  Subscript[\!\(\*OverscriptBox[\(\[ScriptCapitalE]\), \(.\)]\), \[Infinity]2]           as function of a and \!\(\*OverscriptBox[\(r\), \(~\)]\)/Subscript[\!\(\*OverscriptBox[\(r\), \(~\)]\), isco]*)


(* ::Input::Initialization:: *)
DataInput["d\[ScriptCapitalE]dt\[Infinity]2"]=DataInput["\!\(\*SubscriptBox[OverscriptBox[\(\[Epsilon]\), \(.\)], \(\[Infinity]2\)]\)"];
DataInput["\!\(\*SubscriptBox[OverscriptBox[\(\[Epsilon]\), \(.\)], \(\[Infinity]2\)]\)"]={
 {"r/\!\(\*SubscriptBox[\(r\), \(isco\)]\)", -0.99, -0.9, -0.5, 0, 0.2, 0.5, 0.8, 0.9, 0.99, 0.999},
 {1.000, 1.029, 1.020, 0.9734, 0.8957, 0.8535, 0.7653, 0.5914, 0.4617, 0.1656, 0.06128},
 {1.001, 1.028, 1.019, 0.9730, 0.8954, 0.8533, 0.7652, 0.5915, 0.4620, 0.1661, 0.06170},
 {1.002, 1.028, 1.019, 0.9726, 0.8950, 0.8530, 0.7650, 0.5916, 0.4624, 0.1666, 0.06212},
 {1.005, 1.026, 1.017, 0.9713, 0.8940, 0.8522, 0.7645, 0.5919, 0.4633, 0.1681, 0.06338},
 {1.01, 1.024, 1.015, 0.9693, 0.8925, 0.8508, 0.7638, 0.5925, 0.4649, 0.1707, 0.0655},
 {1.02, 1.019, 1.011, 0.9654, 0.8894, 0.8483, 0.7623, 0.5937, 0.4680, 0.1758, 0.06975},
 {1.05, 1.007, 0.9985, 0.9548, 0.8813, 0.8415, 0.7587, 0.5974, 0.4773, 0.1909, 0.08241},
 {1.1, 0.9900, 0.9818, 0.9403, 0.8704, 0.8327, 0.7545, 0.6037, 0.4918, 0.2154, 0.1038},
 {1.2, 0.9648, 0.9574, 0.9196, 0.8558, 0.8214, 0.7506, 0.6165, 0.5178, 0.2618, 0.1465},
 {1.3, 0.9480, 0.9411, 0.9063, 0.8474, 0.8156, 0.7504, 0.6287, 0.5402, 0.3039, 0.1881},
 {1.4, 0.9364, 0.9301, 0.8977, 0.8427, 0.8131, 0.7523, 0.6400, 0.5597, 0.3417, 0.2277},
 {1.7, 0.9191, 0.9138, 0.8867, 0.8403, 0.8151, 0.7635, 0.6697, 0.6050, 0.4309, 0.3303},
 {2.0, 0.9138, 0.9092, 0.8857, 0.8450, 0.8227, 0.7769, 0.6941, 0.6382, 0.4930, 0.4077},
 {2.5, 0.9144, 0.9106, 0.8910, 0.8566, 0.8377, 0.7983, 0.7268, 0.6791, 0.5617, 0.4954},
 {3, 0.9187, 0.9154, 0.8984, 0.8684, 0.8517, 0.8167, 0.7525, 0.7097, 0.6075, 0.5524},
 {4, 0.9286, 0.9260, 0.9125, 0.8882, 0.8745, 0.8453, 0.7908, 0.7542, 0.6680, 0.6241},
 {5, 0.9372, 0.9351, 0.9237, 0.9034, 0.8914, 0.8662, 0.8183, 0.7857, 0.7085, 0.6699},
 {6, 0.9442, 0.9424, 0.9326, 0.9142, 0.9043, 0.8821, 0.8391, 0.8095, 0.7387, 0.7033},
 {7, 0.9499, 0.9483, 0.9396, 0.9232, 0.9145, 0.8945, 0.8554, 0.8282, 0.7625, 0.7295},
 {8, 0.9545, 0.9531, 0.9453, 0.9323, 0.9226, 0.9044, 0.8686, 0.8434, 0.7819, 0.7507},
 {9, 0.9584, 0.9571, 0.9500, 0.9369, 0.9293, 0.9126, 0.8795, 0.8560, 0.7981, 0.7685},
 {10, 0.9616, 0.9604, 0.9540, 0.9400, 0.9349, 0.9195, 0.8887, 0.8666, 0.8119, 0.7837}
};


(* ::Subsubsection::Initialization::Closed:: *)
(*Table  V:  Subscript[\!\(\*OverscriptBox[\(\[ScriptCapitalE]\), \(.\)]\), \[Infinity]3]           as function of a and \!\(\*OverscriptBox[\(r\), \(~\)]\)/Subscript[\!\(\*OverscriptBox[\(r\), \(~\)]\), isco]*)


(* ::Input::Initialization:: *)
DataInput["d\[ScriptCapitalE]dt\[Infinity]3"]=DataInput["\!\(\*SubscriptBox[OverscriptBox[\(\[Epsilon]\), \(.\)], \(\[Infinity]3\)]\)"];
DataInput["\!\(\*SubscriptBox[OverscriptBox[\(\[Epsilon]\), \(.\)], \(\[Infinity]3\)]\)"]={
 {"r/\!\(\*SubscriptBox[\(r\), \(isco\)]\)", -0.99, -0.9, -0.5, 0, 0.2, 0.5, 0.8, 0.9, 0.99, 0.999},
 {1.000, 0.9753, 0.9614, 0.8926, 0.7848, 0.7309, 0.6292, 0.4684, 0.3712, 0.1573, 0.06456},
 {1.001, 0.9748, 0.9608, 0.8922, 0.7845, 0.7307, 0.6291, 0.4685, 0.3714, 0.1577, 0.06495},
 {1.002, 0.9742, 0.9603, 0.8917, 0.7842, 0.7304, 0.6289, 0.4685, 0.3715, 0.1581, 0.06534},
 {1.005, 0.9726, 0.9587, 0.8904, 0.7832, 0.7296, 0.6285, 0.4685, 0.3719, 0.1592, 0.06651},
 {1.01, 0.9699, 0.9561, 0.8882, 0.7817, 0.7284, 0.6277, 0.4686, 0.3725, 0.1610, 0.06846},
 {1.02, 0.9648, 0.9512, 0.8841, 0.7787, 0.7260, 0.6263, 0.4688, 0.3739, 0.1646, 0.07237},
 {1.05, 0.9507, 0.9376, 0.8729, 0.7708, 0.7197, 0.6229, 0.4699, 0.3780, 0.1751, 0.08391},
 {1.1, 0.9313, 0.9189, 0.8576, 0.7606, 0.7118, 0.6191, 0.4728, 0.3852, 0.1919, 0.1026},
 {1.2, 0.9033, 0.8921, 0.8365, 0.7476, 0.7026, 0.6166, 0.4806, 0.3998, 0.2223, 0.1374},
 {1.3, 0.8849, 0.8747, 0.8235, 0.7411, 0.6990, 0.6183, 0.4900, 0.4141, 0.2489, 0.1686},
 {1.4, 0.8728, 0.8633, 0.8157, 0.7385, 0.6988, 0.6223, 0.5000, 0.4278, 0.2722, 0.1963},
 {1.7, 0.8562, 0.8483, 0.8085, 0.7424, 0.7078, 0.6402, 0.5301, 0.4649, 0.3280, 0.2627},
 {2.0, 0.8534, 0.8466, 0.8117, 0.7531, 0.7221, 0.6603, 0.5580, 0.4969, 0.3701, 0.3114},
 {2.5, 0.8588, 0.8531, 0.8239, 0.7737, 0.7466, 0.6919, 0.5986, 0.5417, 0.4238, 0.3710},
 {3, 0.8676, 0.8627, 0.8373, 0.7929, 0.7687, 0.7190, 0.6324, 0.5785, 0.4657, 0.4156},
 {4, 0.8851, 0.8811, 0.8606, 0.8243, 0.8040, 0.7616, 0.6850, 0.6358, 0.5297, 0.4822},
 {5, 0.8993, 0.8961, 0.8788, 0.8477, 0.8301, 0.7929, 0.7240, 0.6786, 0.5780, 0.5320},
 {6, 0.9106, 0.9078, 0.8928, 0.8658, 0.8500, 0.8168, 0.7541, 0.7119, 0.6164, 0.5717},
 {7, 0.9198, 0.9172, 0.9039, 0.8787, 0.8657, 0.8356, 0.7780, 0.7387, 0.6478, 0.6045},
 {8, 0.9272, 0.9249, 0.9130, 0.8902, 0.8783, 0.8508, 0.7975, 0.7607, 0.6741, 0.6322},
 {9, 0.9334, 0.9313, 0.9204, 0.9004, 0.8887, 0.8633, 0.8137, 0.7791, 0.6965, 0.6559},
 {10, 0.9386, 0.9367, 0.9267, 0.9087, 0.8974, 0.8739, 0.8275, 0.7948, 0.7158, 0.6766}
};


(* ::Subsubsection::Initialization::Closed:: *)
(*Table VI:  Subscript[\!\(\*OverscriptBox[\(\[ScriptCapitalE]\), \(.\)]\), \[Infinity]4]           as function of a and \!\(\*OverscriptBox[\(r\), \(~\)]\)/Subscript[\!\(\*OverscriptBox[\(r\), \(~\)]\), isco]*)


(* ::Input::Initialization:: *)
DataInput["d\[ScriptCapitalE]dt\[Infinity]4"]=DataInput["\!\(\*SubscriptBox[OverscriptBox[\(\[Epsilon]\), \(.\)], \(\[Infinity]4\)]\)"];
DataInput["\!\(\*SubscriptBox[OverscriptBox[\(\[Epsilon]\), \(.\)], \(\[Infinity]4\)]\)"]={
 {"r/\!\(\*SubscriptBox[\(r\), \(isco\)]\)", -0.99, -0.9, -0.5, 0, 0.2, 0.5, 0.8, 0.9, 0.99, 0.999},
 {1.000, 0.9393, 0.9209, 0.8319, 0.6981, 0.6342, 0.5196, 0.3574, 0.2720, 0.1116, 0.04673},
 {1.001, 0.9387, 0.9203, 0.8314, 0.6978, 0.6339, 0.5194, 0.3574, 0.2721, 0.1118, 0.04699},
 {1.002, 0.9380, 0.9197, 0.8309, 0.6975, 0.6337, 0.5193, 0.3575, 0.2722, 0.1120, 0.04726},
 {1.005, 0.9361, 0.9179, 0.8295, 0.6965, 0.6329, 0.5189, 0.3575, 0.2725, 0.1127, 0.04807},
 {1.01, 0.9330, 0.9149, 0.8271, 0.6949, 0.6317, 0.5183, 0.3576, 0.2730, 0.1139, 0.04940},
 {1.02, 0.9270, 0.9091, 0.8225, 0.6920, 0.6295, 0.5171, 0.3578, 0.2739, 0.1163, 0.05207},
 {1.05, 0.9106, 0.8935, 0.8102, 0.6841, 0.6235, 0.5143, 0.3589, 0.2770, 0.1231, 0.05993},
 {1.1, 0.8881, 0.8720, 0.7936, 0.6740, 0.6162, 0.5115, 0.3617, 0.2826, 0.1342, 0.07255},
 {1.2, 0.8560, 0.8416, 0.7709, 0.6618, 0.6084, 0.5109, 0.3697, 0.2947, 0.1547, 0.09588},
 {1.3, 0.8353, 0.8221, 0.7573, 0.6563, 0.6064, 0.5143, 0.3793, 0.3073, 0.1731, 0.1168},
 {1.4, 0.8218, 0.8097, 0.7496, 0.6549, 0.6077, 0.5199, 0.3897, 0.3198, 0.1898, 0.1355},
 {1.7, 0.8035, 0.7943, 0.7440, 0.6627, 0.6212, 0.5424, 0.4217, 0.3555, 0.2325, 0.1821},
 {2.0, 0.8021, 0.7934, 0.7493, 0.6774, 0.6398, 0.5669, 0.4523, 0.3880, 0.2676, 0.2190},
 {2.5, 0.8109, 0.8036, 0.7664, 0.7037, 0.6710, 0.6052, 0.4977, 0.4355, 0.3163, 0.2683},
 {3, 0.8232, 0.8169, 0.7843, 0.7286, 0.6984, 0.6383, 0.5365, 0.4759, 0.3570, 0.3086},
 {4, 0.8466, 0.8416, 0.8151, 0.7687, 0.7432, 0.6905, 0.5983, 0.5409, 0.4233, 0.3737},
 {5, 0.8656, 0.8614, 0.8389, 0.7987, 0.7765, 0.7297, 0.6450, 0.5910, 0.4758, 0.4257},
 {6, 0.8807, 0.8770, 0.8574, 0.8227, 0.8021, 0.7599, 0.6818, 0.6306, 0.5190, 0.4689},
 {7, 0.8928, 0.8895, 0.8720, 0.8412, 0.8224, 0.7838, 0.7115, 0.6631, 0.5551, 0.5055},
 {8, 0.9026, 0.8997, 0.8839, 0.8546, 0.8388, 0.8033, 0.7359, 0.6902, 0.5859, 0.5371},
 {9, 0.9108, 0.9081, 0.8938, 0.8673, 0.8523, 0.8195, 0.7563, 0.7130, 0.6125, 0.5647},
 {10, 0.9177, 0.9152, 0.9020, 0.8759, 0.8637, 0.8331, 0.7737, 0.7326, 0.6355, 0.5889}
};


(* ::Subsubsection::Initialization::Closed:: *)
(*Defined\[ScriptCapitalE]dt\[Infinity]*)


(* ::Input::Initialization:: *)
(*read table DataInput["d\[ScriptCapitalE]dt\[Infinity]m"] and define d\[ScriptCapitalE]dt\[Infinity][m,a][Overscript[\[CapitalOmega], ~]/Subscript[Overscript[\[CapitalOmega], ~], isco]], m="All" or 1,2,3,4*)
(*rTildeOverrISCOList={-0.99,-0.9,-0.5,0,0.2,0.5,0.8,0.9,0.99,0.999};*)
Defined\[ScriptCapitalE]dt\[Infinity][m_]:=Module[
{TableTag,table,col,data},
Do[
(*fetch data table DataInput["d\[ScriptCapitalE]dt\[Infinity]m"]*)
TableTag="d\[ScriptCapitalE]dt\[Infinity]"<>ToString[m];
(*fetch data table DataInput["d\[ScriptCapitalE]dt"]*)
If[m==="All",TableTag="d\[ScriptCapitalE]dt"];
(*\[ScriptCapitalE] data table*)
table=DataInput[TableTag];
(*column for spin a*)
col=Position[MBHSpinList,a][[1,1]]+1;
(*{{Overscript[r, ~]/Subscript[Overscript[r, ~], isco],\[ScriptCapitalE]},...}*)
data["\!\(\*OverscriptBox[\(r\), \(~\)]\)/\!\(\*SubscriptBox[OverscriptBox[\(r\), \(~\)], \(isco\)]\)-\[ScriptCapitalE]"]=Table[{rTildeOverrISCOList[[n]],table[[n+1,col]]},{n,Length[rTildeOverrISCOList]}];
(*{{Overscript[\[CapitalOmega], ~]/Subscript[Overscript[\[CapitalOmega], ~], isco],\[ScriptCapitalE]},...}*)
data["\!\(\*OverscriptBox[\(\[CapitalOmega]\), \(~\)]\)/\!\(\*SubscriptBox[OverscriptBox[\(\[CapitalOmega]\), \(~\)], \(isco\)]\)-\[ScriptCapitalE]"]=Table[
{OmegaTildeOverISCO[a,data["\!\(\*OverscriptBox[\(r\), \(~\)]\)/\!\(\*SubscriptBox[OverscriptBox[\(r\), \(~\)], \(isco\)]\)-\[ScriptCapitalE]"][[n,1]]],data["\!\(\*OverscriptBox[\(r\), \(~\)]\)/\!\(\*SubscriptBox[OverscriptBox[\(r\), \(~\)], \(isco\)]\)-\[ScriptCapitalE]"][[n,2]]},{n,Length[rTildeOverrISCOList]}
];
(*xx=data["Overscript[\[CapitalOmega], ~]/Subscript[Overscript[\[CapitalOmega], ~], isco]-\[ScriptCapitalE]"];*)
(*usage: d\[ScriptCapitalE]dt\[Infinity][m,a][Overscript[\[CapitalOmega], ~]/Subscript[Overscript[\[CapitalOmega], ~], isco]]*)
If[m==="All",d\[ScriptCapitalE]dt,d\[ScriptCapitalE]dt\[Infinity]][m,a]=Interpolation[data["\!\(\*OverscriptBox[\(\[CapitalOmega]\), \(~\)]\)/\!\(\*SubscriptBox[OverscriptBox[\(\[CapitalOmega]\), \(~\)], \(isco\)]\)-\[ScriptCapitalE]"]];
,
{a,MBHSpinList}
]
];
Defined\[ScriptCapitalE]dt\[Infinity]/@{1,2,3,4,"All"};


(* ::Subsection::Initialization::Closed:: *)
(*\[ScriptCapitalN]*)


(* ::Input::Initialization:: *)
(*\[ScriptCapitalN](a,Overscript[\[CapitalOmega], ~]/Subscript[Overscript[\[CapitalOmega], ~], isco])*)
\[ScriptCapitalN][a_,OmegaTildeOverISCO_]:=With[{rt=rTilde[a,OmegaTildeOverISCO]},1/d\[ScriptCapitalE]dt["All",a][OmegaTildeOverISCO] (1+a/rt^(3/2))^(5/3) (1-6/rt+(8a)/rt^(3/2)-(3a^2)/rt^2)(1-3/rt+(2a)/rt^(3/2))^(-3/2)];
(*\[ScriptCapitalN]r(a,Overscript[r, ~]/Subscript[Overscript[r, ~], isco])*)
\[ScriptCapitalN]r[a_,rTildeOverISCO_]:=\[ScriptCapitalN][a,OmegaTildeOverISCO[a,rTildeOverISCO]];


(* ::Text::Initialization:: *)
(*Table of \[ScriptCapitalN] as function of a and \!\(\*OverscriptBox[\(r\), \(~\)]\)/Subscript[\!\(\*OverscriptBox[\(r\), \(~\)]\), isco]=r/Subscript[r, isco]. Agree with Table VIII.*)


(* ::Input::Initialization:: *)
(*Export["Figures/N-rrisco.png",DataInput["\[ScriptCapitalN]-r/Subscript[r, isco]"]];*)


(* ::Input::Initialization:: *)
DataInput["\[ScriptCapitalN]-r/\!\(\*SubscriptBox[\(r\), \(isco\)]\)"]=Module[
{aList,nRow,MainTable,FirstRow,row,NoFirstRow,Table1},
nRow=Length[rTildeOverrISCOList];
aList=MBHSpinList;
MainTable=Table[N@\[ScriptCapitalN]r[a,rTildeOverISCO],{rTildeOverISCO,rTildeOverrISCOList},{a,aList}];
FirstRow=PrependTo[aList,"r/\!\(\*SubscriptBox[\(r\), \(isco\)]\)"];(*add the (1,1) component of the table*)
NoFirstRow=
Table[
(*add for each row of MainTable the corresponding value of r/Subscript[r, isco]*)
Prepend[MainTable[[row]],rTildeOverISCOList[[row]]]
,{row,1,nRow}
];
(*add first row*)
Table1=PrependTo[NoFirstRow,FirstRow];
Grid[Table1,Frame->All]
];


(* ::Subsection::Initialization::Closed:: *)
(*\[ScriptCapitalT](a,\!\(\*OverscriptBox[\(r\), \(~\)]\)/Subscript[\!\(\*OverscriptBox[\(r\), \(~\)]\), isco]), Subscript[\[ScriptCapitalN], orb(a,\!\(\*OverscriptBox[\(r\), \(~\)]\)/Subscript[\!\(\*OverscriptBox[\(r\), \(~\)]\), isco])], TimeToISCO*)


(* ::Input::Initialization:: *)
(*\[ScriptCapitalT](a,Overscript[r, ~]/Subscript[Overscript[r, ~], isco])*)
\[ScriptCapitalT][a_,rTildeOverISCO_]:=Module[
{\[CapitalOmega]t,\[CapitalOmega]tISCO,\[CapitalOmega]tOverISCO},
\[CapitalOmega]tISCO=OmegaTildeISCO[a];
\[CapitalOmega]tOverISCO=OmegaTildeOverISCO[a,rTildeOverISCO];
\[CapitalOmega]t=\[CapitalOmega]tISCO*\[CapitalOmega]tOverISCO;
8/3 \[CapitalOmega]t^(8/3) NIntegrate[\[ScriptCapitalN][a,x\[CapitalOmega]t/\[CapitalOmega]tISCO]/x\[CapitalOmega]t^(11/3),{x\[CapitalOmega]t,\[CapitalOmega]t,\[CapitalOmega]tISCO}]
];
(*Unit of MSMBHSolar: SI*)
TimeToISCO[MSMBH_,\[Eta]_,a_,rTildeOverISCO_]:=Module[
{\[CapitalOmega]t,\[CapitalOmega]tISCO,\[CapitalOmega]tOverISCO},
\[CapitalOmega]tISCO=OmegaTildeISCO[a];
\[CapitalOmega]tOverISCO=OmegaTildeOverISCO[a,rTildeOverISCO];
\[CapitalOmega]t=\[CapitalOmega]tISCO*\[CapitalOmega]tOverISCO;
5/256 1/\[Eta] MSMBH/\[CapitalOmega]t^(8/3) G*c^-2*c^-1 \[ScriptCapitalT][a,rTildeOverISCO]
];
(*New at Mini-EMRI*)
TimeToISCOFromFreq[MSMBH_,\[Eta]_,a_,m_,f_]:=Module[
{\[CapitalOmega]t},
If[f>fISCO[a,m,MSMBH],Return[0.]];
\[CapitalOmega]t=(2\[Pi])/m*MSMBH*f*(G*c^-2)*c^-1;
TimeToISCO[MSMBH,\[Eta],a,rTilde[a,\[CapitalOmega]t/OmegaTildeISCO[a]]/rTildeISCO[a]]
];
(*\[ScriptCapitalN](a,Overscript[r, ~]/Subscript[Overscript[r, ~], isco])*)
\[ScriptCapitalN]orb[a_,rTildeOverISCO_]:=Module[
{\[CapitalOmega]t,\[CapitalOmega]tISCO,\[CapitalOmega]tOverISCO},
\[CapitalOmega]tISCO=OmegaTildeISCO[a];
\[CapitalOmega]tOverISCO=OmegaTildeOverISCO[a,rTildeOverISCO];
\[CapitalOmega]t=\[CapitalOmega]tISCO*\[CapitalOmega]tOverISCO;

5/3 \[CapitalOmega]t^(5/3) NIntegrate[\[ScriptCapitalN][a,x\[CapitalOmega]t/\[CapitalOmega]tISCO]/x\[CapitalOmega]t^(8/3),{x\[CapitalOmega]t,\[CapitalOmega]t,\[CapitalOmega]tISCO}]
];
Norb[a_,rTildeOverISCO_,\[Eta]_]:=Module[
{\[CapitalOmega]t,\[CapitalOmega]tISCO,\[CapitalOmega]tOverISCO},
\[CapitalOmega]tISCO=OmegaTildeISCO[a];
\[CapitalOmega]tOverISCO=OmegaTildeOverISCO[a,rTildeOverISCO];
\[CapitalOmega]t=\[CapitalOmega]tISCO*\[CapitalOmega]tOverISCO;
1/(64\[Pi]) 1/\[Eta] 1/\[CapitalOmega]t^(5/3) \[ScriptCapitalN]orb[a,rTildeOverISCO]
];


(* ::Input::Initialization:: *)
(*
For the definition y(Overscript[\[CapitalOmega], ~],a)=\!\(
\*SubsuperscriptBox[\(\[Integral]\), 
OverscriptBox[\(\[CapitalOmega]\), \(~\)], 
SubscriptBox[
OverscriptBox[\(\[CapitalOmega]\), \(~\)], \(ISCO\)]]\(
\*FractionBox[\(\[ScriptCapitalN]\((a, x/
\*SubscriptBox[
OverscriptBox[\(\[CapitalOmega]\), \(~\)], \(ISCO\)])\)\), 
SuperscriptBox[\(x\), \(11/3\)]]\[DifferentialD]x\)\),
the pre-calculated result here is
{
{y(Subscript[Overscript[\[CapitalOmega], ~], 1],a1),y(Subscript[Overscript[\[CapitalOmega], ~], 2],a1),...,y(Subscript[Overscript[\[CapitalOmega], ~], ISCO],a1)},
{y(Subscript[Overscript[\[CapitalOmega], ~], 1],a2),y(Subscript[Overscript[\[CapitalOmega], ~], 2],a2),...,y(Subscript[Overscript[\[CapitalOmega], ~], ISCO],a2)}
...
}
*)
(*
RemainTime["DimensionlessIntegral-Data-LogScan"]=Table[
Table[
{Exp[ln\[CapitalOmega]t],NIntegrate[\[ScriptCapitalN][a,x\[CapitalOmega]t/OmegaTildeISCO[a]]/x\[CapitalOmega]t^(11/3),{x\[CapitalOmega]t,Exp[ln\[CapitalOmega]t],OmegaTildeISCO[a]}]}
,
{ln\[CapitalOmega]t,Log[10^-5OmegaTildeISCO[a]],Log[OmegaTildeISCO[a]],(Log[OmegaTildeISCO[a]]-Log[10^-5OmegaTildeISCO[a]])/200}
]
,
{a,MBHSpinList}
];
RemainTime["DimensionlessIntegral-Data-Scan"]=Table[
Table[
{\[CapitalOmega]t,NIntegrate[\[ScriptCapitalN][a,x\[CapitalOmega]t/OmegaTildeISCO[a]]/x\[CapitalOmega]t^(11/3),{x\[CapitalOmega]t,\[CapitalOmega]t,OmegaTildeISCO[a]}]}
,
{\[CapitalOmega]t,N[OmegaTildeISCO[a]/50],OmegaTildeISCO[a],N[(OmegaTildeISCO[a]-OmegaTildeISCO[a]/50)/500]}
]
,
{a,MBHSpinList}
];
RemainTime["DimensionlessIntegral-Data-Scan-ISCO"]=Table[
Table[
{\[CapitalOmega]t,NIntegrate[\[ScriptCapitalN][a,x\[CapitalOmega]t/OmegaTildeISCO[a]]/x\[CapitalOmega]t^(11/3),{x\[CapitalOmega]t,\[CapitalOmega]t,OmegaTildeISCO[a]}]}
,
{\[CapitalOmega]t,N[0.9*OmegaTildeISCO[a]],OmegaTildeISCO[a],N[(OmegaTildeISCO[a]-0.9*OmegaTildeISCO[a])/100]}
]
,
{a,MBHSpinList}
];
RemainTime["DimensionlessIntegral-Data-Scan-ISCO-2"]=Table[
Table[
{\[CapitalOmega]t,NIntegrate[\[ScriptCapitalN][a,x\[CapitalOmega]t/OmegaTildeISCO[a]]/x\[CapitalOmega]t^(11/3),{x\[CapitalOmega]t,\[CapitalOmega]t,OmegaTildeISCO[a]}]}
,
{\[CapitalOmega]t,N[0.999OmegaTildeISCO[a]],OmegaTildeISCO[a],N[(OmegaTildeISCO[a]-0.999OmegaTildeISCO[a])/100]}
]
,
{a,MBHSpinList}
];
Do[
RemainTime["Integral-y"][MBHSpinList[[n]]]=
DropDuplicate@N@Sort[
N@Flatten[{
RemainTime["DimensionlessIntegral-Data-LogScan"][[n]],
RemainTime["DimensionlessIntegral-Data-Scan"][[n]],
RemainTime["DimensionlessIntegral-Data-Scan-ISCO"][[n]],
RemainTime["DimensionlessIntegral-Data-Scan-ISCO-2"][[n]]
},1]
,
#1[[1]]<#2[[1]]&
],
{n,1,Length[MBHSpinList]}
];
*)


(* ::Input::Initialization:: *)
(*Interpolation*)
(*Do[RemainTime["Overscript[\[CapitalOmega], ~]-y(a)"][MBHSpinList[[n]]]=Interpolation[RemainTime["DimensionlessIntegral"][[n]]],{n,1,Length[MBHSpinList]}];
Do[RemainTime["y(a)-Overscript[\[CapitalOmega], ~]"][MBHSpinList[[n]]]=Interpolation[Reverse/@(RemainTime["DimensionlessIntegral"][[n]])]
,{n,1,Length[MBHSpinList]}];*)


(* ::Input::Initialization:: *)
(*
(*Interpolation: Log*)
Do[RemainTime["Log(Overscript[\[CapitalOmega], ~])-y(a)"][a]=Interpolation[{Log@#[[1]],#[[2]]}&/@RemainTime["Integral-y"][a]],{a,MBHSpinList}];
Do[RemainTime["y(a)-Log(Overscript[\[CapitalOmega], ~])"][a]=Interpolation[{#[[2]],Log@#[[1]]}&/@RemainTime["Integral-y"][a]],{a,MBHSpinList}];
*)


(* ::Input::Initialization:: *)
(*
RemainTime["Integral-y"][0.999]=Drop[RemainTime["Integral-y"][0.999],{-2}];
RemainTime["Integral-y"][0.999]>>"data/OmegaTile-y0999.txt";
*)
(*new data with no detailed data around ISCO*)
(*
RemainTime["DimensionlessIntegral-Data-LogScan"]=ParallelTable[
Table[
{Exp[ln\[CapitalOmega]t],NIntegrate[\[ScriptCapitalN][a,x\[CapitalOmega]t/OmegaTildeISCO[a]]/x\[CapitalOmega]t^(11/3),{x\[CapitalOmega]t,Exp[ln\[CapitalOmega]t],OmegaTildeISCO[a]}]}
,
{ln\[CapitalOmega]t,Log[10^-5OmegaTildeISCO[a]],Log[OmegaTildeISCO[a]],(Log[OmegaTildeISCO[a]]-Log[10^-5OmegaTildeISCO[a]])/1000}
]
,
{a,MBHSpinList}
];
RemainTime["DimensionlessIntegral-Data-LogScan"]>>"data/OmegaTilde_y_all-spins_logscan_1000.txt";
*)
RemainTime["DimensionlessIntegral-Data-LogScan"]=<<"data/OmegaTilde_y_all-spins_logscan_1000.txt";
(*for this one, use more refined data set*)
Do[
RemainTime["Log(\!\(\*OverscriptBox[\(\[CapitalOmega]\), \(~\)]\))-y(a)"][MBHSpinList[[n]]]=Interpolation[{Log@#[[1]],#[[2]]}&/@RemainTime["DimensionlessIntegral-Data-LogScan"][[n]]];
RemainTime["y(a)-Log(\!\(\*OverscriptBox[\(\[CapitalOmega]\), \(~\)]\))"][MBHSpinList[[n]]]=Interpolation[{#[[2]],Log@#[[1]]}&/@RemainTime["DimensionlessIntegral-Data-LogScan"][[n]]];
,
{n,Length@MBHSpinList}
];
RemainTime["Integral-y"][0.999]=<<"data/OmegaTile-y0999.txt";
RemainTime["Log(\!\(\*OverscriptBox[\(\[CapitalOmega]\), \(~\)]\))-y(a)"][0.999]=Interpolation[{Log@#[[1]],#[[2]]}&/@RemainTime["Integral-y"][0.999]];
RemainTime["y(a)-Log(\!\(\*OverscriptBox[\(\[CapitalOmega]\), \(~\)]\))"][0.999]=Interpolation[{#[[2]],Log@#[[1]]}&/@RemainTime["Integral-y"][0.999]];


(* ::Input::Initialization:: *)
(*Given the time counted from ISCO(the remaining time), calculate the corresponding dimensionless frequency: Overscript[\[CapitalOmega], ~]*)
(*Inputs in SI unit*)
OmegaTildeAtTime[a_,M_,\[Eta]_][T_]:=Module[
{factor,yInt},
factor=5/256 M^2/(\[Eta]*M) 8/3 G*c^-2*c^-1;
yInt=T/factor;
(*RemainTime["y(a)-Overscript[\[CapitalOmega], ~]"][a][yInt]*)
Exp[RemainTime["y(a)-Log(\!\(\*OverscriptBox[\(\[CapitalOmega]\), \(~\)]\))"][a][yInt]]
];
(*The actual frequency: extra parameter is m(harmonic number)*)
FrequencyAtTime[a_,M_,\[Eta]_,m_][T_]:=(m*OmegaTildeAtTime[a,M,\[Eta]][T])/(2\[Pi] M*G*c^-3);
FrequencyAtISCO[a_,M_,m_]:=(m*OmegaTildeISCO[a])/(2\[Pi] M*G*c^-3);


(* ::Text::Initialization:: *)
(*Table of  \[ScriptCapitalT] and \[ScriptCapitalN] as function of a and \!\(\*OverscriptBox[\(r\), \(~\)]\)/Subscript[\!\(\*OverscriptBox[\(r\), \(~\)]\), isco]=r/Subscript[r, isco]. Agree with Table IX, Table X.*)


(* ::Input::Initialization:: *)
(*Export["Figures/T-rrisco.png",DataInput["\[ScriptCapitalT]-r/Subscript[r, isco]"]];*)
DataInput["\[ScriptCapitalT]-r/\!\(\*SubscriptBox[\(r\), \(isco\)]\)"]=Module[
{aList,nRow,MainTable,FirstRow,row,NoFirstRow,Table1},
nRow=Length[rTildeOverrISCOList];
aList=MBHSpinList;
MainTable=Table[N@\[ScriptCapitalT][a,rTildeOverISCO],{rTildeOverISCO,rTildeOverrISCOList},{a,aList}];
FirstRow=PrependTo[aList,"r/\!\(\*SubscriptBox[\(r\), \(isco\)]\)"];(*add the (1,1) component of the table*)
NoFirstRow=
Table[
(*add for each row of MainTable the corresponding value of r/Subscript[r, isco]*)
Prepend[MainTable[[row]],rTildeOverISCOList[[row]]]
,{row,1,nRow}
];
(*add first row*)
Table1=PrependTo[NoFirstRow,FirstRow];
Grid[Table1,Frame->All]
];


(* ::Input::Initialization:: *)
(*Export["Figures/Norb-rrisco.png",DataInput["\[ScriptCapitalN]orb-r/Subscript[r, isco]"]];*)
DataInput["\[ScriptCapitalN]orb-r/\!\(\*SubscriptBox[\(r\), \(isco\)]\)"]=Module[
{aList,nRow,MainTable,FirstRow,row,NoFirstRow,Table1},
nRow=Length[rTildeOverrISCOList];
aList=MBHSpinList;
MainTable=Table[N@\[ScriptCapitalN]orb[a,rTildeOverISCO],{rTildeOverISCO,rTildeOverrISCOList},{a,aList}];
FirstRow=PrependTo[aList,"r/\!\(\*SubscriptBox[\(r\), \(isco\)]\)"];(*add the (1,1) component of the table*)
NoFirstRow=
Table[
(*add for each row of MainTable the corresponding value of r/Subscript[r, isco]*)
Prepend[MainTable[[row]],rTildeOverISCOList[[row]]]
,{row,1,nRow}
];
(*add first row*)
Table1=PrependTo[NoFirstRow,FirstRow];
Grid[Table1,Frame->All]
];


(* ::Subsection::Initialization::Closed:: *)
(*\[ScriptCapitalH]c,\[ScriptCapitalH]o, Subscript[h, c,m], Subscript[\!\(\*SuperscriptBox[\(h\), \('\)]\), c,m], Subscript[h, o,m]*)


(* ::Input::Initialization:: *)
(*\[ScriptCapitalH]c(m,a,Overscript[\[CapitalOmega], ~]/Subscript[Overscript[\[CapitalOmega], ~], isco])*)
ClearAll[\[ScriptCapitalH]c];
\[ScriptCapitalH]c[m_,a_,OmegaTildeOverISCO_]/;(IntegerQ[m]&&m>=1)&&(OmegaTildeOverISCOMim[a]<=OmegaTildeOverISCO<1):=Sqrt[\[ScriptCapitalN][a,OmegaTildeOverISCO]*d\[ScriptCapitalE]dt\[Infinity][m,a][OmegaTildeOverISCO]];
(*A naive extrapolation*)
(*\[ScriptCapitalH]c[m_,a_,OmegaTildeOverISCO_]/;!(OmegaTildeOverISCOMim[a]<=OmegaTildeOverISCO<1):=1;*)
\[ScriptCapitalH]c[m_,a_,OmegaTildeOverISCO_]/;!(OmegaTildeOverISCOMim[a]<=OmegaTildeOverISCO<1):=Module[
{rMinOverr},
rMinOverr=rTilde[a,OmegaTildeOverISCOMim[a]]/rTilde[a,OmegaTildeOverISCO];
1*(1-Sqrt[rMinOverr])+\[ScriptCapitalH]c[m,a,OmegaTildeOverISCOMim[a]]Sqrt[rMinOverr]];


(* ::Input::Initialization:: *)
ClearAll[\[ScriptCapitalH]o];
\[ScriptCapitalH]o[m_,a_,OmegaTildeOverISCO_]/;(IntegerQ[m]&&m>=1)&&(OmegaTildeOverISCOMim[a]<=OmegaTildeOverISCO<1):=Sqrt[d\[ScriptCapitalE]dt\[Infinity][m,a][OmegaTildeOverISCO]];
(*A naive extrapolation*)
(*\[ScriptCapitalH]c[m_,a_,OmegaTildeOverISCO_]/;!(OmegaTildeOverISCOMim[a]<=OmegaTildeOverISCO<1):=1;*)
\[ScriptCapitalH]o[m_,a_,OmegaTildeOverISCO_]/;!(OmegaTildeOverISCOMim[a]<=OmegaTildeOverISCO<1):=Module[
{rMinOverr},
rMinOverr=rTilde[a,OmegaTildeOverISCOMim[a]]/rTilde[a,OmegaTildeOverISCO];
(*use a simple extrapolation, though the same as Hc*)
1*(1-Sqrt[rMinOverr])+\[ScriptCapitalH]o[m,a,OmegaTildeOverISCOMim[a]]Sqrt[rMinOverr]
];


(* ::Input::Initialization:: *)
(*1-cSqrt[rTilde[a,OmegaTildeOverISCOMim[a]]/rTilde[a,OmegaTildeOverISCO]]=\[ScriptCapitalH]c[m,a,OmegaTildeOverISCOMim[a]]*)


(* ::Input::Initialization:: *)
hc2[m_,a_,M_,\[Eta]_,ro_,fm_]/;IntegerQ[m]&&m>=1:=Module[
{fmisco,M0},
fmisco=OmegaTildeISCO[a]/((2\[Pi])/m*M*(G*c^-2)*c^-1);
hc[m,a,M,\[Eta],ro,fm]Min[1,Sqrt[3(1-fm/fmisco)]]
];
fMim[a_,m_,M_]:=(OmegaTildeMim[a]m)/(2\[Pi] M(G*c^-2)*c^-1);
ClearAll[ho];
Options[ho]={"RelativisticCorrection"->True,"ISCOCutoff"->True};
(*added NumberQ Boson Star*)
ho[m_?NumberQ,a_?NumberQ,M_?NumberQ,\[Eta]_?NumberQ,ro_?NumberQ,fm_?NumberQ,OptionsPattern[]]/;IntegerQ[m]&&m>=1:=Module[
{M0,\[CapitalOmega]t,hoNoHc,RC,ISCOCut},
M0=M*G*c^-2;
\[CapitalOmega]t=(2\[Pi])/m*M*fm*(G*c^-2)*c^-1;(*dimensionless: calc from SI value of Subscript[f, m] and M*)
If[ro===0||ro===0.,Return[Infinity]];
hoNoHc=Switch[m,
1,Sqrt[5./7.] ( \[Eta]*M0)/ro \[CapitalOmega]t,
_,Sqrt[(8 g[m])/m] (\[Eta]*M0)/ro \[CapitalOmega]t^(m/3.)
];
ISCOCut=OptionValue["ISCOCutoff"];
If[OptionValue["RelativisticCorrection"],ISCOCut=True];
If[(\[CapitalOmega]t>OmegaTildeISCO[a])&&ISCOCut,Return[0.]];
(*for RC, f<fISCO is always applied*)
RC=If[OptionValue["RelativisticCorrection"],\[ScriptCapitalH]o[m,a,\[CapitalOmega]t/OmegaTildeISCO[a]],1];
hoNoHc*RC
];


(* ::Subsection::Initialization::Closed:: *)
(*hc*)


(* ::Input::Initialization:: *)
hc[m_?NumberQ,a_?NumberQ,M_?NumberQ,\[Eta]_?NumberQ,ro_?NumberQ,fm_?NumberQ]/;IntegerQ[m]&&m>=1:=Module[
{M0,\[CapitalOmega]t,hcNoHc,res},
M0=M*G*c^-2;
\[CapitalOmega]t=(2\[Pi])/m*M*fm*(G*c^-2)*c^-1;(*dimensionless: calc from SI value of Subscript[f, m] and M*)
If[ro===0||ro===0.,Return[Infinity]];
hcNoHc=Switch[m,
1,(5 Sqrt[\[Eta]]M0)/(Sqrt[672\[Pi]]ro) \[CapitalOmega]t^(1/6),
_,Sqrt[(5 g[m])/(12\[Pi])] (Sqrt[\[Eta]]M0)/ro \[CapitalOmega]t^((2m-5)/6)
];
(*maximum frequency is Subscript[Overscript[\[CapitalOmega], ~], isco](a), if larger, will plunge*)
(*Return[
If[(\[CapitalOmega]t>OmegaTildeISCO[a])||(\[CapitalOmega]t<OmegaTildeMim[a])
,
0
,
hcNoHc*\[ScriptCapitalH]c[m,a,\[CapitalOmega]t/OmegaTildeISCO[a]]
]
];*)
Return[
If[\[CapitalOmega]t>OmegaTildeISCO[a]
,
0
,
hcNoHc*\[ScriptCapitalH]c[m,a,\[CapitalOmega]t/OmegaTildeISCO[a]]
]
];
];


(* ::Subsection::Initialization::Closed:: *)
(*Ch*)


(* ::Input::Initialization:: *)
Ch[m_,a_,M_,fm_]:=With[{\[CapitalOmega]t=(2\[Pi])/m*M*fm*(G*c^-2)*c^-1},\[ScriptCapitalH]o[m,a,\[CapitalOmega]t/OmegaTildeISCO[a]]];


(* ::Section::Initialization:: *)
(*Frequency-Hough*)


(* ::Subsection::Initialization::Closed:: *)
(*Basics*)


(* ::Input::Initialization:: *)
(*\[ScriptCapitalN](a,Overscript[\[CapitalOmega], ~]/Subscript[Overscript[\[CapitalOmega], ~], isco])*)
\[ScriptCapitalN]d\[ScriptCapitalE]dt[a_,OmegaTildeOverISCO_]:=With[{rt=rTilde[a,OmegaTildeOverISCO]},(1+a/rt^(3/2))^(5/3) (1-6/rt+(8a)/rt^(3/2)-(3a^2)/rt^2)(1-3/rt+(2a)/rt^(3/2))^(-3/2)];
(*\[ScriptCapitalN]r(a,Overscript[r, ~]/Subscript[Overscript[r, ~], isco])*)
\[ScriptCapitalN]d\[ScriptCapitalE]dtInr[a_,rTildeOverISCO_]:=\[ScriptCapitalN]d\[ScriptCapitalE]dt[a,OmegaTildeOverISCO[a,rTildeOverISCO]];
\[ScriptCapitalN]d\[ScriptCapitalE]dtInf[a_,fm_,m_,M_]:=With[{\[CapitalOmega]t=(2\[Pi])/m*M*fm*(G*c^-2)*c^-1},\[ScriptCapitalN]d\[ScriptCapitalE]dt[a,\[CapitalOmega]t/OmegaTildeISCO[a]]];


(* ::Input::Initialization:: *)
fISCO[a_,m_,M_]:=OmegaTildeISCO[a]/((2\[Pi])/m*M*(G*c^-2)*c^-1);


(* ::Input::Initialization:: *)
LabelInset[label_,color_,x_,y_,fontsize_,rotate_,scale_,LeftRightCenter_]:=Inset[Style[Rotate[Text[label],rotate],color,Italic,Bold,FontSize->fontsize],Scaled[{x,y}],LeftRightCenter,scale,{1,0}];


(* ::Input::Initialization:: *)
(*Here ML and MS can be given in any order*)
FrequencyAtTimeNoRC[ML_,MS_,tRemaining_]:=Module[
{Mc},
Mc=(MS*ML)^(3/5)/(MS+ML)^(1/5);
c*1/\[Pi] (5/(256tRemaining*c))^(3/8) ((G*Mc)/c^2)^(-5/8)
];


(* ::Input::Initialization:: *)
SqrtSn=Import["data/fs_Sn.txt","Table"];
SqrtSnLogFunc=Interpolation@Log@SqrtSn;
SnFunc[f_]:=(Exp@SqrtSnLogFunc[Log[f]])^2;


(* ::Subsection::Initialization::Closed:: *)
(*dEdfEMRI*)


(* ::Input::Initialization:: *)
ClearAll[dEdfEMRI];
Options[dEdfEMRI]={"f"->100.,"MS"->1Msolar,"ML"->1Msolar,"a"->0,"m"->2};
dEdfEMRI["EMRI",OptionsPattern[]]:=Module[
{MS,ML,Mc,f},
f=OptionValue["f"];
MS=OptionValue["MS"];
ML=OptionValue["ML"];
Mc=(MS*ML)^(3/5)/(MS+ML)^(1/5);
\[Pi]^(2/3)/(3G) (G Mc)^(5/3) f^(-1/3) \[ScriptCapitalN]d\[ScriptCapitalE]dtInf[OptionValue["a"],f,OptionValue["m"],ML]
];


(* ::Subsection::Initialization::Closed:: *)
(*TimeRemaining*)


(* ::Input::Initialization:: *)
(*This needs ISCO added! Currently going to f\[Rule]\[Infinity]*)
TimeRemaining["IMR",m1_,m2_,f_]:=Module[
{Mc},
Mc=(m1*m2)^(3/5)/(m1+m2)^(1/5);
5/(256c) 1/(((f*\[Pi])/c)^(8/3) ((G Mc)/c^2)^(5/3))
];


(* ::Subsection::Initialization::Closed:: *)
(*dfdt*)


(* ::Input::Initialization:: *)
dfdt["IMR",m1_,m2_,f_]:=Module[
{Mc},
Mc=(m1*m2)^(3/5)/(m1+m2)^(1/5);
c^2 96/5 \[Pi]^(8/3) ((G Mc)/c^2)^(5/3) (f/c)^(11/3)
];


(* ::Input::Initialization:: *)
dfdt["EMRI",a_,m1_,m2_,fm2_]:=Module[
{Mc,m,\[CapitalOmega]t,M},
Mc=(m1*m2)^(3/5)/(m1+m2)^(1/5);
(*m=2 by default*)
m=2;
M=Max[m1,m2];
\[CapitalOmega]t=(2\[Pi])/m*M*fm2*(G*c^-2)*c^-1;
(*this reduces to Finn&Thorn's Eq.3.2 when Mc is expanded in terms of mLight/mHeavy*)
c^2 96/5 \[Pi]^(8/3) ((G Mc)/c^2)^(5/3) (fm2/c)^(11/3) 1/\[ScriptCapitalN][a,\[CapitalOmega]t/OmegaTildeISCO[a]]
];


(* ::Subsection::Initialization::Closed:: *)
(*hc*)


(* ::Input::Initialization:: *)
hc["IMR",M_?NumberQ,\[Eta]_?NumberQ,ro_?NumberQ,fm_?NumberQ,ISCOCutoffQ_]:=Module[
{M0,\[CapitalOmega]t,hcNoHc,res,m},
(*this inspiral hc just corresponds to the m=2 case, with relativistic corrections dropped*)
M0=M*G*c^-2;
m=2;
\[CapitalOmega]t=(2\[Pi])/m*M*fm*(G*c^-2)*c^-1;(*dimensionless: calc from SI value of Subscript[f, m] and M*)
If[ro===0||ro===0.,Return[Infinity]];
(*dimensionless, G, c have been added to M*)
hcNoHc=Sqrt[2./(3\[Pi])] (Sqrt[\[Eta]]M0)/ro \[CapitalOmega]t^(-(1/6));
If[ISCOCutoffQ,
If[\[CapitalOmega]t>OmegaTildeISCO[0],Return[0.]];
];
Return[hcNoHc];
];


(* ::Input::Initialization:: *)
hc[m_?NumberQ,a_?NumberQ,M_?NumberQ,\[Eta]_?NumberQ,ro_?NumberQ,fm_?NumberQ]/;IntegerQ[m]&&m>=1:=Module[
{M0,\[CapitalOmega]t,hcNoHc,res},
M0=M*G*c^-2;
\[CapitalOmega]t=(2\[Pi])/m*M*fm*(G*c^-2)*c^-1;(*dimensionless: calc from SI value of Subscript[f, m] and M*)
If[ro===0||ro===0.,Return[Infinity]];
hcNoHc=Switch[m,
1,(5 Sqrt[\[Eta]]M0)/(Sqrt[672\[Pi]]ro) \[CapitalOmega]t^(1/6),
_,Sqrt[(5 g[m])/(12\[Pi])] (Sqrt[\[Eta]]M0)/ro \[CapitalOmega]t^((2m-5)/6)
];
Return[
If[\[CapitalOmega]t>OmegaTildeISCO[a]
,
0
,
hcNoHc*\[ScriptCapitalH]c[m,a,\[CapitalOmega]t/OmegaTildeISCO[a]]
]
];
];


(* ::Subsection::Initialization::Closed:: *)
(*ScanFFT*)


(* ::Input::Initialization:: *)
ScanFFT["EMRI",a_,ML_,fStart_,fEnd_,\[CapitalDelta]f_]:=Module[
{m,fisco,fend,res,fe},
m=2;
fisco=fISCO[a,m,ML];
If[fStart>fisco,Print["fISCO exceeded"];Return[{},Module]];
fend=Min[fisco,fEnd];
res={};
Do[
Do[
AppendTo[res,
{
(*calc TFFT with df/dt at the end*)
\[Eta],TimeToISCOFromFreq[ML,\[Eta],a,m,fs]-TimeToISCOFromFreq[ML,\[Eta],a,m,#],fs,1/Sqrt[dfdt["EMRI",a,ML,\[Eta]*ML,#]]
}&@Min[fs+\[CapitalDelta]f,fisco]
];
,
{fs,Exp@Range[Log@fStart,Log@fend,(Log@fend-Log@fStart)/100.]}
],
{\[Eta],Exp@Range[Log@(10^-8),Log@(10^-2),(Log@(10^-2)-Log@(10^-8))/201.]}
];
res
];


(* ::Subsection::Initialization::Closed:: *)
(*FrequencyHough*)


(* ::Input::Initialization:: *)
ClearAll[FrequencyHough];
Options[FrequencyHough]={
"fStart"->100.,
"\[CapitalDelta]f"->10.,
"M1"->1Msolar,
"M2"->1Msolar,
"a"->0,
"\[CapitalGamma]"->0.95,
"\[Theta]Threshold"->2.5,
"CRThreshold"->5.,
"RC"->True,
"Detector"->"aLIGO-design-dcc",
"ISCOCutoff"->True,
"FreqInBand"->-1,
"Compactness"->"BH",
"TidalCutoff"->False,(*only if ISCOCutoff is turned on*)
"LastBand"->False(*overwrite other options, use last \[CapitalDelta]f band*),
"MaxDuration"->OneYearSI
};
FrequencyHough[OptionsPattern[]]:=Module[
{m,p0,p1,CR,a,\[CapitalGamma],n,\[Theta],RC,ML,MS,Mc,\[Eta],fisco,fs,\[CapitalDelta]f,fe,$C,rTidalOverISCO,fTidal,tRemainingAll,tTotal,tFFT,nSFT,tRemainingList,fList,SnList,hA,Amin,fBase,hMin,dMax,\[ScriptCapitalF]List},
m=2;(*fixed*)
CR=OptionValue["CRThreshold"];
a=OptionValue["a"];
\[CapitalGamma]=OptionValue["\[CapitalGamma]"];
\[Theta]=OptionValue["\[Theta]Threshold"];
RC=OptionValue["RC"];
ML=Max@{OptionValue["M1"],OptionValue["M2"]};
MS=Min@{OptionValue["M1"],OptionValue["M2"]};
Mc=(MS*ML)^(3/5)/(MS+ML)^(1/5);
\[Eta]=MS/ML;
fisco=fISCO[a,m,ML];
fs=OptionValue["fStart"];
\[CapitalDelta]f=OptionValue["\[CapitalDelta]f"];
(**************************************************************)
(*generally we need this cutoff, but we need it for crosschecks*)
If[OptionValue["ISCOCutoff"],
If[fs>fisco,Return[{}]];
fe=Min[fisco,fs+\[CapitalDelta]f];
$C=OptionValue["Compactness"];
(*no change if inspiralling object is a BH*)
If[OptionValue["TidalCutoff"]&&$C=!="BH",
rTidalOverISCO=rTidal[ML,MS,$C]/rSI[ML,rTildeISCO[a]];
(*no change if tidal radius is smaller than ISCO*)
If[rTidalOverISCO>1,
fTidal=FrequencyAtNISCO[a,ML,m][rTidalOverISCO];
If[fs>fTidal,Return[{}]];
fe=Min[fTidal,fs+\[CapitalDelta]f];
];
];
,
(*in this case, the frequency can go to infinity, we also do not impose tidal disruption criteria*)
fe=fs+\[CapitalDelta]f;
];
(*here the we take the last band of width \[CapitalDelta]f, counted from the smaller f at ISCO and Tidal*)
If[OptionValue["LastBand"],
rTidalOverISCO=rTidal[ML,MS,$C]/rSI[ML,rTildeISCO[a]];
fTidal=FrequencyAtNISCO[a,ML,m][rTidalOverISCO];
fe=Min[fisco,fTidal]+30;
fs=fe-\[CapitalDelta]f;
];
(**************************************************************)
(*determine the number of SFTs*)
If[RC,
tRemainingAll=TimeToISCOFromFreq[ML,\[Eta],a,m,fs];
tTotal=tRemainingAll-TimeToISCOFromFreq[ML,\[Eta],a,m,fe];
tFFT=1/Sqrt[dfdt["EMRI",a,ML,\[Eta]*ML,fe]];
,
tRemainingAll=TimeRemaining["IMR",ML,\[Eta]*ML,fs];
tTotal=tRemainingAll-TimeRemaining["IMR",ML,\[Eta]*ML,fe];
tFFT=1/Sqrt[dfdt["IMR",ML,\[Eta]*ML,fe]];
];
If[tTotal>OptionValue["MaxDuration"],Return[{"Error"->"MaxDurationExceeded"}]];
nSFT=Floor[tTotal/tFFT];
If[nSFT<1,Return[{}]];
(**************************************************************)
tRemainingList=Table[#-n*tFFT,{n,0,nSFT-1}]&@tRemainingAll;
fList=If[RC,
FrequencyAtTime[a,ML,\[Eta],m][#]&/@tRemainingList
,
FrequencyAtTimeNoRC[ML,MS,#]&/@tRemainingList
];
(*SnList=Exp[LogHn[OptionValue["Detector"]][Log[#]]]^2/#&/@fList;*)
(*currently just use Andrew's sensitivity curve*)
SnList=SnFunc/@fList;
(*hA[d_,fm_,rc_]:=ho[m,a,ML,\[Eta],d,fm,"RelativisticCorrection"\[Rule]rc,"ISCOCutoff"\[Rule]OptionValue["ISCOCutoff"]]/Sqrt[2/5.];*)
p0=Exp[-\[Theta]]-Exp[-2\[Theta]]+1/3. Exp[-3\[Theta]];
p1=Exp[-\[Theta]]-2Exp[-2\[Theta]]+Exp[-3\[Theta]];
\[ScriptCapitalF]List=fList^(2/3) (Ch[m,a,ML,#]&/@fList);
Amin=4.02/(nSFT^(1/4) Sqrt[\[Theta]]) Sqrt[nSFT/tFFT] 1/Sqrt[Total[\[ScriptCapitalF]List^2/SnList]] ((p0(1-p0))/p1^2)^(1/4) Sqrt[CR-Sqrt[2]InverseErfc[2\[CapitalGamma]]];
fBase=fList[[OptionValue["FreqInBand"]]];
hMin=Amin*fBase^(2/3) Ch[m,a,ML,fBase];
(*hMin=Amin*fBase^(2/3);
dMax=hA[1,fBase,RC]/hMin;*)
dMax=4/Amin ((G*Mc)/c^2)^(5/3) (\[Pi]/c)^(2/3);
(**************************************************************)
Return[
{
"MassRatio"->\[Eta],
"Duration"->tTotal,
"nSFT"->nSFT,
"tFFT"->tFFT,
"hMin"->hMin,
"dMax"->dMax/(10^6 onepc)
}
];
];


(* ::Subsection::Initialization::Closed:: *)
(*ScanMCTidal*)


(* ::Input::Initialization:: *)
ScanMCFileName[MH_,a_,fs_,\[CapitalDelta]f_]:="ScanMCTidal_"<>ToString[Floor[MH/Msolar]]<>"Msolar_a"<>ToString[a]<>"_"<>ToString[fs]<>"-"<>ToString[\[CapitalDelta]f]<>"Hz.txt";


(* ::Input::Initialization:: *)
ClearAll[ScanMCTidal];
Options[ScanMCTidal]={"SavePath"->".","nScan"->100,"UseLastBand"->False};
ScanMCTidal[MH_,a_,fs_,\[CapitalDelta]f_,OptionsPattern[]]:=Module[
{filename,CMTable,cECO,mECO,fhscan,dMax},
CMTable={};
filename=FileNameJoin[{OptionValue["SavePath"],ScanMCFileName[MH,a,fs,\[CapitalDelta]f]}];
SetSharedVariable[CMTable];
ParallelDo[
cECO=Exp@RandomReal[{Log@(10^-9.),Log@10.}];
mECO=Exp@RandomReal[{Log@(10^-9.),Log[10^-3.]}];
fhscan=FrequencyHough[
"fStart"->fs,
"\[CapitalDelta]f"->10.,
"M1"->MH,
"M2"->mECO*Msolar,
"a"->a,
"\[CapitalGamma]"->0.95,
"\[Theta]Threshold"->2.5,
"CRThreshold"->5.,
"RC"->True,
"Detector"->"aLIGO-design-dcc",
"ISCOCutoff"->True,
"FreqInBand"->1,
"Compactness"->cECO,
"TidalCutoff"->True,
"LastBand"->OptionValue["UseLastBand"]
];
(*AppendTo[CMTable,{cECO,mECO,"dMax"/.fhscan}];*)
dMax="dMax"/.fhscan;
If[dMax=!="dMax",
AppendTo[CMTable,{cECO,mECO,dMax,"Duration"/.fhscan,"nSFT"/.fhscan,"tFFT"/.fhscan,"hMin"/.fhscan}];
];
,
{n,Floor@OptionValue["nScan"]}
];
Put[CMTable,filename];
CMTable
];
(*ScanMCTidal[MH_,a_,fs_,nTotal_,LastBandQ_]:=Module[
{CMTable,cECO,mECO,fhscan},
CMTable={};
SetSharedVariable[CMTable];
ParallelDo[
cECO=Exp@RandomReal[{Log@(10^-9.),Log@10.}];
mECO=Exp@RandomReal[{Log@(10^-9.),Log[10^-3.]}];
fhscan=FrequencyHough[
"fStart"\[Rule]fs,
"\[CapitalDelta]f"\[Rule]10.,
"M1"\[Rule]MH,
"M2"\[Rule]mECO*Msolar,
"a"\[Rule]a,
"\[CapitalGamma]"\[Rule]0.95,
"\[Theta]Threshold"\[Rule]2.5,
"CRThreshold"\[Rule]5.,
"RC"\[Rule]True,
"Detector"->"aLIGO-design-dcc",
"ISCOCutoff"\[Rule]True,
"FreqInBand"\[Rule]1,
"Compactness"\[Rule]cECO,
"TidalCutoff"\[Rule]True,
"LastBand"\[Rule]LastBandQ
];
AppendTo[CMTable,{cECO,mECO,"dMax"/.fhscan}];
,
{n,nTotal}
];
CMTable
];
*)


(* ::Subsection::Initialization::Closed:: *)
(*ScanFH*)


(* ::Input::Initialization:: *)
ScanFH["EMRI",a_,MHeavy_,fStart_,fEnd_]:=Module[
{fisco,fend,res,fh},
(*m=2 enforced*)
fisco=fISCO[a,2,MHeavy];
If[fStart>fisco,Print["fISCO exceeded"];Return[{},Module]];
fend=Min[fisco,fEnd];
res={};
SetSharedVariable[res];
ParallelDo[
fh=FrequencyHough[
"fStart"->fs,
"\[CapitalDelta]f"->10.,
"M1"->MHeavy,
"M2"->\[Eta]*MHeavy,
"a"->a,
"\[CapitalGamma]"->0.95,
"\[Theta]Threshold"->2.5,
"CRThreshold"->5.,
"RC"->True,
"Detector"->"aLIGO-design-dcc",
"ISCOCutoff"->True,
"FreqInBand"->1
];
If["Duration">="tFFT"/.fh,AppendTo[res,{\[Eta],fs,"dMax","hMin"}/.fh];];
,
{fs,Table[Exp@RandomReal[{fStart,fend}],{n,100}]},
{\[Eta],Table[Exp@RandomReal[{Log@(10.^-8),Log@(10.^-2)}],{n,100}]}
(*{fs,Exp@Range[Log@fStart,Log@fEnd,(Log@fEnd-Log@fStart)/30.]},*)
(*{\[Eta],Exp@Range[Log@(10.^-8),Log@(10.^-2),(Log@(10^-2)-Log@(10^-8))/30.]}*)
];
res
];


(* ::Subsection::Initialization::Closed:: *)
(*ScanFH\[Eta]*)


(* ::Input::Initialization:: *)
ScanFH\[Eta]["EMRI",a_,MHeavy_,\[Eta]_,fsList_]:=Module[
{res,fh},
res={};
Do[
fh=FrequencyHough[
"fStart"->fs,
"\[CapitalDelta]f"->10.,
"M1"->MHeavy,
"M2"->\[Eta]*MHeavy,
"a"->a,
"\[CapitalGamma]"->0.95,
"\[Theta]Threshold"->2.5,
"CRThreshold"->5.,
"RC"->True,
"Detector"->"aLIGO-design-dcc",
"ISCOCutoff"->True,
"FreqInBand"->1
];
If["Duration">="tFFT"/.fh,AppendTo[res,{fs,"dMax","hMin"}/.fh];];
,
{fs,fsList}
];
res
];


(* ::Subsection::Initialization::Closed:: *)
(*ScanFHfs*)


(* ::Input::Initialization:: *)
ScanFHfs["EMRI",a_,MHeavy_,fs_]:=Module[
{res,fh},
res={};
Do[
fh=FrequencyHough[
"fStart"->fs,
"\[CapitalDelta]f"->10.,
"M1"->MHeavy,
"M2"->\[Eta]*MHeavy,
"a"->a,
"\[CapitalGamma]"->0.95,
"\[Theta]Threshold"->2.5,
"CRThreshold"->5.,
"RC"->True,
"Detector"->"aLIGO-design-dcc",
"ISCOCutoff"->True,
"FreqInBand"->1
];
If["Duration">="tFFT"/.fh,AppendTo[res,{\[Eta],fs,"dMax","hMin"}/.fh];];
,
{\[Eta],Exp@Range[Log@(10.^-8),Log@(10.^-2),(Log@(10^-2)-Log@(10^-8))/30.]}
];
res
];
