function [fs,Cf] = mini_emri_get_Ch_Cf(m1)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
if isdeployed
    directory = './';
else
    directory = '../../mini_emri/Cleaned/';
end
m1_str = num2str(m1);
filename = ['fs_vs_Cf_' m1_str 'Msun.txt'];

try
    T =readtable(strcat(directory,filename));
catch
    disp('table not created for the mass you have chosen - only for m1= 1, 10 and 100 Msun. Exiting..')
    return;
end

fs = T.Var1; %% frequencies at which Cf is calculated
Cf = T.Var2;
end