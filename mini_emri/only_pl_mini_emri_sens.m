function [outputArg1,outputArg2] = only_pl_mini_emri_sens(detector,m1,fmins,fmaxs,fstep)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
cc=constants;

if ~exist('fmin','var')
    fmin=50;                                                                    % minimum frequency of distance reach (Hz)
end

if ~exist('fmax','var')
    fmax=2000;                                                                  % maximum frequency of distance reach (Hz)
end

if ~exist('fstep','var')    
    fstep=50;                                                                   % calculate the distance reach every fstep (Hz)
end

if ~exist('mcs','var')
    mcs=[1e-3 1e-4 1e-5];                                                       % chirp mass (Msun)
end

if ~exist('tobs','var')
    tobs=365*86400;                                                             % observation time (s)
end

if ~exist('detector','var')
%     detector='O2_L'; %'ET';
    detector='advL_design';
end


if ~exist('Gamma','var')
    Gamma=0.9;                                                                  % confidence level
    theta_thr=2.5;                                                              % threshold on peak selection in peakmap
    CR_thr=5;                                                                   % threshold on critical ratio in Hough map
end




one_mpc=3.086e+22;                                                          % 1 Mpc = 3.086e22 m

one_kpc=one_mpc/1e3;                                                        % 1 kpc= 1 mpc/1e3


fisco=calc_f_isco(m1);
m2_over_m1=logspace(-8,-2,100);

mcs=m1*(m2_over_m1).^(3/5) ./ (1+ m2_over_m1).^(1/5);

if strcmp(detector,'lisa')

    fmin=1e-5;
	fmax=1e-3;%[1e-5 1e-4 1e-3];
    tobs=2*tobs;
    
    det_freq=logspace(log10(fmin),0,1000);
    Sn=lisa_Sn(det_freq,tobs_LISA);
    fmins=[1e-5 1e-4 1e-3 1e-2 1e-1];

elseif strcmp(detector,'decigo')
    fmin=1e-3;
    det_freq=logspace(log10(fmin),0,1000);
    Sn=decigo_Sn(det_freq);
elseif strcmp(detector,'advL_design')
%     fmins=[20 50 100 500 1000];
%     fstep=10;
%     if fisco>1000
%         fmins=20:100:fisco-fstep;
%     elseif fisco>100 && fisco<1000
%         fmins=20:50:fisco-fstep;
%     elseif fisco<100
%         fmins=20:10:fisco-fstep;
%     end
%     fmaxs=fmins+fstep;
end
%%%%%%%%% find systems that will be visible in both
figure(1);
figure(2);
for j=1:length(fmins)
    fmin=fmins(j);
    fmax=fmaxs(j);
    for i=1:length(mcs)
        mc=mcs(i);
       
        
        if strcmp(detector,'lisa') || strcmp(detector,'decigo')
            fdot_min=calc_fdot_chirp(mc,fmin);
%             fdot_max=calc_fdot_chirp(mc,fmax_LISA);
            
            TFFT_space=1/sqrt(fdot_min);
            
%             Nsky=4*pi*calc_ND(2000,1024)^2;
%             TFFT2=cc.c/cc.v_earth_orb * sqrt(Nsky/(2*pi))*1/ff_min_LISA;
%             TFFT=min(TFFT,TFFT2);
            if TFFT_space>tobs
                TFFT_space=tobs;
            end
            h0min=cw_calc_theo_h0min(Sn,TFFT_space,tobs);
            [~,ii]=min(abs(ff_min_LISA-det_freq));
            this_h0(i,j)=h0min(ii);
            d_lisa_kpc(i,j)=calc_chirp_d(this_h0(i,j),mc,fmin);
            TFFT_lisa(i,j)=TFFT_space;
        elseif strcmp(detector,'advL_design')
%         tini=calc_time_to_coalescence(mc,fmax_LISA);
%         fsss=calc_fgw_as_coalescing(mc,tini:-1:tini-tobs);
%         if abs(min(fsss)-max(fsss))<1e-5
%             fsss=fmax_LISA;
%         end
% %         fdot=calc_fdot_chirp(mc,fsss);
%         TFFT=1./sqrt(2*fdot);
%         if TFFT>8192 %%% bad
%             TFFT=8192;
%         end
        
%         [h00min_LISA(i,j),ddmax_LISA(i,j)] = cbc_calc_theo_hmin('lisa',fsss,mc,Sn,TFFT,tobs_LISA,Gamma,theta_thr,CR_thr);
            [ f0s(i,j),h00min(i,j),ddmax(i,j),Tobss(i,j),TFFTs(i,j) ] = pbh_theo_sens( detector,mc,tobs,fmin,fmax,fstep,Gamma,theta_thr,CR_thr,m1);
        end
        
        %%% need to combine different frequency bands for same signal [20
        %%% 30], [30 40] (recompute tfft and tobs) ,etc., until tfft<1 s
        
        
    end
    
    if strcmp(detector,'lisa')
        figure(3);
        scatter(m2_over_m1,d_lisa_kpc(:,j)/1e3,50,this_h0(:,j),'sq','filled'); colormap('jet'); colorbar;
        set(gca,'xscale','log') ;set(gca,'yscale','log'); set(gca,'ColorScale','log'); set(gca,'FontSize',14)
        xlabel('mass ratio'); ylabel('distance (Mpc)'); cblabel('gravitational-wave amplitude')
        hold on;
    end
    
    figure(1);
    indexes=Tobss(:,j)>TFFTs(:,j) & TFFTs(:,j)>1;
    scatter(m2_over_m1(indexes),Tobss(indexes,j),50,TFFTs(indexes,j),'sq','filled'); colormap('jet'); colorbar;
    set(gca,'xscale','log') ;set(gca,'yscale','log'); set(gca,'ColorScale','log'); set(gca,'FontSize',14)
    xlabel('mass ratio'); ylabel('observation time (s)'); cblabel('FFT length (s)')
    hold on;
    
    
    figure(2); 
    scatter(m2_over_m1(indexes),ddmax(indexes,j)/one_mpc,50,h00min(indexes,j),'sq','filled'); colormap('jet'); colorbar; 
    hold on;
    set(gca,'xscale','log') ;set(gca,'yscale','log'); set(gca,'ColorScale','log'); set(gca,'FontSize',14)
    xlabel('mass ratio'); ylabel('distance (Mpc)'); cblabel('gravitational-wave amplitude')
end
end

