function [p0,p1] = calc_p0_p1(theta_thr)
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here

if ~exist('theta_thr','var')
    theta_thr = 2.5;
end
p0=exp(-theta_thr)-exp(-2*theta_thr)+1/3*exp(-3*theta_thr);
p1=exp(-theta_thr)-2*exp(-2*theta_thr)+exp(-3*theta_thr);


end