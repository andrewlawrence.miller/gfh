function [ts,fs] = mini_emri_load_tf(m1)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
m1_str = num2str(m1);

if ~isdeployed
    directory = '../../mini_emri/Cleaned/';
else
    directory = './';
end

filename = ['ts_vs_fs_' m1_str 'Msolar.txt'];

try
    T =readtable(strcat(directory,filename));
catch
    disp('table not created for the mass you have chosen - only for m1= 1, 10 and 100 Msun. Exiting..')
    return;
end

ts = T.Var1; %% frequencies at which Cf is calculated
fs = T.Var2;
end