function [mean_numpeaks,std_numpeaks] = calc_mean_std_peaks(Tobs,TFFT,p0)
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here

if ~exist('p0','var')
    [p0,~] = calc_p0_p1();
end

N = 2 *Tobs ./ TFFT;

mean_numpeaks = N * p0;

std_numpeaks = sqrt(N * p0 * (1 - p0));


end