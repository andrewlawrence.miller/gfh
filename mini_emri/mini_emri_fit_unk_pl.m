function [n_onefit,k_onefit,rsq_onefit] = mini_emri_fit_unk_pl(fmin,fmax,mc,test_flag)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
m1 = 10;
n = 11 / 3;
[fs,Cf] = mini_emri_get_Ch_Cf(m1);


if ~exist('fmin','var')
    fmin = 10;
end
if ~exist('fmax','var')
    fmax = 420;
    if fmax>max(fs)
        fmax = max(fs);
    end
else
    if fmax>max(fs)
        disp('ISCO frequency exceeded! Try a frequency lower than')
        ISCO_f = max(fs)
        return;
    end
end

if ~exist('test_flag','var')
    test_flag = 1;
end

k = calc_k(mc);

unk_pl = k * fs.^n .* Cf;


[f,gof] = fit(fs(fs>=fmin & fs<=fmax),unk_pl(fs>=fmin & fs<=fmax),'power1');
coeffs_onefit = coeffvalues(f);
rsq_onefit = gof.rsquare;
n_onefit = coeffs_onefit(2);
k_onefit = coeffs_onefit(1);

if test_flag == 1
    ii = find(fs==fmin);
    jj = find(fs==fmax);
    k = 1;
    for i = ii:100:jj
        [f,gof] = fit(fs(fs<=fs(i)),unk_pl(fs<=fs(i)),'power1');
        coeffs = coeffvalues(f);
        n_fits(k) = coeffs(2);
        rsq(k) = gof.rsquare;
        corresp_f(k) = fs(i);
        k = k + 1;
    end
    
    figure;plot(corresp_f,n_fits,'sq')

end
end