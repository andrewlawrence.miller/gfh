function [outputArg1,outputArg2] = untitled3(inputArg1,inputArg2)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
cc=constants;
G=cc.G; c=cc.c; Msun=cc.Msun;
Mc1=1e-2*Msun;
Mc2=1e-7*Msun;
% x1=(G*Mc1*omegaS/c^3)^(2/3);
% x2=(G*Mc1*omegaS/c^3)^(2/3);
% nu=1/4; %equal masses

t_until_merger=-[0.1 1 10 10^2 10^3 10^4 10^5 10^6 10^7];
tc=0;
deltaT=tc-t_until_merger

m1=Mc1*2^(1/5);
m2=Mc2*2^(1/5);

theta1=nu*c^3/(5*G*m1)*deltaT;
theta2=nu*c^3/(5*G*m2)*deltaT;


PN0=1;
PN1_1=(3715/8064+5/96*nu)*(theta1.^(-1/4));
PN1_2=(3715/8064+5/96*nu)*(theta2.^(-1/4));




end

