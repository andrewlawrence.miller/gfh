function [rate_model_indep,ftilde_equal,ftilde_asymm,m22,fm2,...
    all_allowed_freqs,all_allowed_dists  ] = cleaned_get_constraints_from_uls(run,search,fdotmax,Mcs,fsup,m1,fm1,fpbh )
% Function that takes as input upper limits, h0(f), from an all-sky search,
% and outputs constraints on the rates of very light PBH binary systems
% inspiralling in two regimes: equal-mass and asymmetric mass ratio. The
% code follows the discussion in https://arxiv.org/pdf/2110.06188.pdf

% Inputs:
% run:      which run ('O2', 'O3a' or 'O3'), def='O3';
% search:   what kind of search ('all-sky'), def='all-sky'
% fdotmax:  maximum spin-up or abs(spin-down) considered (Hz/s), def=1e-8
%           Hz/s
% Mcs:      chirp masses at which to place constraints (solar masses),
%           def=logspace(-7,-4,50);
% fsup:     suppression factor due to grav. influence of PBHs in clusters
% m1:       mass of primary component for asymmetric mass ratio case (solar
%           masses), def=2.5
% fm1:      probabilty distribution function for m1 in asymmetric mass
%           ratio case, def=1
% fpbh:     fraction of dark matter that PBHs could compose, used in the
%           asymmetricm mass ratio case to constrain the effective 
%           parameters ftilde, def=1
%
% Outputs:   
% 
% rate_model_indep:     rate of PBH inspirals, indep of any formation model
% ftilde_equal:         effective parameter for the equal-mass case of PBH
%                       inspirals
% ftilde_asymm:         effective parameter for the asymm-mass case of PBH
%                       inspirals
% m22:                  secondary masses in the asymm-mass case
% fm2:                  probability distribution function of m22 in asymm
%                       case
% all_allowed_freqs:    the frequencies at which we can compute constraints
% all_allowed_dists:    the distances that we can include in constraints

n=11/3;
age_of_univ=13.6*1e9; %yrs
cutoff_time=age_of_univ/10*86400*365.25; % s

if ~exist('run','var')
    run='O3';
end

if ~exist('search','var')
    search='all-sky';
end

if strcmp(run,'O2')
    Tobs=268.37*86400;
elseif strcmp(run,'O3a')
    Tobs=250*86400/2;
elseif strcmp(run,'O3')
    Tobs=361*86400;                                                         % 2019 April 1 to 27 March 2020
end

if strcmp(search,'all-sky')
    if strcmp(run,'O2')
        fid=fopen('~/Downloads/FH_95UL.txt');
        A = textscan(fid,'%s %f64');
        fs_UL=A{1};
        h0s_UL=A{2};
    elseif strcmp(run,'O3a')
        A=readmatrix('~/Downloads/o3a_ul_all_simplified.csv');
        fs_UL=A(:,1);
        h0s_UL_circ=A(:,9);                                                 % circular polarization is ideal
        h0s_UL=h0s_UL_circ*2.30;                                            % from Keith; proxy for pop-avg upper limits is col. 9 * 2.30
    elseif strcmp(run,'O3')
        fid=fopen('~/Desktop/Belgio/O3_all-sky/O3_FH_UL.txt');
        A = textscan(fid,'%s %f64');
        fs_UL=A{1};
        h0s_UL=A{2};
    end
    
    
    
    if ~exist('fdotmax','var')
        if strcmp(run,'O2')
            fdotmax=2e-9;                                                   % -1e-8 up to 512 Hz
        elseif strcmp(run,'O3a')
            fdotmax=1e-9;                                                   %1e-8 if consider abs(fdot`)
        elseif strcmp(run,'O3')
            fdotmax=1e-8;                                                   % max abs(\dot{f})
        end
    end
end

if ~exist('Mcs','var')
    Mcs=logspace(-7,-4,50);
end

if ~exist('fsup','var')
    fsup=1;                                                                 % reasonable choice, given what CBC SSM papers choose..
end

if ~exist('m1','var')
    m1=2.5;                                                                 % assume this motivated by QCD transition and events like GWXXXXX
end

if ~exist('fm1','var')
    fm1=1;                                                                  % for asymm-mass case, assume PDF of m1, that most BHs have mass m1
end

if ~exist('fpbh','var')                                                     % for asymm-mass case, we constrain effective parameter at fpbh=1
    fpbh=1;                                                                 % fraction of dark matter that PBHs could compose
end

if iscell(fs_UL)
    fs_UL=str2double(fs_UL);                                                % just a check, to ensure we get an array of frequencies
end
fs_UL=fs_UL(h0s_UL>0 & h0s_UL<1);                                           % just a check, to ensure no unphysical upper limits
h0s_UL=h0s_UL(h0s_UL>0 & h0s_UL<1);

delta_f=fdotmax*Tobs;                                                       % sets bandwidth size for interpolation

interp_fs=[min(fs_UL):delta_f:max(fs_UL)]';                                 % interpolate frequencies and h0 values
h0s_UL_interp=interp1(fs_UL,h0s_UL,interp_fs);                              % we interpolate to have signals in frequency bands of size delta_f
                                                                            % because the search is sensitive to any signals that fall within
                                                                            % delta_f, even though upper limits are quoted every 1-Hz

fs_UL=interp_fs;                                                            % overwrite initial frequencies and h0s with interpolated ones
h0s_UL=h0s_UL_interp;

if strcmp(search,'all-sky')

    if strcmp(run,'O2')                                                     % max frequencies correspond to particular FFT lengths
        fmax_db1=128; fmax_db2=512;fmax_db3=1024;                           
    elseif strcmp(run,'O3a')
        fmax_db1=475; fmax_db2=1475;
    elseif strcmp(run,'O3')
        fmax_db1=128; fmax_db2=512; fmax_db3=1024; 
    end
end

if strcmp(run,'O3a')                                                        % max frequencies correspond to particular FFT lengths
    tffts_interp(fs_UL<=fmax_db1)=7200;
    tffts_interp(fs_UL<=fmax_db2 & fs_UL>fmax_db1)=3600;
    tffts_interp(fs_UL>fmax_db2)=1800;
elseif strcmp(run,'O3')
    tffts_interp(fs_UL<=fmax_db1)=8192;
    tffts_interp(fs_UL<=fmax_db2 & fs_UL>fmax_db1)=4096;
    tffts_interp(fs_UL>fmax_db2 & fs_UL<=fmax_db3)=2048;
    tffts_interp(fs_UL>fmax_db3)=1024;
end


%%%% initalization 
rate_model_indep=zeros(1,length(Mcs));
rate_model_indep_asymm=rate_model_indep;
ftilde_equal=rate_model_indep;
m22=ftilde_equal;
ftilde_asymm=m22;
all_allowed_freqs=cell(length(Mcs),1);
all_allowed_dists=all_allowed_freqs;
%%%% initalization

figure;

for kk=1:length(Mcs)                                                        % for each chirp mass, compute a limit on \tilde{f}
    m2=(Mcs(kk) / m1^(2/5)).^(5/3);                                         % calculate m2 for the asymmetric mass ratio case

    dists=calc_chirp_d(h0s_UL,Mcs(kk),fs_UL);                               % calculate distance reach, in kpc
    
    fmaxes=power_law(Tobs,fs_UL,fdotmax,n);                                 % obtain the maximum frequency spread of hypothetical signals
    
    fdots=calc_fdot_chirp(Mcs(kk),fmaxes);                                  % obtain the corresponding fdots at the maximum frequencies (conserv)
    
    allowed_indices_fdot=fdots<=fdotmax;                                    % check that the spin-ups don't exceed maximum allowed in search
    
    mtot_equal=2^(6/5)*Mcs(kk);                                             % for equal-mass case, compute the total mass
    
    fisco_equal=calc_f_isco(mtot_equal);                                    % calculate ISCO frequency, equal-mass case
    fisco_asymm_mass=calc_f_isco(m1);                                       % calculate ISCO frequency, asymmetric-mass ratio case
    
    linear_inds=is_fevol_linear(Tobs,fs_UL,fdots,tffts_interp',n);          % determine if frequency evolution is linear for each f/fdot/tfft pair
    
    delta_T=calc_delta_T(Mcs(kk),fs_UL,fmaxes);                             % calculate the effective duration of the signals within delta_f
    
    
    delta_T_isco_equal=calc_delta_T(Mcs(kk),fs_UL,fisco_equal);             % calculate the effective duration until ISCO, equal-mass case
    delta_T_isco_asymm=calc_delta_T(Mcs(kk),fs_UL,fisco_asymm_mass);        % calculate the effective duration until ISCO, asymm-mass case

    inds_allowed_times_equal=delta_T_isco_equal<cutoff_time;                % ensure that time to ISCO is not larger than age of universe/10
    inds_allowed_times_asymm=delta_T_isco_asymm<cutoff_time;    

    allowed_dists=dists(allowed_indices_fdot ...                            % only use distances that have fdots<fdotmax, follow linear f/t evol,
        & inds_allowed_times_equal & linear_inds);                          % and have durations less than age of universe/10
    allowed_times=delta_T(allowed_indices_fdot...                           % equal mass case here
        & inds_allowed_times_equal & linear_inds)/(525600*60);
    
    rate_model_indep(kk)=calc_rate_VT(allowed_dists,allowed_times);         % calculate rate, equal-mass case
    
    allowed_dists_asymm=dists(allowed_indices_fdot ...                      % same as above, but for asymm mass ratio case
        & inds_allowed_times_asymm & fs_UL<fisco_asymm_mass & linear_inds);
    allowed_times_asymm=delta_T(allowed_indices_fdot... 
        & inds_allowed_times_asymm & fs_UL<fisco_asymm_mass & linear_inds)/(525600*60);
    
    rate_model_indep_asymm(kk)=calc_rate_VT(allowed_dists_asymm,...         % calculate rate for asymm-mass case
        allowed_times_asymm);

    ftilde_equal(kk) = calc_ftilde_equal_mass(rate_model_indep(kk),...      % calculate ftilde for equal-mass case
        mtot_equal,fsup);

    ftilde_asymm(kk) = calc_ftilde_asymm_mass...                            % calculate ftilde for asymm-mass case
        (rate_model_indep_asymm(kk),m1,m2,fsup);
    
    allowed_freqs=fs_UL(allowed_indices_fdot ...                            % obtain frequencies at which fdot<fdotmax, linear f/t evol, 
        & inds_allowed_times_equal & linear_inds);                          % and durations less than age of universe/10
    
    scatter(Mcs(kk)*ones(size(allowed_dists)),allowed_dists,2,allowed_freqs,'o','filled');colormap('jet'); colorbar; hold on;
    set(gca,'xscale','log') ;set(gca,'yscale','log'); set(gca,'ColorScale','log'); set(gca,'FontSize',14)
    xlabel('chirp mass (solar mass)'); ylabel('distance reach (kpc)'); cblabel('frequency (Hz)')
    
    all_allowed_freqs{kk}=allowed_freqs;
    all_allowed_dists{kk}=allowed_dists;
    m22(kk)=m2;

end

fm2=calc_fm2(ftilde_asymm,fm1,fpbh);                                        % calculate PDF for m2 (assumes f(m1)=1)

figure;scatter(Mcs,rate_model_indep,10,ftilde_equal,'o','filled'); colormap('jet'); colorbar;
set(gca,'xscale','log') ;set(gca,'yscale','log'); set(gca,'ColorScale','log'); set(gca,'FontSize',14)
xlabel('chirp mass (solar mass)'); ylabel('rate (kpc^{-3} yr^{-1})'); cblabel('fbar equal mass')

figure;scatter(m22,ftilde_asymm,10,fm2,'o','filled'); colormap('jet'); colorbar;
set(gca,'xscale','log') ;set(gca,'yscale','log'); set(gca,'ColorScale','log'); set(gca,'FontSize',14)
xlabel('m_2 (solar mass)'); cblabel('f(m_2)'); ylabel('f tilde asymm mass')

end

