function [orig_fs_UL,dist_pc,Mc_max_per_f,fff,mmm,ds,fff2,Mc_max_relevant,...
    Mcsss,fbar_equal_mass_one_num,rate_one_value_model_indep_allowed,fbar_asymm_mass_one_value,m22,fm2,all_allowed_freqs,all_allowed_dists  ] = get_constraints_from_uls(run,search,fdotmax )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
%%% https://journals.aps.org/prd/pdf/10.1103/PhysRevD.100.024004?casa_token=_TapPUnmY8gAAAAA%3Akb3lZLceg5TSx4wZBg0C8PBPtz4jLs4FZO-3Lixnhbrij0Pf__KF-dabvgVtckMUQXBg-vVXsjqV

%%% FH from 10-128 Hz --> TFFT = 8192 s, maxspinup is 2*10^9 Hz/s,
%%% Tobs=268.37 days
n=11/3;
consts=constants;
c=consts.c;
G=consts.G;
Msun=consts.Msun;

if ~exist('run','var')
    run='O2';
end

if ~exist('search','var')
    search='all-sky';
end
% 
if strcmp(run,'O2')
    Tobs=268.37*86400;
elseif strcmp(run,'O3a')
    Tobs=250*86400/2;
end
% Tobs=525600*60;

if strcmp(search,'all-sky')
    if strcmp(run,'O2')
%         fid=fopen('~/Downloads/ul_fh_02.txt');
        fid=fopen('~/Downloads/FH_95UL.txt');
        A = textscan(fid,'%s %f64');
        fs_UL=A{1};
        h0s_UL=A{2};
%         fs_UL=A{1};
%         fs_UL = sprintf('%s ', fs_UL{:});
%         fs_UL = sscanf(fs_UL, '%f');
%         h0s_UL=A{2}*1e-20;
    elseif strcmp(run,'O3a')
        A=readmatrix('~/Downloads/o3a_ul_all_simplified.csv');
        fs_UL=A(:,1);
        h0s_UL_circ=A(:,9);      %% circular polarization is ideal
        h0s_UL=h0s_UL_circ*2.30; %% from Keith; private communication; approx proxy for pop-avg upper limits is col. 9 * 2.30
    end
    if ~exist('fdotmax','var')
        if strcmp(run,'O2')
            fdotmax=2e-9; %% -1e-8 up to 512 Hz
            st=1;
        elseif strcmp(run,'O3a')
            fdotmax=1e-9; %1e-8 if consider abs(fdot`)
            st=10;
        end
    end
    
elseif strcmp(search,'gcL')
    TT=readtable('~/Downloads/Recalibrated_UL_L_95_readable.txt');
    fs_UL=TT.Var1;
    h0s_UL=TT.Var2;
    if ~exist('fdotmax','var')
        fdotmax=1.8e-9;%3.7e-11; %[?1.8 � 10?9 to +3.7e-11]
    end
elseif strcmp(search,'gcH')
    TT=readtable('~/Downloads/Recalibrated_UL_H_95_readable.txt');
    fs_UL=TT.Var1;
    h0s_UL=TT.Var2;
    if ~exist('fdotmax','var')
        fdotmax=3.7e-11;
    end
end

if iscell(fs_UL)
    fs_UL=str2double(fs_UL);
end
fs_UL=fs_UL(h0s_UL>0 & h0s_UL<1);
h0s_UL=h0s_UL(h0s_UL>0 & h0s_UL<1);

% ii=find(fs_UL<max(ff));
% fful=fs_UL(ii);
% h0s=h0s_UL(ii);
if strcmp(run,'O2')
    TFFT=8192;
elseif strcmp(run,'O3a')
    TFFT=7200;
end
fdotmin=1/TFFT/Tobs;%5.26e-12; %% step in sd
tffts=zeros(size(fs_UL));

if strcmp(search,'all-sky')

    if strcmp(run,'O2')
        fmax_db1=128; fmax_db2=512;fmax_db3=1024;

        tffts(fs_UL<=fmax_db1)=8192;
        tffts(fs_UL<=fmax_db2 & fs_UL>fmax_db1)=4096;
        tffts(fs_UL<=fmax_db3 & fs_UL>fmax_db2)=2048;
        tffts(fs_UL>=fmax_db3)=1024;
    elseif strcmp(run,'O3a')
        fmax_db1=475; fmax_db2=1475;
        tffts(fs_UL<=fmax_db1)=7200;
        tffts(fs_UL<=fmax_db2 & fs_UL>fmax_db1)=3600;
        tffts(fs_UL>fmax_db2)=1800;


    end
elseif strcmp(search,'gc')
    prop_constant=287146.905398613; %% Tfft=prop_const/sqrt(fmax)
    fmaxes=ceil(TT.Var1/10)*10;
    tffts=rndeven(prop_constant./sqrt(fmaxes));
end


ds=[1e-7 1e-6 1e-5 1e-4 0.001 0.01 0.05 0.1 1 10 100]; % kpc
% figure;
% ds=[1e-5 1e-4 1e-3 1e-2 1e-1 1];
ii=0;
% ds=logspace(-7,-2,1000);%1e-7:1e-7:1e-2;
allfs=[]; allMc=[];allds=[];
for i=1:length(ds)
    Mcs=calc_mc_from_h0(h0s_UL,ds(i),fs_UL);
    dotf=calc_fdot_chirp(Mcs,fs_UL);
    fs_after_tobs=power_law(Tobs/2,fs_UL,dotf,n); %Tobs/2 is at refernce time
    dot_f_after_tobs=calc_fdot_chirp(Mcs,fs_after_tobs);
    aaa=is_fevol_linear(Tobs,fs_UL,dotf,tffts,n);

%     aaa=is_fevol_linear(Tobs,fs_UL,dot_f_after_tobs,tffts,n);  %%%% ensures freqs corresp to \dot{f} at Tobs used to det lin
%     iii=dotf<=fdotmax & dotf>=fdotmin;
    iii=dot_f_after_tobs<=fdotmax;% & dotf>=fdotmin; %%% fdot at end  doesn't matter, only at beginning b/c ref time is there'
    bbb=aaa & iii;
    fff{i}=fs_UL(bbb);
    mmm{i}=Mcs(bbb);
%     semilogy(fff{i},mmm{i},'sq-')
%     hold on;
    try
        maxfs(i)=max(fff{i});
        ii=ii+1;
    catch
        continue;
    end
    
    allfs=[allfs; fs_UL(bbb)];
    allMc=[allMc; Mcs(bbb)];
    allds=[allds; ds(i)*ones(size(Mcs(bbb)))];
    
end

[fs_sorted,b]=sort(allfs);
Mcs_sorted=allMc(b);
ds_sorted=allds(b);

matr=[fs_sorted Mcs_sorted ds_sorted];
if strcmp(search,'all-sky')
    writetable(array2table(matr),strcat('~/Downloads/',run,'_f_mc_d_in_pc_95cl.txt'));
end

maxf=max(maxfs);
inds=fs_UL<=maxf;
adj_fdotmax=fdotmax-fdotmin/2;

grid_su=[fdotmin:fdotmin:fdotmax fdotmax];
for lll=1:length(grid_su) %%%%% try each spinup, determine the maximially allowed chirp mass, and save the spinups 
    su=grid_su(lll);      %%%%% that correspond to the maximum chirp mass, keep lin behavior and within 1 bin of fdotmax, all indepednent of h0
    Mc_temp=calc_mc_from_fdot(fs_UL,su);
    fffs=power_law(Tobs/2,fs_UL,su,n);
    newfdots=calc_fdot_chirp(Mc_temp,fffs);
    ikk=is_fevol_linear(Tobs,fs_UL,su,tffts,n);
    jjj=newfdots<=fdotmax+fdotmin/2 & newfdots>=fdotmin;
    ccc=ikk & jjj;

    Mc_max_per_f(ccc)=Mc_temp(ccc);
    sus(ccc)=su;
end


fff2=fs_UL(inds);
Mc_max_relevant=Mc_max_per_f(inds);

% semilogy(fff2,Mc_max_relevant)
% set(gca,'FontSize',14)
% xlabel('frequency (Hz)'); ylabel('minimum allowed chirp mass (M_{sun})'); grid on
% if ii==3
%     hl=legend(['d=' num2str(ds(1)*1000) ' pc'], ['d=' num2str(ds(2)*1000) ' pc'], ['d=' num2str(ds(3)*1000) ' pc'],['max $M_{c}$ from $\dot{f}_{max}$'])
% elseif ii==4
%     hl=legend(['d=' num2str(ds(1)*1000) ' pc'], ['d=' num2str(ds(2)*1000) ' pc'], ['d=' num2str(ds(3)*1000) ' pc'],['d=' num2str(ds(4)*1000) ' pc'],['maximum $M_c$ based only on max $\dot{f}'])
% end
% set(hl,'Interpreter','latex')
% set(hl,'FontSize',14)


% Mc_max=calc_mc_from_fdot(fs_UL,adj_fdotmax); %%% here the Mcs represent those whose maximum spinup in Tobs is fdotmax
% fffs=power_law(Tobs,fs_UL,adj_fdotmax,n);
% newfdots=calc_fdot_chirp(Mc_max,fffs);
% ikk=is_fevol_linear(Tobs,fs_UL,adj_fdotmax,tffts,n);
% jjj=newfdots<=fdotmax+fdotmin & newfdots>=fdotmin;
% ccc=ikk & jjj;
% Mc_other=calc_mc_from_fdot(fs_UL,fdotmax/1.3);
% sd_bins_off=(newfdots-fdotmax)/fdotmin;
% ggg=sd_bins_off<1;
% 
% fff=fs_UL(inds & ggg);
% Mc_max_relevant=Mc_max(inds & ggg);
% 
% semilogy(fff,Mc_max_relevant)
% set(gca,'FontSize',14)
% xlabel('frequency (Hz)'); ylabel('minimum allowed chirp mass (M_{sun})'); grid on
% 
% oo=is_fevol_linear(Tobs,fs_UL,fdotmax,tffts,n);
% 
% dd=calc_chirp_d(h0s_UL(oo),Mc_max(oo),fs_UL(oo)); %%%% MA

dd=calc_chirp_d(h0s_UL,Mc_max_per_f.',fs_UL); %%%% MA


% TO CALCULATE CONSTRAINT at fixed chirp mass as a function of frequency,
% and then sum
% Mcsss=logspace(-9,-5,50);%[1e-8 1e-7 1e-6 1e-5];
% Delta_f=fs_UL.^(-8/3)-fmaxes.^(-8/3);
delta_f=fdotmax*Tobs;
fband=max(fs_UL)-min(fs_UL);
prefact_asymm=5.3e-7; %% kpc^-3 yr^-1
prefact_equal=1.9e-6;
fsup_normfact=1;%2.5e-3;
% [fs_unique,ind]=unique(fs_UL,'stable');
if strcmp(run,'O3a')
    interp_fs=[min(fs_UL):delta_f:max(fs_UL)]'; %% only for comparison
else
    interp_fs=[min(fs_UL):delta_f:max(fs_UL)]';
end
h0s_UL_interp=interp1(fs_UL,h0s_UL,interp_fs);

orig_fs_UL=fs_UL;
fs_UL=interp_fs;
h0s_UL=h0s_UL_interp;
Mcsss=calc_mc_from_fdot(fs_UL,fdotmax);
fdotss=calc_fdot_chirp(Mcsss,fs_UL);
fmaxes=power_law(Tobs,fs_UL,fdotss,11/3);

tffts_interp(fs_UL<=fmax_db1)=7200;
tffts_interp(fs_UL<=fmax_db2 & fs_UL>fmax_db1)=3600;
tffts_interp(fs_UL>fmax_db2)=1800;

age_of_univ=13.6*1e9; %yrs
cutoff_time=age_of_univ/10*86400*365.25; % s

% Delta_f=fs_UL.^(-8/3)-fmaxes.^(-8/3);
% useMcs=logspace(-9,-5,1000);
% if strcmp(run,'O2')
%     useMcs=Mcsss; %I don't think this automatically works
% elseif strcmp(run,'O3a')
    useMcs=logspace(-7,-4,50);
    Mcsss=useMcs;
% end
rate_one_value_model_indep_allowed=zeros(1,length(useMcs));
rate_one_value_model_indep_allowed_asymm=rate_one_value_model_indep_allowed;
fbar_equal_mass_one_num=rate_one_value_model_indep_allowed;
m22=fbar_equal_mass_one_num;
fbar_asymm_mass_one_value=m22;
% Mcsss=useMcs;
figure;
for kk=1:length(useMcs)
    m1=2.5; %%% assume this motivated by QCD transition and events like GWXXXXX
    m2=(useMcs(kk) / m1^(2/5)).^(5/3);

    dists=calc_chirp_d(h0s_UL,useMcs(kk),fs_UL); %% in kpc
    fmaxes=power_law(Tobs,fs_UL,fdotmax,n);
    fdots=calc_fdot_chirp(useMcs(kk),fmaxes);
    allowed_indices_fdot=fdots<=fdotmax;
%    allowed_indices=useMcs(kk)<=Mcsss;
%     dists_cubed_per_allowed_chirp_mass=dists(allowed_indices).^3;
%     delta_T=5/256*pi^(-8/3)*(c^3./(G*useMcs(kk)*Msun)).^(5/3).*Delta_f';
%     delta_T=0.017*86400*365.25*Mcsss(kk)^(-5/3)*fs_UL.^(-11/3);
    mtot_equal=2^(6/5)*useMcs(kk);
    fisco_equal=calc_f_isco(mtot_equal);
    fisco_asymm_mass=calc_f_isco(m1); 
    linear_inds=is_fevol_linear(Tobs,fs_UL,fdotmax,tffts_interp',n);
    delta_T=calc_delta_T(useMcs(kk),fs_UL,fmaxes);
    
%     delta_T(delta_T<Tobs)=Tobs;
    
    delta_T_isco_equal=calc_delta_T(useMcs(kk),fs_UL,fisco_equal);
    delta_T_isco_asymm=calc_delta_T(useMcs(kk),fs_UL,fisco_asymm_mass);

%     delta_T_isco=calc_delta_T(useMcs(kk),fs_UL,fisco);
%     delta_T_isco=5/256*pi^(-8/3)*(c^3./(G*useMcs(kk)*Msun)).^(5/3).*(fs_UL.^(-8/3)-fisco.^(-8/3));
%     allowed_times_isco=delta_T_isco<cutoff_time;
    inds_allowed_times_equal=delta_T_isco_equal<cutoff_time;
    inds_allowed_times_asymm=delta_T_isco_asymm<cutoff_time;    

%     delta_T_allowed=delta_T(allowed_indices & allowed_times_equal)/(525600*60);
%     dists_cubed_per_allowed_chirp_mass=dists(allowed_indices & allowed_times_equal).^3;
%     delta_T_allowed=delta_T_allowed(delta_T_allowed<cutoff_time);
    allowed_dists=dists(allowed_indices_fdot & inds_allowed_times_equal & linear_inds);
    allowed_times=delta_T(allowed_indices_fdot & inds_allowed_times_equal & linear_inds)/(525600*60);
    rate_one_value_model_indep_allowed(kk)=calc_rate_VT(allowed_dists,allowed_times);
    
    allowed_dists_asymm=dists(allowed_indices_fdot & inds_allowed_times_asymm & fs_UL<fisco_asymm_mass & linear_inds);
    allowed_times_asymm=delta_T(allowed_indices_fdot & inds_allowed_times_asymm & fs_UL<fisco_asymm_mass & linear_inds)/(525600*60);
    
    rate_one_value_model_indep_allowed_asymm(kk)=calc_rate_VT(allowed_dists_asymm,allowed_times_asymm);
%    times=1;%(age_of_univ-delta_T_allowed)/age_of_univ;
%    summation_term=sum(dists_cubed_per_allowed_chirp_mass.* delta_T_allowed .* times.^(-34/37)) ;% * (delta_f ) /fband ;
%    rate_one_value_model_indep_allowed(kk)=(3 ./(4*pi*summation_term ) );
    
%     M=2^(6/5)*useMcs(kk);
    Mterm=(mtot_equal/1).^(-32/37);
    chirp_term=1;%(Mcsss(kk)/1).^(-68/37);
    
    fbar_equal_mass_one_num(kk) = (rate_one_value_model_indep_allowed(kk)./(Mterm.*chirp_term*prefact_equal*fsup_normfact)).^(37/53) ;
    
    

    m1_term=(m1/1).^(-32/37);
    m2_term=(m2/m1).^(-34/37);

    m22(kk)=m2;

    fbar_asymm_mass_one_value(kk) = (rate_one_value_model_indep_allowed(kk)'./(m1_term.*m2_term*prefact_asymm*fsup_normfact)).^(37/53) ;
    
%     alldists(kk,:)=dists;
%     loglog(useMcs(kk)*ones(size(allowed_dists)),allowed_dists,'sq'); hold on;
    allowed_freqs=fs_UL(allowed_indices_fdot & inds_allowed_times_equal & linear_inds);
    scatter(useMcs(kk)*ones(size(allowed_dists)),allowed_dists,2,allowed_freqs,'o','filled');colormap('jet'); colorbar; hold on;
    set(gca,'xscale','log') ;set(gca,'yscale','log'); set(gca,'ColorScale','log'); set(gca,'FontSize',14)
    xlabel('chirp mass (solar mass)'); ylabel('distance reach (kpc)'); cblabel('frequency (Hz)')
    
    all_allowed_freqs{kk}=allowed_freqs;
    all_allowed_dists{kk}=allowed_dists;
end

fm1=1;
fpbh=1;
fm2=fbar_asymm_mass_one_value.^(53/37)/(fm1*fpbh^(53/57)); %% for fsup=2.5e-4, 

figure;scatter(useMcs,rate_one_value_model_indep_allowed,10,fbar_equal_mass_one_num,'o','filled'); colormap('jet'); colorbar;
set(gca,'xscale','log') ;set(gca,'yscale','log'); set(gca,'ColorScale','log'); set(gca,'FontSize',14)
xlabel('chirp mass (solar mass)'); ylabel('rate (kpc^{-3} yr^{-1})'); cblabel('fbar equal mass')

figure;scatter(useMcs,fm2,10,fbar_asymm_mass_one_value,'o','filled'); colormap('jet'); colorbar;
set(gca,'xscale','log') ;set(gca,'yscale','log'); set(gca,'ColorScale','log'); set(gca,'FontSize',14)
xlabel('chirp mass (solar mass)'); ylabel('f(m_2)'); cblabel('fbar asymm mass')


% plot_triplets(fs_UL,dd*1000,Mc_max_per_f,'o',0,1,1)
% xlabel('frequency (Hz)'); ylabel('distance (pc)')
% cblabel('log maximum chirp mass detectable (M_{sun})')

% plot_triplets(Mc_max_per_f,dd*1000,fs_UL,'o',1,1,1)
% cblabel('log frequency (Hz)'); ylabel('distance (pc)')
% xlabel('maximum chirp mass detectable (M_{sun})')

% B=readmatrix('~/Downloads/rate_ftilde_O3a_unequalmass_fsup_1.txt');
% rate_nancy=(B(:,3));
% chirp_nancy=(B(:,1));
% ftilde_nancy=(B(:,4));
% figure;loglog(chirp_nancy,rate_nancy,'sq')
% hold on;loglog(useMcs,rate_one_value_model_indep_allowed_asymm,'o-k')
% 
% 
% C=readmatrix('~/Downloads/rate_ftilde_O3a_equalmass_fsup_1.txt');
% ftilde_nancy=C(:,3);
% chirp_nancy=C(:,1);
% rate_nancy=C(:,2);
% figure;loglog(chirp_nancy,rate_nancy,'sq')
% hold on;loglog(useMcs,rate_one_value_model_indep_allowed,'o-k')

dist_pc=dd*1000;


%%%%%%%%%%% for CDOs as in https://arxiv.org/pdf/1902.08273.pdf

% fact=1.4e-9;


% mD=((200./fs_UL).^(2/3).*(h0s_UL/fact)).^(3/5);


% kappa=5.3e6; %GPc^-3 yr^-1

% Tobsyr=Tobs/(525600*60); 
% Delta_f=1./tffts';
% fmaxes=power_law(Tobs,fs_UL',fdotss,11/3)';
% Delta_f=fs_UL'.^(-8/3)-fmaxes.^(-8/3);
% fmaxes=fs_UL+fdotmax*Tobs;
% fmaxes=fs_UL+1;

% fdotss=calc_fdot_chirp(Mc_max_per_f,orig_fs_UL);
% fmaxes=power_law(Tobs,orig_fs_UL,fdotss,11/3);
% Delta_f=orig_fs_UL.^(-8/3)-fmaxes.^(-8/3);
% Delta_t=5/256*pi^(-8/3)*(c^3./(G*Mc_max_per_f*Msun)).^(5/3).*Delta_f';
% % Delta_t=calc_time_to_coalescence(Mc_max_per_f,fs_UL');
% % 
% % t1=calc_time_to_coalescence(Mc_max_per_f',fs_UL);
% % t2=calc_time_to_coalescence(Mc_max_per_f',fs_UL+1./tffts);
% % Dt=abs(t2-t1);
% % Dt_yr=Dt/(525600*60);
% % Delta_t=525600*60;
% Delta_t_yr=Delta_t/(525600*60);
% age_of_univ=13.6*1e9; %yrs
% Delta_t_yr(Delta_t_yr>age_of_univ)=age_of_univ;
% 
% dcubed_kpc=(dd).^3;
% % dist_term=3./(4*pi*dcubed.*Delta_t'*kappa);
% M=2^(6/5)*Mc_max_per_f; %% Mc --> Mtot
% % M_term=M'/(1e-2);
% 
% rate_model_indep=(3 ./ (4*pi*dcubed_kpc.*Delta_t_yr')); %% rates in kpc^-3 yr^-1
% % delta_f=fdotmax*Tobs;
% % fband=max(fs_UL)-min(fs_UL);
% % rate_one_value_model_indep=(3 ./ ( sum(4*pi*dcubed_kpc.*Delta_t_yr') * (delta_f ) /fband ) ) ;
% 
% prefact=5.3e-7; %% kpc^-3 yr^-1
% fsup_normfact=2.5e-4;
% %%%%%%%% for equal mass
% 
% Mterm=(M/1).^(-32/37);
% chirp_term = (Mc_max_per_f/1).^(-68/37);
% rate_one_value_model_indep=(3 ./ ( sum(4*pi*dcubed_kpc.*Delta_t_yr) * (delta_f ) /fband ) ); 
% 
% fbar_equal_mass = (rate_model_indep'./(Mterm.*chirp_term*prefact*fsup_normfact)).^(37/53) ;
% 
% fbar_equal_mass_one_num_before = (rate_one_value_model_indep./(Mterm.*chirp_term*prefact*fsup_normfact)).^(37/53) ;
% 
% 
% 
% plot_triplets(Mc_max_per_f,rate_model_indep,fbar_equal_mass,'.',1,1,1)
% xlabel('chirp mass (M_{sun})'); ylabel('rate (kpc^{-3} yr^{-1})'); cblabel('f tilde (equal mass binaries)')
% % 
% 
% 
% plot_triplets(Mc_max_per_f,rate_model_indep,fbar_equal_mass_one_num_before,'.',1,1,1)
% xlabel('chirp mass (M_{sun})'); ylabel('rate (kpc^{-3} yr^{-1})'); cblabel('f tilde (equal mass binaries) for sum')
% 
% % figure;plot(Mc_max_per_f,fbar_equal_mass_one_num,'sq')
% % xlabel('chirp mass (M_{sun})'); ylabel('f tilde (equal mass binaries)')
% 
% %%%%%%% for asymm mass
% 
% m1=2.5; %%% assume this motivated by QCD transition and events like GWXXXXX
% m2=(Mc_max_per_f / m1^(2/5)).^(5/3);
% 
% m1_term=(m1/1).^(-32/37);
% m2_term=(m2/m1).^(-34/37);
% 
% 
% fbar_asymm_mass = (rate_model_indep'./(m1_term.*m2_term*prefact*fsup_normfact)).^(37/53) ;
% plot_triplets(m2,rate_model_indep,fbar_asymm_mass,'.',1,1,1)
% xlabel('m_2 (M_{sun})'); ylabel('rate (kpc^{-3} yr^{-1})'); cblabel('f tilde (EMRIs)')
% 


% fm1=1;
% fpbh=1;
% fm2=fbar_asymm_mass.^(53/37)/(fm1*fpbh^(53/57)); %% for fsup=2.5e-4, 
% 
% plot_triplets(m2,rate_model_indep,fm2,'.',1,1,1)
% xlabel('m_2 (M_{sun})'); ylabel('rate (kpc^{-3} yr^{-1})'); cblabel('f(m_2)')
% 
% 
% fpbh=0.1*(3 ./ (Tobsyr*kappa*4*pi*(dd*1e-3).^3) .* (Mc_max_per_f / (1e-2)).^(32/37) ).^(37/53);
% fpbh=0.1*(dist_term .* M_term.^(32/37) ).^(37/53);
% Rth = kappa*(M_term).^(-32/37).*(fpbh/0.1).^(53/37); %Gpc^-3 yr^-1
% 
% plot_triplets(Mc_max_per_f,fpbh,fs_UL,'.',1,1,1)
% xlabel('chirp mass (M_{sun})'); ylabel('f_{pbh}'); cblabel('log_{10} frequency (Hz)')
% % 
% plot_triplets(Mc_max_per_f,rate_model_indep,fpbh,'.',1,1,1)
% xlabel('chirp mass (M_{sun})'); ylabel('rate (Gpc^{-3} yr^{-1})'); cblabel('log_{10} frequency (Hz)')
% 
% plot_triplets(fs_UL, Mc_max_per_f,fpbh,'.',0,1,1)
% xlabel('chirp mass (M_{sun})'); ylabel('f_{pbh}'); cblabel('frequency (Hz)')
% figure;semilogy(fs_UL,mD)

% mcccs=Mc_max(inds);
% for jj=1:length(Mc_max(inds))
%     
% 
% 
% end
end

