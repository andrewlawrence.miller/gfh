function [] = pbh_search_do_hough(minf,maxf,t00,dur,TFFT,Mc,det,run,hough_outdir,where_am_i,peakmap_indir,which_hough)
%pbh_search_do_hough
% 

%   -----------------------------INPUTS:-----------------------------------
%   minf:           minimum frequency                                  (Hz)
%   maxf:           maximum frequency, typically minf+10               (Hz)
%   t00:            start time of analysis                          (GPS s)
%   dur:            duration of signal                                  (s)
%   TFFT:           Fast Fourier Transform time                         (s)
%   Mc:             chirp mass                                       (Msun)
%   det:            detector ('L','H','V')
%   run:            which observing run ('O2','O3')
%   outdir:         where the peakmap should be saved
%   where_am_i:     cluster on which you run ('laptop','louvain','cnaf')

%   -----------------------------OUTPUTS:----------------------------------


%   One summary file containing:


%   -----------------------------EXAMPLE:----------------------------------


n=11/3;
ref_perc_time=0.5;

if isdeployed
    minf=str2num(minf)
    maxf=str2num(maxf)
    t00=str2num(t00)
    dur=str2num(dur)
    TFFT=str2num(TFFT)
    Mc=str2num(Mc)
    det
    run
    hough_outdir
    where_am_i
    which_hough
end


str_fmin=sprintf( '%04d', minf );
str_fmax=sprintf( '%04d', maxf );
str_t00=num2str(t00);
str_tfft=sprintf( '%06d', TFFT );
str_Mc=sprintf( '%04d', Mc );


fname=strcat('peakmap_',str_fmin,'_',str_fmax,'_',str_t00,'_',str_tfft,'_',str_Mc,'_.mat');             % save coincidences and candidates


load(strcat(peakmap_indir,fname))

if strcmp(data_struct,'sfdb')
    
    rootkcand=5;
    basic_info=job_pack_0.basic_info;                                           % structure that contains basic information about the peakmap    


    [SLong, SLat]=astro_coord('equ','ecl',sour.a,sour.d);                       % conversation of equatorial to ecliptic sky coordinates

    p=job_pack_0.peaks;                                                         % the peaks in time/frequency, with corresponding amps

    if plot_flag==1
        plot_triplets(86400*(p(1,:)-p(1,1)),p(2,:),p(3,:))
        xlabel('time (s)'); ylabel('frequency (Hz)'); cblabel('equalized spectrum')
        set(gca,'FontSize',14)
    end
    pout=andrew_hfdf_patch(p,basic_info,[SLong SLat]);                          % Doppler correction

    hm_job.minf=minf;                                                           % minimum frequency to do the Hough on
    hm_job.maxf=maxf;                                                           % maximum frequency to do the Hough on
    hm_job.df=1/TFFT;                                                           % step in frequency
    hm_job.dur=dur;
    hm_job.patch=[SLong SLat];
    hm_job.n=n;
    hm_job.ref_perc_time=ref_perc_time;

    if strcmp(which_hough,'gfh')
        hm_job.frenh=1;
        fdotmin = calc_fdot_chirp(Mc,minf);
        fdotmax = calc_fdot_chirp(Mc,maxf);
        [gridk,~]=andrew_long_transient_grid_k(TFFT,[minf maxf],[fdotmin fdotmax],dur,n);
        hm_job.gridk=gridk;
        tic;
        [hfdf,hm_job]=hfdf_hough_transients(pout,hm_job);                       % does the Generalized Hough with nonuniform k grid, uniform x grid
        toc;

        [cand2, job_info]=hfdf_peak_transients(hfdf,hm_job,rootkcand,0,0);      % selects candidates in every box of hough map of size rootkcand x rootkcand
    elseif strcmp(which_hough,'fh')
        hm_job.oper='noadapt';                                                  % The Hough is nonadaptive, meaning that the antenna pattern is not considered
        kcand=20;                                                               % 20 candidates are selected from the Frequency-Hough map uniformaly in f and fdot
        hm_job.frenh=10;                                                        % Frequency-Hough uses overesolution factor of frequency to improve sensitivity
        hm_job.sd=construct_grid_sd_parms(sour.df0);                            % Creates spin-up or spin-down grid for Frequency-Hough
        [hfdf,~,~,hm_job]=fu_transients_hough(pout,hm_job,ref_perc_time);       % Does the Frequency-Hough
        [cand2, job_info]=fu_transients_hfdf_peak(hfdf,hm_job, kcand);          % selects candidate in Frequency-Hough map

    else
        disp('hough not going to work, exitting')
        return;
    end

end

save(fname,'hfdf','hm_job','cand2','minf','maxf','t00','dur','TFFT','Mc',...
    'det','run','hough_outdir','where_am_i','peakmap_indir','which_hough')

copy_file(fname,hough_outdir,maxx,where_am_i)

end

