function [ftilde] = calc_ftilde_asymm_mass(rate,m1,m2,fsup)
% Calculates effective parameter ftilde for the asymmetric mass case,
% equations 8 and 10
% 
% rate: the rate of binary inspirals, (kpc^-3 yr^-1)
% m1: mass of primary component (solar masses)
% m2: mass of secondary component (solar masses), m2<<m1
% fsup: suppression factor of rates due to gravitational influence of early
% forming PBH clusters, nearby PBHs and matter inhomogeneities 
% ftilde: effective parameter that relates to fraction of DM that PBHs
% could compose

prefact_asymm=5.3e-7; %% kpc^-3 yr^-1

m1_term=(m1/1).^(-32/37);
m2_term=(m2/m1).^(-34/37);
    
ftilde = (rate./(m1_term.*m2_term*prefact_asymm*fsup)).^(37/53) ;
    
end



