function [outputArg1,outputArg2] = pbh_det_sens_parm_space(inputArg1,inputArg2)
%UNTITLED Summary of this function goes here
%   Given the different f0s/ Mcs /Tobs/ TFFTs to search over in the loaded
%   mat file, what other chirp masses / f0s are we sensitiive to, and at
%   what cost
load pbh_search_parmspace.mat
max_num_skypts = 10;
skypts_to_search = skypts'<max_num_skypts;

cc = constants;
Tobs_tot = 365*86400*(1+20/24);

f0s_to_search = f0(points_to_search & skypts_to_search);
Mcs_to_search = Mc(points_to_search & skypts_to_search);
f_fin_to_sear = f_fin(points_to_search & skypts_to_search);
tim_to_search = t_fdotbest(points_to_search & skypts_to_search);
TFFT_to_searc = TFFT(points_to_search & skypts_to_search);
dmax_to_searc = dmax(points_to_search & skypts_to_search)/cc.units.kpc_to_m;
fdot_min_sear = fdot_min(points_to_search & skypts_to_search);
fdot_max_sear = fdot_max(points_to_search & skypts_to_search);
 




jj = 2;
index(1) = 1;
for ii = 1:length(f0s_to_search) - 1
    f000 = f0s_to_search(ii);
    Mc00 = Mcs_to_search(ii);
    fend = f_fin_to_sear(ii);
    Tobs = tim_to_search(ii);
    TFFT0 = TFFT_to_searc(ii);
    fdotmin = fdot_min_sear(ii);
    fdotmax = fdot_max_sear(ii);


    dist = dmax_to_searc(ii);

    dist_next = dmax_to_searc(ii+1);
    f0_next = f0s_to_search(ii+1);
    Mc_next = Mcs_to_search(ii+1);

    if abs(f000 - f0_next) < 1 && (dist_next-dist)/dist < 0.05
        index(jj) = index(jj-1);
    else
        index(jj) = ii;
    end
    jj = jj + 1;

    [gridk,~]=andrew_long_transient_grid_k(TFFT0,[f000 fend],[fdotmin fdotmax],Tobs,11/3);

    mc_grid = calc_mc_with_k(gridk);
    mc_min(ii) = min(mc_grid);
    mc_max(ii) = min( max(mc_grid), Mc00);

    all_mcgrid{ii} = mc_grid(mc_grid<=mc_max(ii));

end

u_ind = unique(index);

matrix = [];
for kk = 1:length(u_ind)-1
    group_inds = index(u_ind(kk):u_ind(kk+1)-1);
    group_f0s = f0s_to_search(group_inds);
    group_f_fin = f_fin_to_sear(group_inds);
    group_tfft = TFFT_to_searc(group_inds);
    group_tobs = tim_to_search(group_inds);
    group_mcmin = mc_min(group_inds);
    group_mcmax = mc_max(group_inds);
    group_dmaxs = dmax_to_searc(group_inds);
    matrix = [matrix; min(group_f0s) max(group_f_fin) min(group_tfft)...
        min(group_tobs) max(group_mcmin) min(group_mcmax) min(group_dmaxs) ];

    fffff(kk) = min(group_f0s);
    ffmax(kk) = max(group_f_fin);
end

Njobs = ceil(Tobs_tot./matrix(:,4));
Njobs_tot = sum(Njobs);

figure;semilogy(matrix(:,end-2),'sq')
hold on; semilogy(matrix(:,end-1),'sq')

collect_fs = [];
collect_Mc =[];
collect_tfft = [];
collect_tobs = [];
figure; hold on;
for ll = 1:length(u_ind)-1
    loglog(fffff(ll)*ones(size(all_mcgrid{u_ind(ll)})),all_mcgrid{u_ind(ll)},'o')
%     loglog(ffmax(ll)*ones(size(all_mcgrid{u_ind(ll)})),all_mcgrid{u_ind(ll)},'o')
    collect_fs = [collect_fs fffff(ll)*ones(size(all_mcgrid{u_ind(ll)}))];
    collect_Mc = [collect_Mc all_mcgrid{u_ind(ll)}];
    collect_tfft = [collect_tfft matrix(ll,3) * ones(size(all_mcgrid{u_ind(ll)}))];
    collect_tobs = [collect_tobs matrix(ll,4) * ones(size(all_mcgrid{u_ind(ll)}))];
end
set(gca,'XScale','log')
set(gca,'YScale','log')

figure;scatter((collect_fs),collect_Mc,6,collect_tfft,'o','filled');colormap('jet'); colorbar; hold on;
set(gca,'YScale','log')
set(gca,'XScale','log')

figure;scatter((collect_fs),collect_Mc,6,collect_tobs,'o','filled');colormap('jet'); colorbar; hold on;
set(gca,'YScale','log')
set(gca,'XScale','log')
set(gca,'ColorScale','log')


end