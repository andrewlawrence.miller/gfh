function [] = pbh_search_make_peakmaps(minf,maxf,t00,dur,TFFT,Mc,det,run,outdir,where_am_i)
%pbh_search_make_peakmaps
% This code creates peakmaps (time/frequency maps) for a given Fourier
% Transform time and observation time.

%   -----------------------------INPUTS:-----------------------------------
%   minf:           minimum frequency                                  (Hz)
%   maxf:           maximum frequency, typically minf+10               (Hz)
%   t00:            start time of analysis                          (GPS s)
%   dur:            duration of signal                                  (s)
%   TFFT:           Fast Fourier Transform time                         (s)
%   Mc:             chirp mass                                       (Msun)
%   det:            detector ('L','H','V')
%   run:            which observing run ('O2','O3')
%   outdir:         where the peakmap should be saved
%   where_am_i:     cluster on which you run ('laptop','louvain','cnaf')

%   -----------------------------OUTPUTS:----------------------------------


%   One summary file containing:
%       job_pack_0:      cell containing Ncand/detector, cell size 1 x det
%       coin:       coincidence structure for all combos of detectors
%       fbin_thr:   max number of frequency bins a cand can be from inj
%       tfft0:      FFT length used to calculate  peakmap, optimal for maxf
%       Sn:         estimation of background PSD
%       fbins:      frequency bins into which peaks are projected
%       counts:     histogrammed peak counts per frequency bin
%       veto:       frequency bins to veto, boolean array, 1: veto, 0: keep
%       All input parmaeters to dp_search

%   -----------------------------EXAMPLE:----------------------------------

% dp_search( 720,730,0.000766667,0.00181459,20,'ligol','O3','./','louvain')


if isdeployed
    minf=str2num(minf)
    maxf=str2num(maxf)
    t00=str2num(t00)
    dur=str2num(dur)
    TFFT=str2num(TFFT)
    Mc=str2num(Mc)
    det
    run
    outdir
    where_am_i
end


det_run_string=[det '/sfdb/' run 'C01/GAT_CL60/'];

if strcmp(where_am_i,'cnaf')
    basedir='/storage/gpfs_virgo4/virgo4/virgo/RomePSS/';
elseif strcmp(where_am_i,'louvain')
    basedir='/nfs/user/gw/';
elseif strcmp(where_am_i,'rome')
elseif strcmp(where_am_i,'nikhef')
end

if maxf<=128                %% these are the frequencies of the SFDBs
    f_str='128';
    tfft_db=8192;
elseif maxf>128 && maxf<=512
    f_str='512';
    tfft_db=4096;
elseif maxf>512 && maxf<=1024
    f_str='1024';
    tfft_db=2048;
elseif maxf>1024 && maxf<=2048
    f_str='2048';
    tfft_db=1024;
elseif maxf>2048
    disp('maximum frequency greater than 2048 Hz, not going to work, exiting')
    return;
end

if strcmp(det,'L')
    det_for_bsd='ligol';
elseif strcmp(det,'H')
    det_for_bsd='ligoh';
elseif strcmp(det,'V')
    det_for_bsd='virgo';
else
    disp('detector name invalid, not going to work,exiting')
    return;
end


bw=10;
maxx=0;


str_fmin=sprintf( '%04d', minf );
str_fmax=sprintf( '%04d', maxf );
str_t00=num2str(t00);
str_tfft=sprintf( '%06d', TFFT );
str_Mc=sprintf( '%04d', Mc );


fname=strcat('peakmap_',str_fmin,'_',str_fmax,'_',str_t00,'_',str_tfft,'_',str_Mc,'_.mat');             % save coincidences and candidates

if tfft_db<=TFFT || (maxf-minf)<=bw                                           %% if true, use BSDs
    disp('using BSDs')
    data_struct='bsd';
    
    if dur<=tobs 
        tini=gps2mjd(t00);
        tfin=gps2mjd(t00+dur);
        this_bsd=bsd_lego(basedir,det_for_bsd,run,[tini tfin],[minf maxf],2);
        this_bsd=cut_bsd(this_bsd,[tini tfin]);
    else
        this_bsd=bsd_lego(basedir,det_for_bsd,run,1,[minf maxf],2); 
    end
    
    dt=dx_gd(this_bsd);
    lfft=rndeven(TFFT/dt);
    tfstr=bsd_peakmap(this_bsd,lfft);
    
    
    save(fname,'tfstr','data_struct','minf','maxf','t00','dur','TFFT','Mc','det','run','outdir','where_am_i')
else
    disp('using SFDBs')
    data_struct='sfdb';
    sfdbdir=strcat(basedir,det_run_string,f_str);                              % main directory where SFDBs are
    job_pack_0=inject_power_law_signal([],[minf maxf],t00,dur,TFFT,sfdbdir);   % does the injection of sour in freq band, makes peakmap with TFFT



    
    save(fname,'job_pack_0','data_struct','minf','maxf','t00','dur','TFFT','Mc','det','run','outdir','where_am_i')

end
copy_file(fname,outdir,maxx,where_am_i)


end

