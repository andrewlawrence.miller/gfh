function [fm2] = calc_fm2(ftilde,fm1,fpbh)
% Calculate the probability density function for m2 in the asymmetric mass
% ratio case, eq. 8
% 
% ftilde:       the effective parameter related to fpbh
% fm1:          the probabilty density function of m1
% fpbh:         the fraction of dark matter that PBHs could compose
% fm2:          the probability density function for m2



fm2=ftilde.^(53/37)/(fm1*fpbh^(53/57));

end

