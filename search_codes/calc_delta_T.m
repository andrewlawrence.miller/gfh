function [delta_T] = calc_delta_T(Mc,fs_UL,fmaxes)
%Calculates the time of a binary system to go from one frequency to another
%as it inspirals, equation 5
% 
% Mc:       chirp mass
% fs_UL:    the initial frequencies (Hz)
% fmaxes:   the final frequencies (Hz)
% delta_T:  time to get from fs_UL to fmaxes

consts=constants;
c=consts.c;
G=consts.G;
Msun=consts.Msun;
Delta_f=fs_UL.^(-8/3)-fmaxes.^(-8/3);


delta_T=5/256*pi^(-8/3)*(c^3./(G*Mc*Msun)).^(5/3).*Delta_f;



end

