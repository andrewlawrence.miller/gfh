function [ftilde] = calc_ftilde_equal_mass(rate,Mtot,fsup)
%Calculates effective parameter ftilde for the equal mass case, 
% equations 8  and 9
% 
% rate: the rate of binary inspirals, (kpc^-3 yr^-1)
% Mtot: total mass (solar masses)
% fsup: suppression factor of rates due to gravitational influence of early
% forming PBH clusters, nearby PBHs and matter inhomogeneities 
% ftilde: effective parameter that relates to fraction of DM that PBHs
% could compose

prefact_equal=1.9e-6; %kpc^-3 yr^-1
Mterm=(Mtot/1).^(-32/37);

ftilde=(rate./(Mterm.*prefact_equal*fsup)).^(37/53) ;

end

