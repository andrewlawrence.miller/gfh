function [ alpha ] = calc_sat_amp( f0,fdot0,n )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

taugr=-3.3;
G=6.67e-11;

if ~exist('Q','var')
    Q=9.4*10^-2; %polytropic equation of state, constant, unitless
end

if ~exist('rho','var')
   rho=5.5*10^17; %kg/m^3 constant density of NS 
end


k=fdot0/f0^n;

alpha=sqrt((k*taugr*(pi*G*rho)^3)/(-2*Q*(2*pi*(3/4))^6));

end

