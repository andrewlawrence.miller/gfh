function [ nameFolds ] = get_subdir_list( parentdir )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

d = dir(parentdir);
isub = [d(:).isdir]; %# returns logical vector
nameFolds = {d(isub).name}';
nameFolds(ismember(nameFolds,{'.','..'})) = [];

end

