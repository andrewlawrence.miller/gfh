function [rho] = cbc_calc_mf_snr(fs,Sn,Mc,d,Q)
%UNTITLED4 Msun: solar masses; d : Mpc; or can be h0
%   Maggiore 7.181
% this is the optimal snr, calculated as rho = 4 * integral(|h(f)|^2 / Sn)
% for quasi-Newtonian compact object inspirals

cc = constants;
c = cc.c;
G = cc.G;
Msun = cc.Msun;
if ~exist('d','var')
    d = 1; %%% Mpc
end
if ~exist('Q','var')
    Q = (2/5)^2;
end
f0 = min(fs);
if d<1e-10 % if d is really h0
    d = calc_chirp_d(d,Mc,f0) * cc.units.kpc_to_m / cc.units.mpc_to_m;
end

term1 = 2/5 * sqrt(5/6) * 1/pi^(2/3) * sqrt(Q) / (2/5);

term2 = ( c / ( d * cc.units.mpc_to_m) ) * (G * Mc * Msun / c^3).^(5/6);

df = [0 diff(fs)]; 

term3 = sqrt( sum(fs.^(-7/3) ./ Sn .* df )) ;

rho = term1 * term2 * term3;

end