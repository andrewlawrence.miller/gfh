function [ Nd ] = calc_ND( f0,tfft )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
consts=constants;

c=consts.c;
vorb=consts.v_earth_orb;

vc=vorb/c;
Nd=f0.*vc.*tfft;

end

