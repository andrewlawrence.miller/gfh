function [ did_it_load,source,freq_band,t00 ] = load_a_file(fnameout,where_am_i,fileoutdir,maxx)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here

if ~exist('maxx','var')
    maxx=1;
end

if strcmp(where_am_i,'nikhef')
    [status,result]=copy_a_file_to_local_node(fnameout,fileoutdir,maxx)
    if status==0
        try
            load(fnameout)
            did_it_load=1;
        catch
            disp('file is empty')
            did_it_load=0;
        end
    end
elseif strcmp(where_am_i,'cnaf')
    dirrr='/storage/gpfs_virgo4/virgo4/virgo/RomePSS/';
    file_to_load=strcat(dirrr,fileoutdir,fnameout);
    try
        load(file_to_load)
        did_it_load=1;
    catch
        disp('load failed, file is empty')
        did_it_load=0;
    end
    
elseif strcmp(where_am_i,'laptop')
    if strcmp(fnameout(3:6),'cand')
        basedir='/Users/andrewmiller/Desktop/full_sensitivity_estimation/HOUGH/';
    else
        basedir='/Users/andrewmiller/Desktop/full_sensitivity_estimation/INJECT_SIGNALS/';
    end
    basedir=fileoutdir;
    fullpath=strcat(basedir,fnameout);
    try
        load(fullpath)
        did_it_load=1;
    catch
        disp('load failed')
        did_it_load=0;
    end
    
    
elseif strcmp(where_am_i,'rome')
    fullpath=strcat(fileoutdir,fnameout);
    try
        load(fullpath)
        did_it_load=1;
    catch
        disp('load failed')
        did_it_load=0;
    end
    
end

if exist('job_pack_0','var')
    freq_band=job_pack_0.freq_band;
    t00=mjd2pgs(job_pack_0.basic_info.tim(1));
else
    freq_band=[];
    t00=[];
end

if ~exist('source','var')
    source=[];
end

if exist('job_pack_0_all','var')
    if iscell(job_pack_0_all)
        ind=length(job_pack_0_all);
        try
            t00=mjd2gps(job_pack_0_all{1}{ind}.basic_info.tim(1));
        catch
            t00=mjd2gps(job_pack_0_all{ind}.basic_info.tim(1));
        end

    end
else
    t00=[];
end


