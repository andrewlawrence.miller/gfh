function [this_bsd]=load_bsd(where_am_i,minf,maxf,run,det,callib)

if ~exist('where_am_i','var')
    where_am_i='laptop';
end

if ~exist('minf','var')
    minf=740;
    maxf=741;
end
if ~exist('run','var')
    run='O2';
end
if ~exist('det','var')
    det='ligol';
end

if ~exist('callib','var')
    callib = '';
end



if strcmp(where_am_i,'laptop')
    bsd_basedir='~/Desktop';
    if strcmp(run,'O2')
        tini=57845;
        tfin=tini+1/2;
    elseif strcmp(run,'O3')
%         tini = 5.858202053240741e+04;
        tini = 58665.096088 ; % for high-frequency
        tfin=tini+2.15e5/86400;
    end

    if ~strcmp(callib,'')
        run = [callib '_' run];
    end
    this_bsd=bsd_lego(bsd_basedir,det,run,[tini tfin],[minf maxf],1);
    this_bsd=cut_bsd(this_bsd,[tini tfin]);
elseif strcmp(where_am_i,'louvain')
     bsd_basedir='/nfs/user/gw'
     this_bsd=bsd_lego(bsd_basedir,det,run,1,[minf maxf],2);
end
