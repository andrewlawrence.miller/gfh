function [hm_job] = make_hm_job_struct...
    (minf,maxf,TFFT,dur,n,ref_perc_time,gridk)
%UNTITLED40 Summary of this function goes here
%   Detailed explanation goes here
hm_job.minf=minf;                                                           % minimum frequency to do the Hough on
hm_job.maxf=maxf;                                                           % maximum frequency to do the Hough on
hm_job.df=1/TFFT;                                                           % step in frequency
hm_job.dur=dur;
hm_job.patch = [0 0];
hm_job.n=n;
hm_job.ref_perc_time=ref_perc_time;
hm_job.frenh=1;
hm_job.gridk=gridk;

end