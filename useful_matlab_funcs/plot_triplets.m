function plot_triplets(x,y,z,marker,flag_logx,flag_logy,flag_log_cb,colorm)
%PLOT_TRIPLETS plots many points with different colors
%              faster than plot_manypoints
%
%          plot_triplets(x,y,z,marker,colorm)
%
%      x,y,z      triplets; x,y coordinates, z amplitude
%      marker     'x', 'o','+','.','<','>','^','v','s','d',...
%      colormap   default 'cool'

% Version 2.0 - August 2006
% Part of Snag toolbox - Signal and Noise for Gravitational Antennas
% by Sergio Frasca - sergio.frasca@roma1.infn.it
% Department of Physics - Universita` "La Sapienza" - Rome

if ~exist('flag_logx','var')
    flag_logx=0;
end
if ~exist('flag_logy','var')
    flag_logy=0;
end
if ~exist('flag_log_cb','var')
    flag_log_cb=0;
end

fig = get(groot,'CurrentFigure');
if ~isempty(fig)
    figure;
end
if flag_log_cb==1
    z=log10(z);
end
mi=min(z);
ma=max(z);

mami=ma-mi;
if mami <= 0
    mami=1
    disp(' Attention ! mami = 1')
end

if ~exist('marker','var')
    marker='.';
end

if ~exist('colorm','var')
    colorm='hot';
end

cm=colormap(colorm); 

% figure
nc=length(cm);
zz=floor(nc*0.9999*(z-mi)/mami+1);

for i = 1:nc
    col=cm(i,:);
    if flag_logx==1 && flag_logy~=1
        semilogx(x(zz==i),y(zz==i),marker,'MarkerFaceColor',col,'MarkerEdgeColor',col),hold on
    elseif flag_logy==1 && flag_logx~=1
        semilogy(x(zz==i),y(zz==i),marker,'MarkerFaceColor',col,'MarkerEdgeColor',col),hold on
    elseif flag_logy==1 && flag_logx==1
        loglog(x(zz==i),y(zz==i),marker,'MarkerFaceColor',col,'MarkerEdgeColor',col),hold on
    else
        plot(x(zz==i),y(zz==i),marker,'MarkerFaceColor',col,'MarkerEdgeColor',col),hold on
    end
end
%if flag_log_cb==1
    %caxis(log10([min(z) max(z)]))
%else
    caxis([min(z) max(z)])
%end
colormap(colorm),colorbar
set(gca, 'FontSize', 14)
%xlabel('time (seconds)')
%ylabel('frequency (Hz)')
grid on