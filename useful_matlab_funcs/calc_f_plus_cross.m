function [ fplus,fcros ] = calc_f_plus_cross(det,alpha,delta, psi,phir,t )
%UNTITLED12 Summary of this function goes here
%   Detailed explanation goes here

omegar=2*pi/86400;

alpha=deg2rad(alpha);
delta=deg2rad(delta);
psi=deg2rad(psi);

if det==0
    lambda=deg2rad(30.56);
    gamma=deg2rad(243);
    L=deg2rad(90.77);
    zeta=deg2rad(90);
elseif det==1
    lambda=deg2rad(46.45);
    L=deg2rad(119.41);
    gamma=deg2rad(171.8);
    zeta=deg2rad(90);
end

if ~exist('phir','var')
    phir=0;
end

if ~exist('t','var')
    t=0;
end

a1=1/16*sin(2*gamma)*(3-cos(2*lambda))*(3-cos(2*delta))*cos(2*(alpha-phir-omegar*t));
a2=-1/4*cos(2*gamma)*sin(lambda)*(3-cos(2*delta))*sin(2*(alpha-phir-omegar*t));
a3=1/4*sin(2*gamma)*sin(2*lambda)*sin(2*delta)*cos(alpha-phir-omegar*t);
a4=-1/2*cos(2*gamma)*cos(lambda)*sin(2*delta)*sin(alpha-phir-omegar*t);
a5=3/4*sin(2*gamma)*cos(lambda)^2*cos(delta)^2;

a=a1+a2+a3+a4+a5;

b1=cos(2*gamma)*sin(lambda)*sin(delta)*cos(2*(alpha-phir-omegar*t));
b2=1/4*sin(2*gamma)*(3-cos(2*lambda))*sin(delta)*sin(2*(alpha-phir-omegar*t));
b3=cos(2*gamma)*cos(lambda)*cos(delta)*cos(alpha-phir-omegar*t);
b4=1/2*sin(2*gamma)*sin(2*lambda)*cos(delta)*sin(alpha-phir-omegar*t);

b=b1+b2+b3+b4;



fplus=sin(zeta)*(a*cos(2*psi)+b*sin(2*psi));
fcros=sin(zeta)*(b*cos(2*psi)-a*sin(2*psi));

end

