function [index] = efficient_index_noise_curve(fsss,det_freq)
%UNTITLED13 Summary of this function goes here
%   Detailed explanation goes here
[~,index] = (min(abs(fsss(:)'-det_freq(:)),[],1));
end