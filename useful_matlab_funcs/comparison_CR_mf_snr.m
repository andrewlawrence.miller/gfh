function [cr_minfs,mf_minf,...
    mf_minfs,min_h0_mf,min_h0_CR,...
    matrix_2Hz_comparison ,matrix_5Hz_comparison] =...
    comparison_CR_mf_snr(CR_thr,mf_thr,mc,ampnoise,maxf,base_fmin)
%comparison of comp cost reduction at fixed sensitivity, and improvement in
%sensitivity at fixed computational cost
% maxf <= 12 or maxf=20

if ~exist('CR_thr','var')
    CR_thr = 3;  %%% for single detector, https://arxiv.org/abs/2303.15923, in CW section
end

if ~exist('mf_thr','var')
    mf_thr = 8; %maggiore p. 359 for single detector
end

if ~exist('ampnoise','var')
    ampnoise=7.94E-04 * 1e-20;
end

if ~exist('maxf','var')
    maxf = 20;
end

if ~exist('mc','var')
    mc = 1;
end

if ~exist('base_fmin','var')
    base_fmin = 2;
end


minfs = base_fmin:maxf-1;
if max(minfs)>15
    minfs = base_fmin:1:15;
end
h0s = logspace(-25,-23,100);

n = 11/3;

Tobs = 365.25 * 86400;
time_per_pt=144.163031/(1859*1434) ;                                         % t/ (nbin_k * nbin_x) lasting 2e4 s covering 50 Hz, EMPIRICAL, found by running Generalized Frequency Hough at Louvain

Mcmax = 3; %%% to cover at least the same range as mf
Mcmin = 1;

Npts_parmspace = zeros(size(minfs));
comp_time_one_dur = Npts_parmspace;
comp_time_tobs = Npts_parmspace;

rho = zeros(length(h0s),length(minfs));
CRs = rho; rho_old = rho;
durr = Npts_parmspace;
min_h0_CR = Npts_parmspace;
min_h0_mf = Npts_parmspace;

for f = 1:length(minfs)

    fdotmin=calc_fdot_chirp(mc,minfs(f));
    fdotmax=calc_fdot_chirp(mc,maxf);

    t1=calc_time_to_coalescence(mc,minfs(f));      % seconds until coalescence at min and max frequency injected
    t2=calc_time_to_coalescence(mc,maxf);
    dur = abs(t2-t1);

%     TFFTmax = floor(1 / sqrt(fdotmax));                                             % calculate TFFT using maximum spinup

    TFFT = 2^floor(log2(rndeven(1/sqrt(fdotmax))));

    [gridk,~]=andrew_long_transient_grid_k(TFFT,[minfs(f) maxf],[fdotmin fdotmax],dur,n);
    mcs = calc_mc_with_k(gridk);
    gridk = gridk(mcs < Mcmax & mcs > Mcmin);
    
    Npts_parmspace(f) = length(gridk) * length(minfs(f):1/TFFT:maxf);
    N_ops = length(gridk) * dur/(TFFT/2);
    comp_time_one_dur(f) = N_ops * time_per_pt;
    comp_time_tobs(f) = comp_time_one_dur(f) * Tobs/dur;
    
%     min_max_mc(f,:) = [min(mcs(mcs<mc+0.5)) max(mcs(mcs<mc+0.5))];
    for h0 = 1:length(h0s)
        [rho(h0,f),CRs(h0,f),durr(f)] = wrapper_cbc_calc_mf_snr(minfs(f),maxf,mc,h0s(h0),ampnoise,base_fmin);
    end

    try
        min_h0_CR(f) = min(h0s(CRs(:,f)>CR_thr));
    catch
        min_h0_CR(f) = inf;
    end
    try
        min_h0_mf(f) = min(h0s(rho(:,f)>mf_thr));
    catch
        min_h0_mf(f) = inf;
    end
end
% min_h0_CR = min_h0_CR(1) * sqrt(CRs((min(find(CRs(:,1)>CR_thr))),:) /CRs(min(find(CRs(:,1)>CR_thr)),1));
% 
% figure;semilogy(h0s,CRs(:,1),'b') ; yline(3,'b')
% hold on; semilogy(h0s,rho(:,10),'k') ; yline(8,'k')

mf_minfs = minfs;%(1:length(min_h0_mf));
second_f = base_fmin+3;
% figure;semilogy(mf_minfs,min_h0_mf,'b.-') %%% min_h0_mf calc at rho>mf_thr = 5
% hold on; yline(min_h0_CR(minfs==base_fmin),'g--')
% yline(min_h0_CR(minfs==second_f),'r--') ; 
% yline(min_h0_CR(minfs==10),'k--')

% plot(cr_minfs,mf_minf(mf_minf>0),'.-k')
% figure;plot(cr_minfs,mf_minf,'.-g')


%%%%% comp cost

%%%%% numbers from https://arxiv.org/pdf/0910.0380.pdf

% Ntemp_mf_et = [150000 80000 40000]; %% https://arxiv.org/pdf/0910.0380.pdf, %MM = 0.95 for [1-3 Msun]
% Ntemp_mf_et = [2396732 282408 14964]; %% running on caltech
if maxf == 20

    Ntemp_mf_et = [2396732 640021 269211 124783 48856 17803 ...
        12131 8254 5611 3802 2558 1701 1098 652]; %% running on caltech for f0 = fmin, min match = 0.97,fmax= 20, 1pn
    minf_mf_et = 2:15;  %% corresponds to above
elseif maxf<20 %maxf == 12 ; cannot do [13,14,15,16,17,18,19]
    Ntemp_mf_et = [1056637 204110 56928 19988 ... 
        11974 6649 3389 1544 567 119];
    minf_mf_et = 2:maxf-1;
end


% pycbc_geom_nonspinbank
% --pn-order onePN --f0 10 --f-low 10 --f-upper 20 --delta-f 0.001 --min-match 0.97
% --min-mass1 1 --min-mass2 1 --max-mass1 3. --max-mass2 3. --verbose
% --output-file testNonSpinET.xml --psd-model EinsteinTelescopeP1600143

durs_mf_et = durr;%[durr(minfs==minf_mf_et(1)) durr(minfs==minf_mf_et(2)) durr(minfs==minf_mf_et(3))];



% fs = 40; %%% assuming sample only until 20 Hz to reduce FFT lengths
% Nsamples = fs * durs_mf_et; %%% tic; fft(rand(floor(Nsamples(1)),1)); toc --> get the times of next line
t_to_eval_fft = [0.248011 0.165672 0.120344 0.078136 0.045362...
    0.034623 0.013004 0.011491 0.01 0.01 0.01 0.01 0.01 0.01]; %% s [2,5,10]

ind1 = find(base_fmin==minf_mf_et);

time_to_eval_all_wfs_one_dur = Ntemp_mf_et(ind1:end) .* t_to_eval_fft(ind1:length(minf_mf_et));

% [~,coln] = find(mf_minfs == minf_mf_et'); %%%equiv to: for i coln(i) = find(mf_minfs(i) == minf_mf_et end

comp_time_tobs_all_wfs = time_to_eval_all_wfs_one_dur .* Tobs./durs_mf_et;


fact = 1:0.2:10;%[1 1.2 1.5 2 4 5 10];
cr_minfs = minfs;%(1:length(mf_minf));

% figure; hold on;
mf_minf = zeros(size(length(fact),length(min_h0_CR)));
mf_comp_cost = mf_minf;
for jj = 1:length(fact)
    for ii = 1:length(min_h0_CR) %%% to obtain approx same sensitivity
        out = minfs(find(min_h0_CR(minfs==minfs(ii))<=min_h0_mf * fact(jj),1));
        if isempty(out)
            mf_minf(jj,ii) = 0;
            mf_comp_cost(jj,ii) = 0;
        else
            mf_minf(jj,ii) = out;
            mf_comp_cost(jj,ii) = comp_time_tobs_all_wfs(minfs==out);
        end
    end
%     plot(cr_minfs(mf_minf(jj,:)>0),mf_minf(jj,mf_minf(jj,:)>0),'.-')

end

speed_up_comp_to_2_Hz = comp_time_tobs_all_wfs(minf_mf_et(ind1:end)>=base_fmin) / comp_time_tobs(minfs==base_fmin)

speed_up_comp_to_5_Hz = comp_time_tobs_all_wfs(minf_mf_et(ind1:end)>=second_f) / comp_time_tobs(minfs==second_f)

%%%% plot speed up as a function of sensitiivity loss

min_mf_freqs_2 = mf_minf(:,minfs==base_fmin);

min_mf_freqs_5 = mf_minf(:,minfs==second_f);



comp_times_for_sens_comp_5 = zeros(size(min_mf_freqs_5));
freqs_5 = comp_times_for_sens_comp_5;

comp_times_for_sens_comp_2 = zeros(size(min_mf_freqs_2));
freqs_2 = comp_times_for_sens_comp_2;
for jj = 1:length(min_mf_freqs_5)
    if min_mf_freqs_5(jj)<second_f
        comp_times_for_sens_comp_5(jj) = 0;
        freqs_5(jj) = 0;
    else
        comp_times_for_sens_comp_5(jj) = comp_time_tobs_all_wfs(min_mf_freqs_5(jj) == minf_mf_et(ind1:end));
        freqs_5(jj) = min_mf_freqs_5(jj);
    end

    if min_mf_freqs_2(jj)<base_fmin
        comp_times_for_sens_comp_2(jj) = 0;
        freqs_2(jj) = 0;
    else
        comp_times_for_sens_comp_2(jj) = comp_time_tobs_all_wfs(min_mf_freqs_2(jj) == minf_mf_et(ind1:end));
        freqs_2(jj) = min_mf_freqs_2(jj);
    end
end

speed_up_vary_sens_compar_to_5_Hz = comp_times_for_sens_comp_5 / comp_time_tobs(minfs==second_f);

speed_up_vary_sens_compar_to_2_Hz = comp_times_for_sens_comp_2 / comp_time_tobs(minfs==base_fmin);

iii = speed_up_vary_sens_compar_to_2_Hz>0;
speed_up_vary_sens_compar_to_2_Hz = speed_up_vary_sens_compar_to_2_Hz(iii);
freqs_2 = freqs_2(iii);
fact_2 = fact(iii);
[a,b]=unique(speed_up_vary_sens_compar_to_2_Hz);

% figure;scatter(freqs_2(b),a,100,fact_2(b),'sq','filled');colormap('jet'); colorbar; hold on;
% hold on; plot(freqs_2(b),a,'k-')

iii = speed_up_vary_sens_compar_to_2_Hz>0;
speed_up_vary_sens_compar_to_2_Hz = speed_up_vary_sens_compar_to_2_Hz(iii);
freqs_2 = freqs_2(iii);
fact_2 = fact(iii);
[a,b]=unique(speed_up_vary_sens_compar_to_2_Hz);

iii = speed_up_vary_sens_compar_to_5_Hz>0;
speed_up_vary_sens_compar_to_5_Hz = speed_up_vary_sens_compar_to_5_Hz(iii);
freqs_5 = freqs_5(iii);
fact_5 = fact(iii);

[c,d]=unique(speed_up_vary_sens_compar_to_5_Hz(speed_up_vary_sens_compar_to_5_Hz>0));
% hold on;scatter(freqs_5(d),c,100,fact_5(d),'sq','filled');colormap('jet'); colorbar; hold on;
% plot(freqs_5(d),c,'m-')
% set(gca,'YScale','log')

matrix_5Hz_comparison = [freqs_5(d) c fact_5(d)'];
matrix_2Hz_comparison = [freqs_2(b) a fact_2(b)'];




% figure; plot(fact(speed_up_vary_sens_compar_to_2_Hz>0),speed_up_vary_sens_compar_to_2_Hz(speed_up_vary_sens_compar_to_2_Hz>0))
% hold on; plot(fact(speed_up_vary_sens_compar_to_5_Hz>0),speed_up_vary_sens_compar_to_5_Hz(speed_up_vary_sens_compar_to_5_Hz>0))
% 
% 
% for kk = 1:length(speed_up_comp_to_2_Hz)
%     fact_red_sens_2(kk) = min(fact(find(speed_up_comp_to_2_Hz(kk) == speed_up_vary_sens_compar_to_2_Hz)));
% end
% 
% for ll = 1:length(speed_up_comp_to_5_Hz)
%     fact_red_sens_5(ll) = min(fact(find(speed_up_comp_to_5_Hz(ll) == speed_up_vary_sens_compar_to_5_Hz)));
% end
% % speed_up_same_sens_2_Hz_gfh_10_Hz_mf = comp_time_tobs_all_wfs(minf_mf_et==10) ./ comp_time_tobs(minfs==2)

% speed_up_same_sens_2_Hz_gfh_05_Hz_mf = comp_time_tobs_all_wfs(minf_mf_et==5) ./ comp_time_tobs(minfs==2)

% speed_up_same_sens_1_Hz_gfh_05_Hz_mf = comp_time_tobs_all_wfs(minf_mf_et==5) ./ comp_time_tobs(minfs==1)

%%%%%%% fix comp cost, determine sensitivity improvement


end