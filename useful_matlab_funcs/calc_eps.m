function [ eps ] = calc_eps( Egw,I,f0,fdot0,n,T,t )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
G=6.67e-11;
c=3e8;

sd_tau=abs(f0*pi^(1-n)/(fdot0*(n-1)));

fact1=-5*c^5*Egw/(32*pi^6*G);
fact2=(I.^2*f0^6*sd_tau).^-1;
fact3=(n-7)/(n-1);

if ~exist('t','var')
    fact4=(sd_tau/T)*((1-n)/(7-n)*(1+T/sd_tau)^((8-2*n)/(1-n))-1)-1;
else
    fact4=((1+t./sd_tau).^((7-n)/(1-n))-1)^-1;

end

eps=sqrt(abs(fact1*fact2*fact3*fact4));
