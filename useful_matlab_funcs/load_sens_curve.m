function [ freq,hnoise ] = load_sens_curve( run,freq,Tobs,pathfile )
%UNTITLED https://dcc.ligo.org/LIGO-T1500293-v11/public
%   Detailed explanation goes here

if ~exist('pathfile','var')
    pathfile='~/Downloads/curves_Jan_2020.mat';
end

if strcmp(run,'O1')
    load O1_ave_noise.mat
    freq=freq;
    hnoise=sqrt(ave_noise_O1H*1e-40);
elseif strcmp(run,'AdV')
    fid=fopen('AdV_refsens_100512.txt');
    C = textscan(fid,'%f %f64');
    freq=C{1};
    hnoise=C{2};
elseif strcmp(run,'O2_L')
    fid=fopen(strcat('~/Downloads/','2017-07-20_C01_L1_O2_Sensitivity_strain_asd.txt'));
    if fid==-1
%         disp('on louvain'); 
        clear fid
        fid=fopen('/nfs/user/amiller/dark_matter/2017-07-20_C01_L1_O2_Sensitivity_strain_asd.txt');
    end
    C = textscan(fid,'%f %f64');
    freq=C{1};
    hnoise=C{2};
elseif strcmp(run,'O2_H')
    fid=fopen('2017-07-2016-12-08_C01_H1_O2_Sensitivity_strain_asd.txt');
    C = textscan(fid,'%f %f64');
    freq=C{1};
    hnoise=C{2};
elseif strcmp(run,'advL_design')
    load(pathfile)
    freq=curves.aLIGO_design(:,1);
    hnoise=curves.aLIGO_design(:,2);
elseif strcmp(run,'advV_design')
    load(pathfile)
    freq=curves.AdVirgo_sqz(:,1);
    hnoise=curves.AdVirgo_sqz(:,2);
elseif strcmp(run,'et')
    load(pathfile)
    freq=curves.ET_D(:,1);
    hnoise=curves.ET_D(:,2);
elseif strcmp(run,'CE')
    load(pathfile)
    freq=curves.CE(:,1);
    hnoise=curves.CE(:,2);
elseif strcmp(run,'lisa')
    [Sn] = lisa_Sn(freq, Tobs);%,L,confusion_flag);
    hnoise=sqrt(Sn);
elseif strcmp(run,'decigo')
    Sn = decigo_Sn(freq);
    hnoise=sqrt(Sn);
elseif strcmp(run,'nemo')
    hnoise=10^-24; %[1-2.5] KHz
elseif strcmp(run,'lsdmin')
    hnoise=10^-23; %%100-m
elseif strcmp(run,'lsdmax')
    hnoise=10^-21; %%100-m
elseif strcmp(run,'holometer')
    hnoise=10^-21; %[1-13 MHz]
elseif strcmp(run,'Akutsu')
    hnoise=10^-20; %100 MHz
elseif strcmp(run,'cardiff')
    hnoise=1.8*10^-19/3 ; % 1-100 MHz, 3-m long detector 1.8e-19 m/sqrt(hz)
elseif strcmp(run,'O3L')
    load(pathfile)
    freq=curves.O3_L1(:,1);
    hnoise=curves.O3_L1(:,2);
elseif strcmp(run,'O3H')
    load(pathfile)
    freq=curves.O3_H1(:,1);
    hnoise=curves.O3_H1(:,2);
elseif strcmp(run,'O3V')
    load(pathfile)
    freq=curves.O3_V1(:,1);
    hnoise=curves.O3_V1(:,2);
end


end

