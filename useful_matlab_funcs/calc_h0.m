function [ h00 ] = calc_h0( ellipticity,Iz,f,d )
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
cc = constants;
G = cc.G;
c = cc.c;

h00=(4*pi^2*G/c^4)*ellipticity.*Iz*f.^2./(d * cc.units.kpc_to_m);


end

