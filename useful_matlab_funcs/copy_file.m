function [ status,result ] = copy_file( fnameout,fileoutdir,maxx,where_am_i )
%Copying from local node back to main area 

%   fnameout:       filename (no path)
%   fileoutdir:     directory where fnameout will be saved
%   maxx:           number of times to try lcg-cp command
%   where_am_i:     string describing if at nikhef or cnaf

if strcmp(where_am_i,'nikhef')
    command111=['mkdir -p ' fileoutdir ' && cp ' pwd '/' fnameout ' ' fileoutdir] 
%     srm_path=strcat('srm://tbn18.nikhef.nl:8446/srm/managerv2?SFN=/dpm/nikhef.nl/home/virgo/CW/RomePSS/',fileoutdir,fnameout);
    %path to diretory at Nikef where we want the output file actually saved
%     command111=['lcg-cp --vo virgo -D srmv2 --srm-timeout 21600 -b -S VIRGO_PEGASUSDISK -v file:'  pwd '/' fnameout ' ',srm_path];
elseif strcmp(where_am_i,'cnaf')
    srm_path=strcat('srm://storm-fe-archive.cr.cnaf.infn.it:8444/virgo4/virgo/RomePSS/',fileoutdir,fnameout);
    %path to diretory at Nikef where we want the output file actually saved
%     command111=['lcg-cp -v --vo virgo -U srmv2 file:'  pwd '/' fnameout ' ',srm_path];
    command111=['gfal-copy -fp ' pwd '/' fnameout ' ',srm_path]
elseif strcmp(where_am_i,'louvain')
    command111=['mkdir -p ' fileoutdir ' && cp ' pwd '/' fnameout ' ' fileoutdir] 
elseif strcmp(where_am_i,'cnaftolouvain')
    srm_path=strcat('srm://ingrid-se02.cism.ucl.ac.be:8444/srm/managerv2?SFN=',fileoutdir,fnameout);
    command111=['gfal-copy -fp ' pwd '/' fnameout ' ',srm_path]
else
    srm_path='';
    command111='';
end
%command to copy the output file from the local directory of the
% node (on which the job was run, pwd) to Nikef, where we can see
% the files
count=0;
[status, result]=system(command111)
while status~=0 && count<=maxx
    [status, result]=system(command111) %executes command111
    count=count+1;
end




end

