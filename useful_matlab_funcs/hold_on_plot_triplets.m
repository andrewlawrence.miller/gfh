function [ output_args ] = hold_on_plot_triplets( x,y,z,marker,cm,flag_logx,flag_logy,flag_log_cb,colorm )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

if ~exist('flag_logx','var')
    flag_logx=0;
end
if ~exist('flag_logy','var')
    flag_logy=0;
end
if ~exist('flag_log_cb','var')
    flag_log_cb=0;
end

if ~exist('cm','var')
    cm=colormap(colorm); 
end

mi=min(z);
ma=max(z);

mami=ma-mi;

% figure
nc=length(cm);
zz=floor(nc*0.9999*(z-mi)/mami+1);

for i = 1:nc
    col=cm(i,:);
    if flag_logx==1 & flag_logy~=1
        semilogx(x(zz==i),y(zz==i),marker,'MarkerFaceColor',col,'MarkerEdgeColor',col),hold on
    elseif flag_logy==1 & flag_logx~=1
        semilogy(x(zz==i),y(zz==i),marker,'MarkerFaceColor',col,'MarkerEdgeColor',col),hold on
    elseif flag_logy==1 &flag_logx==1
        loglog(x(zz==i),y(zz==i),marker,'MarkerFaceColor',col,'MarkerEdgeColor',col),hold on
    else
        plot(x(zz==i),y(zz==i),marker,'MarkerFaceColor',col,'MarkerEdgeColor',col),hold on
    end
end
%if flag_log_cb==1
    %caxis(log10([min(z) max(z)]))
%else
    caxis([min(z) max(z)])
%end
end

