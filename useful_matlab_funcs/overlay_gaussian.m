function [] = overlay_gaussian(orig_dist_of_counts,snrs_desired,sigma)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

if ~exist('snrs_desired','var')
    snrs_desired=-5.5:0.1:5.5;
end
if ~exist('sigma','var')
    sigma=1;
end

scale_val = max(orig_dist_of_counts);
gaussian = scale_val*exp(-(snrs_desired/sigma).^2/2);
hold on;
plot(snrs_desired,gaussian,'color','cyan','linewidth',1.5)
end

