function [ ] = plot_h0_eps2_vs_f( fs,epss2,x_log_flag,y_log_flag )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

cc=constants;
v0=cc.v0;
figure;
plot(fs, epss2, 'k')
if x_log_flag==1
    set(gca,'XScale','log')
end

if y_log_flag==1
    set(gca,'YScale','log')
end

ms=dp_calc_m(fs);

xlabel('frequency (Hz)'); ylabel('\epsilon^2')
box off
ax=gca;
set(gca,'FontSize',14)
axes('ylim', [dp_calc_strain(min(ms),sqrt(ax.YLim(1)),v0) dp_calc_strain(max(ms),sqrt(ax.YLim(2)),v0)], 'Xscale','log','YScale','log','ylim', [sqrt(min(epss2)) sqrt(max(epss2))], 'color', 'none', 'YAxisLocation', 'right', 'XAxisLocation', 'top')
grid on;
ax2=gca;
ax2.XAxis.Visible='off';
ax2.YAxis.Label.String='h_0';
set(gca,'FontSize',14)

end