function [ str_n ] = make_n_string( n )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
test=num2str(n);

if length(test)==3
    str_n=[test '000'];
end

if length(test)==4
    str_n=[test '00'];
end
if length(test)==5
    str_n=[test '0'];
end
    
if length(test)~=3 && length(test)~=4 && length(test)~=5
    str_n=num2str(round(n,4));
end


end

