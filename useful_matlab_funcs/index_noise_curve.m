function [ hnoise_band ] = index_noise_curve(sig_fs,curve_fs,noise_curve)
%UNTITLED2 Summary of this function goes here
%   sig_fs: the frequencies at which you want the values of noise_curve
%   curve_fs: the frequencies that index the noise_curve
%   noise_curve: the ASD
%   hnoise_band: the values of the ASD at the frequencies sig_fs
for r=1:length(sig_fs) % for each frequency find the index of the nearest bin of the noise curve 
    [wal index(r)]=min(abs(sig_fs(r)-curve_fs));   
    hnoise_band=noise_curve(index);
end

end

