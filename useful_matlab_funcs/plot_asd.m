function [] = plot_asd(f,P1,TFFT)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
semilogy(f,P1,'-sq');
xlabel('frequency (Hz)'); ylabel('amplitude spectral density (1/Hz^{1/2})');
set(gca,'FontSize',14); grid on
title(['T_{FFT}=' num2str(TFFT) ' s'])
end

