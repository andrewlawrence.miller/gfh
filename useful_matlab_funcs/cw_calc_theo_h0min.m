function [h0min] = cw_calc_theo_h0min(Sn,TFFT,Tobs,Gamma,theta_thr,CR_thr)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if ~exist('Gamma','var')
    Gamma = 0.95;
end

if ~exist('theta_thr','var')
    theta_thr = 2.5;
end

if ~exist('Crthr','var')
    CR_thr = 5;
end

Lambda=calc_Lambda(Gamma,theta_thr,CR_thr);

N=Tobs/TFFT;

h0min=Lambda/N^(1/4)*sqrt(Sn/TFFT);


end

