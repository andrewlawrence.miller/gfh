function [ ] = make_sfdb_list(path_to_a_file,det)
%UNTITLED3 Constructs list of sfbd files in text file to be used in
%band2sbl3 code. Each file is in a different folder, so we must change the
%folder, obtain the filename, and save the filename into text file in loop
%   Detailed explanation goes here

% try
%     thedir='~/Desktop/codes/';
%     cd(thedir)
% catch
%     thedir='/storage/users/andrew/codes/';
%     cd(thedir)
% end
% path_to_a_file=strcat(thedir,'/sfdbs/');
%path_to_a_file=strcat(thedir,'/sfdbs_POSTMERGER/');
%cd(path_to_a_file)
path_to_a_file
strcat(path_to_a_file,'listFILES.txt')
fileid=fopen(strcat(path_to_a_file,'listFILES.txt'),'w');
    

if det==0 %livingston
    files=dir(strcat(path_to_a_file,'L*.SFDB09'));
    disp('livingston')
end
if det==1 %hanford
    files=dir(strcat(path_to_a_file,'H*.SFDB09'));
    disp('hanford')
end

if det==10
    files=dir(strcat(path_to_a_file,'*.SFDB09'));
    disp('creating .txt file with all sfdbs names, used for lcg-cp command')
end





for i=1:length(files)
%     
%     if length(filestruct(i).name)<3 %because 2 directories don't contain files and look like '.' and '..'
%         continue %advances loop, i = i+1;
%     end
    
    %path_to_a_file=[folder filestruct(i).name];
    
    %path_to_a_file=thedir;
    
    %cd(path_to_a_file) %changes to directory where each SFDB09 file is located
    files(i)
   one_file=files(i).name
   % one_file=strcat(files(i).folder,'/',files(i).name)
fileid
   % total_filename_path=strcat(path_to_a_file,one_file); %concates total filename (path+name)
    
    %fprintf(fileid,total_filename_path); %prints each total name to file
    fprintf(fileid,one_file);
    
    fprintf(fileid,'\n');
end

%cd /storage/users/andrew/summer_2016/

fclose(fileid);