function [ status,result ] = copy_a_file_to_local_node( filename,path_filename,maxx )
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here

%base_dir='srm://tbn18.nikhef.nl:8446/srm/managerv2?SFN=/dpm/nikhef.nl/home/virgo/CW/RomePSS/';

%path_file=strcat(base_dir,fileoutdir,filename);



command0=strcat(path_filename,' file:',pwd,'/',filename); %writes copy command for input filein and paste command for filein in node's local directory
            
command1=['lcg-cp --vo virgo -v -D srmv2 --srm-timeout 21600 -b -S VIRGO_PEGASUSDISK ' command0] 

[status, result] = system(command1)           

count=0;
while status~=0 && count<=maxx
    [status, result] = system(command1) %execute command1
    count=count+1;
end


end

