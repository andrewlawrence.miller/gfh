function [] = make_line_list(run)
%UNTITLED2 function originally used in O1 analysis

if ~exist('run','var')
    run='O2';
end

if strcmp(run,'O2')
    basedir='~/Desktop/dp_searches/O2/Lines/';
    fnamelines = ['O2LinesToBeCleaned_H1_v2.txt';'O2LinesToBeCleaned_L1_v2.txt'];
    fnameveto = ['H1_O2_vetoedbands.txt';'L1_O2_vetoedbands.txt'];
elseif strcmp(run,'O3')
    basedir='~/Desktop/dp_searches/O3/O3_codes/Lines/';
    fnamelines=['O3H1Lines.txt';'O3L1Lines.txt'];
    fnameveto = ['H1_O3_vetoedbands.txt';'L1_O3_vetoedbands.txt'];

else
    disp('invalid run')
    return;
end
% Read in line list 
nchar = length(fnamelines);
fidlines(1) = fopen(strcat(basedir,fnamelines(1,1:nchar)));
fidlines(2) = fopen(strcat(basedir,fnamelines(2,1:nchar)));

for ifo = 1:2
  ncomb(ifo) = 0;
  fprintf('Reading comb / line list from %s\n',fnamelines(ifo,1:nchar));
  while (~feof(fidlines(ifo)))
    record = textscan(fidlines(ifo),'%s',1,'delimiter','\n');
    string = cell2mat(record{1});
    if (length(string)==0)
     fprintf('Done reading file\n');
    elseif (string(1,1)=='%')
     fprintf('Found comment line\n');
    else
%       values = textscan(string,'%f %d %f %d %d %f %f %s');
      values=textscan(string,'%f %d %f %d %d %f %f %[^\n]');
      if isempty(values{1})
          continue;
      end
      ncomb(ifo) = ncomb(ifo) + 1;
      spacing(ncomb(ifo),ifo) = values{1};
      type(ncomb(ifo),ifo) = values{2};
      offset(ncomb(ifo),ifo) = values{3};
      tooth_lo(ncomb(ifo),ifo) = values{4};
      tooth_hi(ncomb(ifo),ifo) = values{5};
      width_left(ncomb(ifo),ifo) = values{6};
      width_right(ncomb(ifo),ifo) = values{7};
      descrip(ncomb(ifo),ifo)= values{8};
%       descrip{ncomb(ifo)}{ifo}= values{8};
%       fprintf('spacing(%d) = %f, type=%d, offset=%f, tooth_lo=%d, tooth_hi=%d, width_left=%f, width_right=%f\n',ncomb(ifo),spacing(ncomb(ifo),ifo),type(ncomb(ifo),ifo),offset(ncomb(ifo),ifo),tooth_lo(ncomb(ifo),ifo),tooth_hi(ncomb(ifo),ifo),width_left(ncomb(ifo),ifo),width_right(ncomb(ifo),ifo));
    end
  end
  fprintf('Found %d combs / lines\n',ncomb(ifo));
end

nchar2 = length(fnameveto);
fidveto(1) = fopen(strcat(basedir,fnameveto(1,1:nchar2)),'w');
fidveto(2) = fopen(strcat(basedir,fnameveto(2,1:nchar2)),'w');


% Loop over interferometer to mark vetoed bins
for ifo = 1:2
	    
  % Declare fbase to be a real number, to avoid later matlab type overriding
  fbase = 1.234;

  % Loop over comb / line list and define bins with a veto
  for comb = 1:ncomb(ifo)
     for tooth = tooth_lo(comb,ifo):tooth_hi(comb,ifo)
      fbase = offset(comb,ifo) + double(tooth)*spacing(comb,ifo);
      if (type(comb,ifo)==0 || type(comb,ifo) == 1)
        flo_tooth = fbase - width_left(comb,ifo);
        fhi_tooth = fbase + width_right(comb,ifo);
      elseif (type(comb,ifo) == 2)
        flo_tooth = fbase - double(tooth)*width_left(comb,ifo);
        fhi_tooth = fbase + double(tooth)*width_right(comb,ifo);
      else
        fprintf('Oops - itype(%d)=%d\n',comb,type(comb,ifo));
        return;
      end
      this_descrip=descrip(comb,ifo);
%       fprintf(fidveto(ifo),'%f \t %f \n', flo_tooth,fhi_tooth);
      fprintf(fidveto(ifo),'%f \t %f \t %s \n', flo_tooth,fhi_tooth,['% ' this_descrip{1}]);
%       fprintf(fidveto(ifo),'Vetoing %f-%f Hz (spacing=%f, offset=%f, tooth=%d)\n',flo_tooth,fhi_tooth,spacing(comb,ifo),offset(comb,ifo),tooth);
%       fprintf('Vetoing %f-%f Hz (spacing=%f, offset=%f, tooth=%d)\n',flo_tooth,fhi_tooth,spacing(comb,ifo),offset(comb,ifo),tooth);
%       int_flo = max(1,floor(flo_tooth*Tcoherence))+1;
%       int_fhi = min(length(freq)-1,ceil(fhi_tooth*Tcoherence))+1;
%       veto(ifo,int_flo:int_fhi) = 1;
%      if (fbase>=1213.2 && fbase<=1213.4)
%	fprintf('*** fbase=%f, flo_tooth=%f, fhi_tooth=%f, int_flo=%d, int_fhi=%d',fbase,flo_tooth,fhi_tooth,int_flo,int_fhi);
%      end
    end
  end
end

fclose('all');

end
