function [f,P1,fs,meanP ] = FFT_and_plot_asd( ts,sig,TFFT,plot_flag,num_plots,N_ffts )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

T=max(diff(ts)); %sampling time
Fs=ceil(1/T);
L=length(sig);

if ~exist('TFFT','var')
    TFFT=L/Fs;
end
if ~exist('plot_flag','var')
    plot_flag=0;
end

if ~exist('num_plots','var')
    num_plots=1;
end

if plot_flag==0
    num_plots=0;
end

TFFT=floor(TFFT);

num_samps_per_fft=(TFFT*Fs);
if isreal(sig)
    f=Fs*(0:(num_samps_per_fft/2))/num_samps_per_fft;
else
    f=Fs*(0:(num_samps_per_fft-1))/num_samps_per_fft;
end

if ~exist('N_ffts','var')
    N_ffts=floor(L/num_samps_per_fft);
end


allPs=zeros(N_ffts,num_samps_per_fft);
fs=zeros(1,N_ffts);

for j=1:N_ffts
    Y=fft(sig(1+(j-1)*num_samps_per_fft:j*num_samps_per_fft));
    P2=abs(Y/num_samps_per_fft);
    if isreal(sig)
        P1=P2(1:num_samps_per_fft/2+1);
        P1(2:end-1)=2*P1(2:end-1);
    else
        P1=P2;
    end
    if plot_flag==1 && j<=num_plots
        figure; plot_psd(f,P1,TFFT)
        title(['TFFT=' num2str(TFFT) ' s'])
      
    end
    [~,b]=max(P1);
    fs(j)=f(b);
    
    allPs(j,:)=P1;
    minf=0.43597;
    maxf=0.438;
    all_means(j)=mean(P1(f>=minf & f<=maxf));
    all_stdss(j)=std(P1(f>=minf & f<=maxf));
    
end

meanP=mean(allPs);

% figure;plot(all_means); xlabel('time segment'); ylabel('mean value per time segment'); title(['TFFT=' num2str(TFFT)])
% figure;plot(all_stdss); xlabel('time segment'); ylabel('stad value per time segment'); title(['TFFT=' num2str(TFFT)])


end

