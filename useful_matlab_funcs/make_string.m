function [ str ] = make_string( value,isdur )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if ~exist('isdur','var')
    isdur=0;
end

if isdur==1
    if value<100
        str=['0000',num2str(value)];
    end

    if value>=100 && value<1000
        str=['000',num2str(value)];
    end

    if value>=1000 && value<10000
        str=['00',num2str(value)];
    end

    if value>=10000 && value<100000
        str=['0',num2str(value)];
    end
    
    if value>=100000 && value< 1000000
        str=num2str(value);
    end
    
else
    
    if value<100
        str=['00',num2str(value)];
    end

    if value>=100 && value<1000
        str=['0',num2str(value)];
    end

    if value>=1000
        str=num2str(value);
    end
end

