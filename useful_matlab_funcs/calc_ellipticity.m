function [ellip] = calc_ellipticity(h00,f,d,Iz)
%UNTITLED2 Summary of this function goes here
%   Iz: kg m^2
%   d: kpc
%   f: Hz

cc=constants;
c=cc.c;
G=cc.G;

if ~exist('Iz','var')
    Iz=10^38; %kg m^2
end

if ~exist('f','var')
    f=100;

end

if ~exist('d','var')
    d=8; %kpc
end

d=d*cc.units.kpc_to_m;

ellip=h00 ./ ((4*pi^2*G/c^4) * Iz * f.^2 / d );

end