function [ egw ] = calc_egw(eps,I,f0,fdot0,n,T,t )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

G=6.67e-11;
c=3e8;

sd_tau=abs(f0*pi^(1-n)/(fdot0*(n-1)));

fact1=(32*pi^6*G)/(-5*c^5);
fact2=(I.^2*f0^6.*eps.^2*sd_tau);
fact3=(n-1)/(n-7);
if ~exist('t','var')
    disp('time avg')
    fact4=(sd_tau/T)*((1-n)/(7-n)*(1+T/sd_tau)^((8-2*n)/(1-n))-1)-1;
else 
    disp('instanteous energy')
    fact4=(1+t./sd_tau).^((7-n)/(1-n))-1;
end

egw=fact1.*fact2.*fact3.*fact4;

end

