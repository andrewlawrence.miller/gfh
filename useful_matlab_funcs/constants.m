function [ consts ] = constants( )
%UNTITLED6 fundamental constants
%   Detailed explanation goes here
c=299792458; % light
h=6.626e-34;
hbar=h/(2*pi);
ev=1.602e-19;
Msun=1.9885e30;
G=6.67430e-11;
f_earth=1/86400;
StellarDay= 23*3600 + 56*60 + 4.098903691;


v0=0.000766667; %over c
vesc=0.00181459; %over c
rhodm=0.4e9/1e-6;   %(*eV/m3*) % current dark matter energy density


omega_earth_orb=2*pi/(365.25*86400);
omega_earth_rot=2*pi*f_earth;
R_earth=6371e3;
Rorb=149597871e3;
eps0=8.854187e-12;
fine_struct=1/137;

units.ev_to_inv_s=ev/hbar;
units.ev_to_inv_m=ev/(hbar*c);
units.ev_to_kg=ev/c^2;
units.kg_to_ev=c^2/ev;
units.charge_LH=ev/sqrt(4*pi*fine_struct);
units.charge_G=ev/sqrt(fine_struct);
units.kpc_to_m=3.086e19; 
units.mpc_to_m=3.086e22;

consts.c=c;
consts.h=h;
consts.hbar=hbar;
consts.hbar_inev=hbar/ev;
consts.ev=ev;
consts.Msun=Msun;
consts.G=G;
consts.eps0=eps0;
consts.fine_struct=fine_struct;
consts.f_earth=f_earth;
consts.omega_earth_rot=omega_earth_rot;
consts.omega_earth_orb=omega_earth_orb;
consts.v_earth_rot=omega_earth_rot*R_earth;
consts.v_earth_orb=omega_earth_orb*Rorb;
consts.Rorb=Rorb;
consts.Re=R_earth;
consts.v0=v0;
consts.vesc=vesc;
consts.rhodm=rhodm;
consts.StellarDay=StellarDay;


consts.units=units;


end

