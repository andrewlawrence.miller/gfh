function [mean_frac_amp,mean_frac_pow] = T_FFT_bsd( ts,sig,TFFT,f0 )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

T=max(diff(ts)); %sampling time
Fs=round(1/T);
L=length(sig);

if ~exist('TFFT','var')
    TFFT=L/Fs;
end

num_samps_per_fft=TFFT*Fs;
if ~isreal(sig)
    f=Fs*(0:(num_samps_per_fft-1))/num_samps_per_fft;
else
    f=Fs*(0:(num_samps_per_fft/2))/num_samps_per_fft;
end

N_ffts=floor(L/num_samps_per_fft);
w=pswindow(3,num_samps_per_fft);
% mean_frac_amp=0;
for j=1:N_ffts
    xx=sig(1+(j-1)*num_samps_per_fft:j*num_samps_per_fft);
%     xx=strains_to_fft.*w.';
    Y=fft(xx);
    h0(j)=sqrt(2*mean(xx.^2));
%     disp('not enough samples to do an FFT')
    if isreal(sig)
        asd=abs(Y/num_samps_per_fft);
        asd = asd(1:num_samps_per_fft/2+1);
        asd(2:end-1) = 2*asd(2:end-1);
    else
        asd=abs(Y*dt/num_samps_per_fft);
    end
%     [~,b]=min(abs(f-f0));
    amp_in_sigbin(j)=max(asd);%asd(b);
%     totpow=totpow+(amp_in_sigbin(j)/h0(j))^2;
%     if j==1
%         figure;semilogy(f,asd)
%         title(strcat('TFFT=',num2str(TFFT)))
%     end
end
mean_frac_amp=mean(amp_in_sigbin./h0);
mean_frac_pow=mean((amp_in_sigbin./h0).^2);


end

