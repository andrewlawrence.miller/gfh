function [rho] = cbc_calc_mf_snr_actual_sens(h0_min,freq,Tobs,Mc,Sn)
%UNTITLED6 Assumes Gaussian noise! Sn pulled out of integral \int_fmin^fmax
%f^(-11/3) /Sn(f) df
% This function calculates the factor in front of Sn(f) assuming that Sn(f)
% does not change with frequency (white noise). The sensitivity computeud
% here is the actual sensitivity, accounting for the need to select a fixed
% number of candidates in a real search
cc = constants;
G = cc.G;
c = cc.c;
Msun = cc.Msun;

% tstart = calc_time_to_coalescence(Mc,fmin);
% tend = calc_time_to_coalescence(Mc,fmax);
% Tobs = abs(tstart - tend);

Mc = Mc * Msun;

first_term = 120 * pi^(8/3);

secon_term = (G * Mc / c^3).^(5/3);

dfs = diff(freq);
try
    df = [0 dfs]; 
catch 
    rho = 0;
    return;
end

% freq = fmin:df:fmax;

third_term = 1 ./ sum(freq.^(-11/3) ./ Sn .* df);

rho = h0_min / sqrt(first_term .* secon_term .* third_term); %% sqrt(lambda_min)= rho

end