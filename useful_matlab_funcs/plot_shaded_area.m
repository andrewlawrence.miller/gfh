function [ ] = plot_shaded_area( x,vec_mean,vec_minus,vec_plus,x_log_flag,y_log_flag )
%UNTITLED30 Summary of this function goes here
%   Detailed explanation goes here
if ~exist('x_log_flag','var')
    x_log_flag=0;
end

if ~exist('y_log_flag','var')
    y_log_flag=0;
end

figure;
hold on;

fill( [x fliplr(x)],  [vec_plus fliplr(vec_minus)], 'k');
alpha(.25);
plot(x, vec_mean, 'k', 'LineWidth', 2)
plot(x, vec_plus, 'k')
plot(x, vec_minus, 'k')
if x_log_flag==1
    set(gca,'XScale','log')
end

if y_log_flag==1
    set(gca,'YScale','log')
end
xlabel('frequency (Hz)'); ylabel('T_{FFT} (s)')
box off
ax=gca;
set(gca,'FontSize',14)
axes('xlim', [dp_calc_m(ax.XLim(1)) dp_calc_m(ax.XLim(2))], 'Xscale','log','YScale','log','ylim', [min(vec_mean) max(vec_mean)], 'color', 'none', 'YAxisLocation', 'right', 'XAxisLocation', 'top')
grid on;
ax2=gca;
ax2.YAxis.Visible='off';
ax2.XAxis.Label.String='mass (eV/c^2)';
set(gca,'FontSize',16)

end

