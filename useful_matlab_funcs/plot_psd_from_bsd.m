function [f,P2 ] = plot_psd_from_bsd( this_bsd,TFFT,num_plots )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
ts=x_gd(this_bsd);
sig=y_gd(this_bsd);
T=max(diff(ts)); %sampling time
Fs=round(1/T);
L=length(sig);

if ~exist('TFFT','var') || TFFT==0
    TFFT=L/Fs;
end

if ~exist('num_plots','var')
    num_plots=1;
end

num_samps_per_fft=TFFT*Fs
f=Fs*(0:(num_samps_per_fft-1))/num_samps_per_fft;
N_ffts=L/num_samps_per_fft;
for j=1:N_ffts
    Y=fft(sig(1+(j-1)*num_samps_per_fft:j*num_samps_per_fft));
    P2=abs(Y/num_samps_per_fft);
%     P1=P2(1:num_samps_per_fft/2+1);
%     P1(2:end-1)=2*P1(2:end-1);
    if j<=num_plots
        figure;
        semilogy(f,P2);
    end
end



end

