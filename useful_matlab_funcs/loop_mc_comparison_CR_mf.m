function [outputArg1,outputArg2] = loop_mc_comparison_CR_mf(inputArg1,inputArg2)

CR_thr = 5;
mf_thr = 8;
mcs = [1 1.5 2 2.5 3];
maxf = 12;
fmin = 5;

for ii = 1:length(mcs)
[~,~,~,~,~, matrix_5Hz_comparison{ii} ,matrix_8Hz_comparison{ii}] =...
    comparison_CR_mf_snr(CR_thr,mf_thr,mcs(ii),'et',maxf,fmin);

    

end
figure;
for i = 1:length(mcs)
    scatter(matrix_5Hz_comparison{i}(:,1),matrix_5Hz_comparison{i}(:,2),100,...
        matrix_5Hz_comparison{i}(:,3),'sq','filled') ;hold on;
    colormap('jet'); colorbar;
end
set(gca,'YScale','log')