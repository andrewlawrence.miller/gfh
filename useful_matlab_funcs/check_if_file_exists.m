function [status,result]=check_if_file_exists(check_fnameoutttt,where_am_i,fileoutdir)

% check_fnameouttttt:   filename to check if exists
% where_am_i:           'cnaf' or 'nikhef'
% fileoutdir:           the name of the directory after .../RomePSS/, e.g. 
%                       fileoutdir = 'L/postmerger/search_output/'


if strcmp(where_am_i,'nikhef')
    comm='lcg-ls --vo virgo -D srmv2 -b -v srm://tbn18.nikhef.nl:8446/srm/managerv2?SFN=/dpm/nikhef.nl/home/virgo/CW/RomePSS/';
    
elseif strcmp(where_am_i,'cnaf')
    % comm='lcg-ls srm://storm-fe-archive.cr.cnaf.infn.it:/virgo4/virgo/RomePSS/'; %% not working
    comm='ls /storage/gpfs_virgo4/virgo4/virgo/RomePSS/';
else
    comm='ls ';
end

%check_fnameoutttt=strcat('COIN_Cand_file_',str_band_ind,'_',str_tfft_ind,'_',str_subjob_ind,'_',str_minf,'_',str_maxf,'_',num2str(this_TFFT),'_',str_dur,...
%'_',str_n,'_.mat'); %it's fine as long as I keep the same configuration of str_band_ind, etc.
commandss=[comm fileoutdir check_fnameoutttt];
[status, result]=system(commandss);

end

