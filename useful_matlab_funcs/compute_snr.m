function [snr] = compute_snr(dat,sig,tfft,Sn)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
if ~exist('Sn','var')
    Sn=1;
end

sigsig=inner_product(sig,sig,tfft,Sn);
datsig=inner_product(dat,sig,tfft,Sn);

snr=abs(datsig)/sqrt(sigsig);
% 
% mf=mf_5vec(dat,sig)


end

