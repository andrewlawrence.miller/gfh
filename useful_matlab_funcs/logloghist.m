function [ ] = logloghist( data,bins )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if ~exist('bins','var')
    [n, xout] = hist(data);
else
    [n, xout] = hist(data,bins);
end


bar(xout, n, 'barwidth', 1, 'basevalue', 1);
set(gca,'YScale','log')
set(gca,'Xscale','log')
set(gca,'FontSize',14)

end

