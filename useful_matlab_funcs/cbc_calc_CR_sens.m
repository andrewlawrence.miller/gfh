function [CR] = cbc_calc_CR_sens(h0min,f0,mc,TFFT,tobs,n,run,Gamma,theta_thr)
%UNTITLED2 run can either be 'o3a, 'O2_L', etc. OR a value for Sn !!!

cc=constants;

if ~exist('run','var')
    run = 'O3a';
end

if ~exist('mc','var')
    mc=1e-3;
end
if ~exist('tobs','var')
    tobs=525600*60;
end
if ~exist('Gamma','var')
    Gamma=0.95;
end
if ~exist('theta_thr','var')
    theta_thr=2.5;
end

if n==11/3
    k=calc_k(mc);
elseif n ==3 || n == 5 || n == 7
    k=fdot0/f0^n;
end
pow=n-1;

times = 0:TFFT/2:tobs;
const = k.*pow.*f0.^pow;
fsss = f0*(1-const.*times).^(-1./pow);

% fisco=calc_f_isco(Mc);
% 
% fsss=fsss(fsss<fisco);
% if isempty(fsss)
%     continue;
% end
if ischar(run) || isstruct(run)
    if strcmp(run,'O3a')
        try
            Snfile = load('~/Desktop/O3Sn.mat');
        catch
            Snfile = load('./O3Sn.mat');
        end
        hnoise = sqrt(Snfile.oneoverSn') * 1e-20;
        det_freq = Snfile.fss;
    elseif isstruct(run)
        hnoise = sqrt(run.oneoverSn') * 1e-20;
        det_freq = run.fss;
    else
        [ det_freq,hnoise ] = load_sens_curve(run);
    end
    try
        [~,index] = (min(abs(fsss-det_freq'),[],1)); 
    catch
       [~,index] = (min(abs(fsss-det_freq),[],1)); 
    end
    
    hnoise_band=hnoise(index);
    clear index
    hnoise_band=hnoise_band';
    Sn=hnoise_band.*hnoise_band; % detector noise power spectrum
else
    Sn = run;
end
N=round(2 * tobs/TFFT);
p0=exp(-theta_thr)-exp(-2*theta_thr)+1/3*exp(-3*theta_thr);             
p1=exp(-theta_thr)-2*exp(-2*theta_thr)+exp(-3*theta_thr);

if n==11/3
    Factor=sum(1./Sn.*fsss.^(4/3)); %%%%%%%%% was 4/6.. a bug!
elseif n==5
    Factor=sum(1./Sn.*fsss.^(4));
elseif n==7
    Factor=sum(1./Sn.*fsss.^(6));
end
fact1=4.02/theta_thr^(1/2);
fact2=sqrt(N/TFFT)*Factor^(-1/2);
fact3=(p0*(1-p0)/(N*p1^2))^(1/4);
% fact4=sqrt(CR_thr-sqrt(2)*erfcinv(2*Gamma));

other = (h0min ./ (max(fsss)^(2/3)*fact1*fact2*fact3) ) .^2;

CR = other + sqrt(2)*erfcinv(2*Gamma);
%     

end