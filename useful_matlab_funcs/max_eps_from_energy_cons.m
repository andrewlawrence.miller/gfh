function [ eps ] = max_eps_from_energy_cons( fgw0,tau,nn,II )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    G=6.67e-11;
    c=3e8;

    Erot_value = Erot(fgw0,II);


    prefactor = 32*pi^6*G/(5*c^5.);
    tau_nn_term = tau*((nn-1.)/(nn-7.))*(-1.);
    other_Egw_terms = II^2. * fgw0.^6;

    eps = sqrt( Erot_value ./ (prefactor*other_Egw_terms.*tau_nn_term));



end

