function [out]=pick_one_uni_dist(min,max,N)

% reset(RandStream.getDefaultStream,sum(100*clock))
% rng(1)

if ~exist('N','var')
    N=1;
end

out=min+(max-min).*rand(1,N);
