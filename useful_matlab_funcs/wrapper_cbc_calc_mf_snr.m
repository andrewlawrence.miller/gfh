function [rho,CR,dur2] = wrapper_cbc_calc_mf_snr(minf,maxf,mc,h0,ampnoise,base_fmin)
%UNTITLED25 Summary of this function goes here
%   Detailed explanation goes here

if ~exist('ampnoise','var')
    ampnoise=7.94E-04 * 1e-20;
end

t0 = 0;
n = 11/3;
fdotmin=calc_fdot_chirp(mc,base_fmin);
fdotmax=calc_fdot_chirp(mc,maxf);
sour=gen_N_power_law_sigs(1,base_fmin,base_fmin,fdotmin,fdotmin,n,n,h0,t0);          % creates source structure, for chirp sigs, sig's f0 alwyas at minf

t1=calc_time_to_coalescence(mc,base_fmin);      % seconds until coalescence at min and max frequency injected
t2=calc_time_to_coalescence(mc,maxf);   
dur = abs(t2-t1);

TFFT = floor(1 / sqrt(fdotmax));                                             % calculate TFFT using maximum spinup
TFFT = 2 * TFFT; %%roughly captures improvement w/ 'opt' FFT
% if TFFTmax == 0
%     TFFTmax = 1;
% end

% TFFT = 2^floor(log2(rndeven(1/sqrt(max([sour.df0])))));

these_ts = 0:TFFT/2:dur;
% fsss = power_law(these_ts,base_fmin,fdotmin,n);
[~, fsss]=pl_gen_fs_amps(these_ts,sour);

if ischar(ampnoise)
    [det_freq,hnoise] = load_sens_curve(ampnoise);
    index = efficient_index_noise_curve(fsss,det_freq);
    ampnoise = hnoise(index)';
end

inds = fsss>=minf;


dur2 = (calc_time_to_coalescence(mc,minf) - calc_time_to_coalescence(mc,maxf));

% CR = cbc_calc_CR_sens(h0,minf,mc,TFFT,dur,n,ampnoise.^2);
CR= cbc_calc_CR_sens_again(h0,fsss(inds),TFFT,dur,n,ampnoise(inds).^2);
% rho = cbc_calc_mf_snr(fsss,ampnoise.^2,mc,h0);
rho = cbc_calc_mf_snr_actual_sens(h0,fsss(inds),dur,mc,ampnoise(inds).^2);
end