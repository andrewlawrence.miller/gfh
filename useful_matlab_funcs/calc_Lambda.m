function [Lambda] = calc_Lambda(Gamma,theta_thr,CR_thr)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

if ~exist('Gamma','var')
    Gamma=0.95;
end
if ~exist('theta_thr','var')
    theta_thr=2.5;
end
if ~exist('CR_thr','var')
    CR_thr=3.4; 
end
p0=exp(-theta_thr)-exp(-2*theta_thr)+1/3*exp(-3*theta_thr);             
p1=exp(-theta_thr)-2*exp(-2*theta_thr)+exp(-3*theta_thr);
cr_fact=sqrt(CR_thr-sqrt(2)*erfcinv(2*Gamma));

Lambda=4.02/theta_thr^(1/2) * (p0 * (1-p0) / p1^2)^(1/4) * cr_fact;
end

