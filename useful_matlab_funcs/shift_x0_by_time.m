function [x] = shift_x0_by_time(x0,kn,delta_t,n)
%UNTITLED6 Summary of this function goes here
%   delta_t_in secs;
x = x0 + (n-1) .* kn .* delta_t;
end