function [ xdata,ydata,zdata ] = get_fig_data( myfigpath )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
open(myfigpath)
h=gcf;
axesObjs = get(h, 'Children');  %axes handles
dataObjs = get(axesObjs, 'Children');
objTypes = get(dataObjs, 'Type');  %type of low-level graphics object
xdata = get(dataObjs, 'XData');  %data from low-level grahics objects
ydata = get(dataObjs, 'YData');
try
    zdata = get(dataObjs, 'ZData');
catch
    zdata=[];
end

end

