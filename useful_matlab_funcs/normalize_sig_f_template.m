function [Asq,rho_opt_sq] = normalize_sig_f_template(sig_fft,tfft,Sn)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
rho_opt_sq=inner_product(sig_fft,sig_fft,tfft,Sn);
Asq=1/rho_opt_sq; %%A^2 <h|h>=1 --> A^2=1/<h|h>

end

