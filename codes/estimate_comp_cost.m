function [f0s,mcss,time,Nd,num_skyps] = estimate_comp_cost(freqs,mcs,tobs,search_flag,save_flag )
%estimate_comp_cost
%   This function estimates the computational cost for a range of
%   frequencies, chirp masses, and observation time.

%   -----------------------------INPUTS:-----------------------------------

%   freqs:          array of frquencies                                (Hz)
%   mcs:            array of chirp masses                            (Msun)
%   tobs:           observation time                                   (s)

%   -----------------------------OUTPUTS:----------------------------------


%   NONE

%   -----------------------------EXAMPLE:----------------------------------

%   estimate_comp_cost()


if ~exist('freqs','var')
    freqs=20:50:2000;
end

if ~exist('mcs','var')
    mcs=logspace(-7,-2);                                                    % range of chirp masses
%     mcs=logspace(-5,-3);
end

if ~exist('tobs','var')
    tobs=365*86400;%268.37*86400;%86400*365;                                % observation time
end

if ~exist('search_flag','var')
    search_flag='directed';
end
if ~exist('save_flag','var')
    save_flag=0;
end

n=11/3;                                                                     % braking index for inspirals
Kf=1;                                                                       % overesolution frequency factor, set=1


k=1;                                                                        % index that counts the total number of iterations of the for loop
numlins=0;                                                                  % counts the number of times that the linear approximation can be used
numobs=0;                                                                   % counts the number of times that the time to merger exceeds Tobs
max_sens_loss=0.1;                                                          % the maximum loss in sensitivity we tolerate by using a TFFT< TFFT_{max} allowed

fmax=2048;                                                                  % maximum frequency we consider
min_dur=1;                                                                  % minimum duration for a signal (s)
max_su=1;                                                                   % maximum spin-up for a signal (Hz/s)
min_fft=1;                                                                  % minimum duration for TFFT (s)

for i=1:length(freqs)-1                                                     % for each frequency in freqs
    if i==length(freqs)-3
        disp('')
    end
    fdotmins=calc_fdot_chirp(mcs,freqs(i));                                 % calculate min and max fdot between freqs(i) and freqs(i+1) for all chirp masses
    fdotmaxs=calc_fdot_chirp(mcs,freqs(i+1));
    t1s=calc_time_to_coalescence(mcs,freqs(i));                             % calculate time before coalesence at freqs(i) and freqs(i+1) for all chirp masses
    t2s=calc_time_to_coalescence(mcs,freqs(i+1));
    durs=t1s-t2s;                                                           % duration of the inspiral consisdered
    for j=1:length(mcs)                                                     % for each chirp mass in mcs
        dur=durs(j);
        mc=mcs(j);
        t1=t1s(j);
        t2=t2s(j);
        fdotmin=fdotmins(j);
        fdotmax=fdotmaxs(j);
        if dur<min_dur || fdotmax>max_su
            continue;
        end
        if dur>tobs                                                         % calculate the frequency and spin-up at time t2 (or at time t1-dur if dur>tobs)
            dur=tobs;
            last_f=calc_fgw_as_coalescing(mc,t1-dur);
            fdotmax=calc_fdot_chirp(mc,last_f);
            numobs=numobs+1;       
        else
            last_f=calc_fgw_as_coalescing(mc,t2);
            fdotmax=calc_fdot_chirp(mc,last_f);
            
        end
        if last_f>fmax
            continue;
        end
        
        tfft=1/sqrt(2*fdotmax);                                             % calculate corresponding TFFT
        
        if strcmp(search_flag,'all-sky') && tfft>1024                       % all-sky searches cover 10-2048 Hz with TFFT=1024 s
            tfft=1024;
        end
        
        if tfft<min_fft
            continue;                                                       % why top right corner of plot is blank
        else
            TFFT(k)=tfft;
        end
            
        [flag,df]=is_fevol_linear( dur,freqs(i),fdotmin,TFFT(k),n );        % for the desired TFFT, starting freq and starting fdot( fdotmin), determine if lin f evol
        if flag==1                                                          % if linear evolution, we don't consider computation cost
            numlins=numlins+1; %% why bottom left hand corner is blank
            continue;
        else
            max_tfft=1/df;                                                  % determine what the maximum allowed TFFT is to use linear approximation
            sens_loss=1-sqrt(sqrt(max_tfft/TFFT(k)));                       % calculate sensitivity loss by using max_tfft and not TFFT(k)
            if sens_loss>max_sens_loss || max_tfft<1                        % if sensitivity loss is too high, then use Generalized Frequency-Hough
                
                
                [gridk,~]=andrew_long_transient_grid_k(TFFT(k),[freqs(i) last_f],[fdotmin fdotmax],dur,n);      % calculate grid in k
                delta_f(k)=last_f-freqs(i);
                
                num_kpts(k)=length(gridk);
                num_tpts(k)=ceil(dur/TFFT(k));
                mcss(k)=mc;
                tobss(k)=dur;
                f0s(k)=freqs(i);
                fdotminss(k)=fdotmin;
                fdotmaxss(k)=fdotmax;
                last_fs(k)=last_f;
                
%                 if strcmp(search_flag,'all-sky')
                
                Nd(k)=calc_ND(f0s(k),TFFT(k));                          % for each frequency and TFFT, calculate ND, number of points in Doppler band
                num_skyps(k)=4*pi*Kf*Nd(k)^2;
                if num_skyps(k)<1
                    num_skyps(k)=1;
                end
             
%                 end
                
                k=k+1;
                
            else
                if max_tfft>1
                    numlins=numlins+1;
                    continue;
                end
            end
            
        end
    end
end
% time_per_pt=5.581427/(664*364);                                           % this is for a signal lasting 2e4 s covering 10 Hz, in seconds, t/ (nbin_k * nbin_x)
time_per_pt=144.163031/(1859*1434)                                          % t/ (nbin_k * nbin_x) lasting 2e4 s covering 50 Hz, EMPIRICAL, found by running Generalized Frequency Hough at Louvain
time=num_kpts.*num_tpts.*time_per_pt;
inds=time<86400;
plot_triplets(f0s,mcss,time/3600,'o',0,1,1,'parula')
xlabel('frequency (Hz)'); ylabel('chirp mass (M_{sun})')
cblabel('log computational time (hr)')% on one core of Quad-Core Intel Core i7 (hr)')

if save_flag==1

    fname='~/Downloads/dir_comp_cost_optimal_FFT.jpg';
    print(fname,'-djpeg');
end

plot_triplets(TFFT,tobss,time/3600,'p',1,1,1,'parula')
xlabel('Fast Fourier Transform time (s)'); ylabel('Observation time (s)');
cblabel('log computational time (hr)')%on one core of Quad-Core Intel Core i7 (hr)')

if save_flag==1
    fname2='~/Downloads/tobs_vs_tfft_comptime_col_dir.jpg';
    print(fname2,'-djpeg')
end

if strcmp(search_flag,'all-sky')

    figure;myloghist(Nd,100)
    xlabel('number of frequency bins in Doppler band')
    ylabel('counts')
    grid on;
    if save_flag==1
        fname3='~/Downloads/hist_ND_pbh.jpg';
        print(fname3,'-djpeg')
    end

    % plot_triplets(f0s,mcss,num_skyps,'.',1,1,1)

    plot_triplets(Nd,num_skyps,mcss,'.',0,1,1,'cool')
    xlabel('number of frequency bins in Doppler band')
    ylabel('number of sky points'); cblabel('log chirp mass (M_{sun})')
    [~, bb]=min(abs(f0s-500))
    % xline(Nd(bb),'--r',{'[f='f0s(bb),'Limit'})) %f0s(bb)
    
    if save_flag==1
        fname4='~/Downloads/nsky_vs_Nd_mc_col.jpg';
        print(fname4,'-djpeg')
    end

end
    
N_segs=tobs./tobss; % e.g. if for a particular f/Mc point, the comp time is 100 s, then the number of times 100s has to be searched is 1 yr/100 s

disp('total number of days on one CPU')
tot_time=sum(N_segs.*time)/86400

disp('total number of days on 2000 CPUs')
tot_time_2k=tot_time/2000


thr_num_skyps=100;

ii=num_skyps<=thr_num_skyps;

frequencies=f0s(ii);
chirp_masses=mcss(ii);
tt=time(ii);

plot_triplets(f0s(ii),mcss(ii),time(ii)/3600,'o',0,1,1,'parula')
xlabel('frequency (Hz)'); ylabel('chirp mass (M_{sun})')
cblabel('log computational time (hr)')% on one core of Quad-Core Intel Core i7 (hr)')


disp('total number of days on one CPU')
tot_time=sum(N_segs(ii).*time(ii))/86400

disp('total number of days on 2000 CPUs')
tot_time_2k=tot_time/2000*thr_num_skyps

end

