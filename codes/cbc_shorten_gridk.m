function [reduced_gridk] = cbc_shorten_gridk(gridk,mink,maxk,frac_around)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

if ~exist('frac_around','var')
    frac_around=0.15;
end

factor = 1 + frac_around;
kmin = mink / factor;
kmax = maxk * factor;
[~, inddd] = min(abs(kmin-gridk));
[~, ind2] = min(abs(kmax-gridk));
reduced_gridk = gridk(inddd:ind2);

end