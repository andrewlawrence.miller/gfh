function [ fsss ] = power_law( times,f0,fdot0,n )
%Gets the freqeuncy evolution of a power law signal
% 
% times: the times at which you want the frequency evolution (s)
% f0:    the initial frequency of the signal (Hz)
% fdot0: the initial spin-down (or spin-up) of the signal (Hz/s)
% n:     braking index (unitless)
% fsss:  frequencies that follow a power-law evolution (Hz)
% 

pow=n-1;

kn=fdot0./f0.^n;
const=kn.*pow.*f0.^pow;

if n~=1
    if n==11/3
        fsss=f0.*(1-const.*times).^(-1./pow);
    else
        fsss=f0.*(1+const.*times).^(-1./pow);
    end
else
    fsss=f0.*exp(-kn*times);
end

end

