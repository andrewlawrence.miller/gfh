function [ fs ] = f_taylor( times,f0,fdot0,fddot0,fdddot0 )
%Taylor series expansion of the frequency evolution, equation 3
% 
% times:    times at which you want to evalute the frequency (s)
% f0:       starting frequency of signal (Hz)
% fdot0:    spin-up of the signal (Hz/s)
% fddot0:   second order spin-up of signal (Hz/s^2)
% fdddot0:  third order spin-up of signal (Hz/s^3)
% fs:       output frequencies at times (Hz)

if ~exist('fddot0','var')
    fddot0=0;
end

if ~exist('fdddot0','var')
    fdddot0=0;
end


fs=f0+fdot0.*times+fddot0.^2./2*times.^2+fdddot0.^3./9*times.^3;

end

