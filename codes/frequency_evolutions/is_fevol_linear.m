function [ is_linear,df ] = is_fevol_linear( tt,f0,fdot0,TFFT,n,num_bins )
%Determines if the frequency evolution of a signal is linear or not based
%on the FFT length (the frequency bin size) and the signal parameters,
%equation 12
% 
% tt:       times of the signal (s)
% f0:       frequency of the signal (Hz)
% fdot0:    spin-up of signal (Hz/s)
% TFFT:     FFT length (s)
% n:        braking index
% num_bins: number of bins that you allow for difference between linear and
% is_linear: a boolean that says whether the evolution is linear (1) or not
%           (0)
% df:       the difference in freqeuncy between linear and power law as a
%           function of times (Hz)
% power-law frequency evolution

if ~exist('num_bins','var')
    num_bins=1;
end

flins=f_taylor(tt,f0,fdot0);
fpls=power_law(tt,f0,fdot0,n);

df=abs(flins-fpls);
df_fft=1./TFFT;
is_linear=df<df_fft*num_bins;

end

