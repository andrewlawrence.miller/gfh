function [outfs,outds,outTffts,outh0,outTobss ] = make_theo_dist_plot(fmin,fmax,fstep,mcs,tobs,detector,Gamma,theta_thr,CR_thr,tab_flag )
%make_theo_dist_plot 

% This code is a wrapper for pbh_theo_sens and creates plots of the
% sensitivity as a function of frequency, and distance reach as a fucntion
% of frequency, for particular chirp masses.

%   -----------------------------INPUTS:-----------------------------------
%   fmin:           minimum frequency                                  (Hz)
%   fmax:           maximum frequency                                  (Hz)
%   fstep:          step in frequency                                  (Hz)
%   mcs:            chirp mass                                       (Msun)
%   tobs:           observation time                                    (s)
%   detector:       detector name/run as string, e.g. 'O2_L' or 'ET'
%   Gamma:          confidence level
%   theta_thr:      threshold on peak selection in peakmap
%   CR_thr:         threshold on critical ratio in Hough map
%   tab_flag:       whether to make table of mc vs. f vs. dmax (=1 is yes)
%                   and whether to save table of parms for fig. in paper

%   -----------------------------OUTPUTS:----------------------------------

%   NONE; note: directory is hardcoded for outupt tables

%   -----------------------------EXAMPLE:----------------------------------

% make_theo_dist_plot()

if ~exist('fmin','var')
    fmin=50;                                                                    % minimum frequency of distance reach (Hz)
end

if ~exist('fmax','var')
    fmax=2000;                                                                  % maximum frequency of distance reach (Hz)
end

if ~exist('fstep','var')    
    fstep=50;                                                                   % calculate the distance reach every fstep (Hz)
end

if ~exist('mcs','var')
    mcs=[1e-3 1e-4 1e-5];                                                       % chirp mass (Msun)
end

if ~exist('tobs','var')
    tobs=365*86400;                                                             % observation time (s)
end

if ~exist('detector','var')
%     detector='O2_L'; %'ET';
    detector='advL_design';
end


if ~exist('Gamma','var')
    Gamma=0.9;                                                                  % confidence level
    theta_thr=2.5;                                                              % threshold on peak selection in peakmap
    CR_thr=5;                                                                   % threshold on critical ratio in Hough map
end

if ~exist('tab_flag','var')
    tab_flag=0;
end



one_mpc=3.086e+22;                                                          % 1 Mpc = 3.086e22 m

one_kpc=one_mpc/1e3;                                                        % 1 kpc= 1 mpc/1e3

for i=1:length(mcs)
    [ fs(i,:),h0s(i,:),d(i,:),Tobss(i,:),TFFTs(i,:) ]=pbh_theo_sens(detector,mcs(i),tobs,fmin,fmax,fstep,Gamma,theta_thr,CR_thr);
end

if tab_flag==1
    mcs_matrix=mcs'*ones(1,size(fs,2));
    TT=table(fs(:),mcs_matrix(:),d(:),'VariableNames',{'frequency (Hz)','Chirp mass (solar masses)','distance at 90% (meters)'});
    if strcmp(detector,'ET')
        writetable(TT,'~/Desktop/Belgio/dark_matter/pbh_cw/d_vs_mc_for_each_f_ET_under_50Hz.txt')
    elseif strcmp(detector,'advL_design')
         writetable(TT,'~/Desktop/Belgio/dark_matter/pbh_cw/d_vs_mc_for_each_f_advL_design_under_50Hz.txt')
    end
end

% num_tpts=ceil(Tobss./TFFTs);


figure;semilogy(fs',d'/one_kpc)
figure;semilogy(fs',h0s')
xlabel('frequency (Hz)'); ylabel('distance reach (kpc)')
str1=['M_c=' num2str(mcs(1),'%.e') ' ' 'M_{sun}'];
str2=['M_c=' num2str(mcs(2),'%.e') ' ' 'M_{sun}'];
str3=['M_c=' num2str(mcs(3),'%.e') ' ' 'M_{sun}'];
legend(str1,str2,str3)
grid on;
set(gca,'FontSize',14)

plot_triplets(fs(:),d(:)/one_kpc,TFFTs(:),'sq',0,1,1)
xlabel('frequency (Hz)');ylabel('d_{90%} (kpc)'); cblabel('log T_{FFT} (s)')

plot_triplets(fs(:),h0s(:),Tobss(:),'sq',0,1,1)
xlabel('frequency (Hz)');ylabel('h_{0,90%} (kpc)'); cblabel('log T_{obs} (s)')

outfs=fs(:);
outds=d(:)/one_kpc;
outTffts=TFFTs(:);
outh0=h0s(:);
outTobss=Tobss(:);

if tab_flag==1
    mc1=mcs(1);
    mc2=mcs(2);
    mc3=mcs(3);


    T=table(tobs,mc1,mc2,mc3,fmin,fmax,fstep,Gamma,theta_thr,CR_thr);

    writetable(T,'~/Desktop/Belgio/sub_solarmass/paper/parameters/optimal_sens_fig_parms.txt')

end

end