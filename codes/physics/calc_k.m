function [ k ] = calc_k( mc )
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here
c=299792458;
msun=1.99e30;
G=6.626e-11;
k=96/5*pi^(8/3)*(G*mc*msun/c^3)^(5/3);
end

