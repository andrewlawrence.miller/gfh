function [R] = calc_R_as_coalescing(M,fgw)
%UNTITLED21 Summary of this function goes here
%   M=m1+m2
consts=constants;
G=consts.G;
Msun=consts.Msun;

R=(G*M*Msun/(16*pi^2)).^(1/3).*fgw.^(-2/3);
end

