function [d] = calc_chirp_d(h0,Mc,f)
%Calculates the distance away that a signal from a light PBH inspiral is
%from us, eqn. 1 inverted to solve for d
% 
%   h0: gravitational-wave amplitude
%   Mc: chirp mass
%   f: freqeuncy
%   d: distance in kpc

consts=constants;
G=consts.G;
c=consts.c;
Msun=consts.Msun;
units=consts.units;
Mc=Mc*Msun;

d=4./h0.*(G.*Mc/c^2).^(5/3).*(pi.*f/c).^(2/3);


d=d/units.kpc_to_m;

end

