function [ f_isco ] = calc_f_isco( mtot )
%Calculates the frequency at the innermost stable circular orbit (ISCO)
% 
% mtot:     total mass of the binary system (solar masses)
% f_isco:   freqeuncy at ISCO

f_isco=2*2.2e3*(1/mtot); %maggiore, eqn. 4.40 2.2 kHz * (1 solar mass / mtot)

end

