function [ h0 ] = calc_chirp_h0( d,Mc,f )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

consts=constants;
G=consts.G;
c=consts.c;
Msun=consts.Msun;
units=consts.units;

d=d*units.kpc_to_m;
Mc=Mc*Msun;

h0=4/d*(G*Mc/c^2)^(5/3)*(pi*f/c)^(2/3);

end

