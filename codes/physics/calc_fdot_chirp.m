function [ fdot ] = calc_fdot_chirp( mc,fgw )
%Calculates spin-up of a chirping gravitational-wave signal, eqn. 2
% 
% Mc: chirp mass (solar masses)
% fgw: gravitational-wave frequency (Hz)
% fdot: spin-up (Hz/s)
% 

consts=constants;
G=consts.G;
c=consts.c;
msun=consts.Msun;

mc=mc*msun;

fdot=96/5*pi^(8/3)*(G*mc./c^3).^(5/3).*fgw.^(11/3);

end

