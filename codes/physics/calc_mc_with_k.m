function [ mc ] = calc_mc_with_k( k )
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here

consts=constants;
G=consts.G;
c=consts.c;
msun=consts.Msun;


mc=k.^(3/5) * (5 / ( 96 * pi^(8/3) ) )^(3/5) /( G / c^3 );
mc=mc/msun;
end