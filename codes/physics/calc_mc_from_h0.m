function [ Mc ] = calc_mc_from_h0( h,d,f )
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
% n=11/3;
% c=299792458;
% 
% G=6.626e-11;
% msun=1.99e30;
% k=fdot0/f0^n; %%must be positive
% Mc=(k*5/(96*pi^(8/3)))^(3/8)*c^3/G;
% 
% mc=Mc/msun;

consts=constants;
G=consts.G;
c=consts.c;
Msun=consts.Msun;
units=consts.units;

d=d*units.kpc_to_m;

t1=(d.*h./4).^(3/5);
t2=(c./(pi*f)).^(2/5);
t3=c^2/G;

Mc=t1.*t2.*t3;

Mc=Mc/Msun;

% 1.165252789525019e-50* 1/d *Mc^(5/3)*f^(2/3)

end

