function [rate]=calc_rate_VT(dists,delta_T,times)
% Calculates the rate of all binary inspirals in each frequency band,
% summed over frequency; Nbin_{tot}=4/3*pi*\Sum{d^3}, equations 4 and 6
% 
% dists:    distances binaries are away from us, at allowed frequencies
% delta_T:  duration of binaries to go from one frequency to another
% times:    ratio of hubble time to time to Hubble, O(1) for parms we
%           consider
% rate:     the rate, in kpc^{-3} yr^{-1}


if ~exist('times','var')
    times=1;
end

dists_cubed=dists.^3;

summation_term=sum(dists_cubed.* delta_T .* times.^(-34/37)) ;
rate=(3 ./(4*pi*summation_term ) );
