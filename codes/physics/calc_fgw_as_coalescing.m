function [ fgw ] = calc_fgw_as_coalescing( Mc,tau )
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here

cc=constants;
c=cc.c;
G=cc.G;
Msun=cc.Msun;

% fgw=134*(1.21./Mc).^(5/8).*(1./tau).^(3/8);

fgw=1/pi * (5/256 * 1./tau).^(3/8) .* (G .* Mc*Msun ./ c^3) .^(-5/8);

end

