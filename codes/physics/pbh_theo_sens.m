function [ f0s,h00min,ddmax,Tobss,TFFTs ] = pbh_theo_sens( run,mc,max_tobs,fmin,fmax,fstep,Gamma,theta_thr,CR_thr,m1)
%pbh_theo_sens  
% This code calculates the theoretical sensitivity of detecting a pbh
% inspiral for given chirp masses and frequencies at a particular
% confidence level. The equations used here are based on Eq. 

%   -----------------------------INPUTS:-----------------------------------
%   run:            observing run ('O2','O3')                           
%   mc:             chirp mass                                      (Msun)
%   max_tobs:       maximum observation time                        (s)
%   fmin:           minimum frequency                               (Hz)
%   fmax:           maximum frequency                               (Hz)
%   fstep:          frequency step                                  (Hz)
%   Gamma:          confidence level (between 0 and 1)
%   theta_thr:      threshold on peak selection (=2.5 typically)
%   CR_thr:         threshond on crtical ratio in Hough map (=5 typically)

%   -----------------------------OUTPUTS:----------------------------------


%   One summary file containing:
%       f0:         frequencies at which sensitivity is evaluated   (Hz)
%       h00min:     minimum amplitude detectable at Gamma confidence
%       ddmax:      distance reach at Gamma confidence              (m)
%       Tobss:      durations of signals considered in estimation   (s)
%       TFFTs:      FFT lengths used in sensitivity estimation      (s)

%   -----------------------------EXAMPLE:----------------------------------

% pbh_theo_sens( 'O2_L',10^-3,86400*365,50,2000,50, 0.9,2.5,5)
cc=constants;

if ~exist('run','var')
    run='O2_L';
end
if ~exist('mc','var')
    mc=1e-3;
end
if ~exist('max_tobs','var')
    max_tobs=525600*60;
end
if ~exist('Gamma','var')
    Gamma=0.9;
end
if ~exist('theta_thr','var')
    theta_thr=2.5;
end
if ~exist('CR_thr','var')
    CR_thr=5;
end

if ~exist('m1','var')
    m1=1e-10; %%% basically, if not given, isco Frequency is so high that it is ignored
end

n=11/3;
% fmin=50;
% fmax=2000;
% fstep=10;
f0s=fmin:fstep:fmax-fstep;

one_mpc=3.086e+22; %m
one_pc=one_mpc/1e6; %pc
one_kpc=one_mpc/1e3; %kpc

G=6.67e-11;
c=3e8;
Msun=1.993e30;
p0=exp(-theta_thr)-exp(-2*theta_thr)+1/3*exp(-3*theta_thr);             
p1=exp(-theta_thr)-2*exp(-2*theta_thr)+exp(-3*theta_thr);

if strcmp(run,'lisa')
    [Sn] = lisa_Sn(f0s,max_tobs); hnoise = sqrt(Sn.');
    det_freq = f0s;
else
    [ det_freq,hnoise ] = load_sens_curve(run);
end
fstart=det_freq(1);
fstop=det_freq(length(det_freq));


pow=n-1;


fisco=calc_f_isco(m1);
tisco=calc_time_to_coalescence(mc,fisco);

for i=1:length(f0s)
    tini=calc_time_to_coalescence(mc,f0s(i)); 
    tfin=calc_time_to_coalescence(mc,f0s(i)+fstep);
    
    worst_fdot=calc_fdot_chirp(mc,f0s(i)+fstep);
    TFFT=1/sqrt(worst_fdot);
    tobs=tini-tfin;

    if tobs>max_tobs
        tobs=max_tobs;
        last_f=calc_fgw_as_coalescing(mc,tini-tobs);
        last_fdot=calc_fdot_chirp(mc,last_f);
        TFFT=1/sqrt(last_fdot);
    end
    if TFFT > tobs
        TFFT = 86400;
    end
     N=round(tobs/TFFT); % number of FFTs in tobs (the FFTs are interlaced by 50% but the theo sensitivity doesn't care about that) 


    times=0:TFFT:tobs;
    k=calc_k(mc);
    const=k.*pow.*f0s(i).^pow;
    fsss=f0s(i)*(1-const.*times).^(-1./pow);
    
    fsss=fsss(fsss<fisco);
    if isempty(fsss)
        continue;
    end

    for r=1:length(fsss) % for each frequency find the index of the nearest bin of the noise curve 
        [wal index(r)]=min(abs(fsss(r)-det_freq));
    end    


    hnoise_band=hnoise(index);
    clear index
    hnoise_band=hnoise_band';
    Sn=hnoise_band.*hnoise_band; % detector noise power spectrum

    Factor=sum(1./Sn.*fsss.^(4/3)); %%%%%%%%% was 4/6.. a bug!

    fact1=4.02/theta_thr^(1/2);
    fact2=sqrt(N/TFFT)*Factor^(-1/2);
    fact3=(p0*(1-p0)/(N*p1^2))^(1/4);
    fact4=sqrt(CR_thr-sqrt(2)*erfcinv(2*Gamma));
    
    h00min(i)=f0s(i)^(2/3)*fact1*fact2*fact3*fact4;
%     
%     dfact1=0.995*(G*mc*Msun/c^2)^(5/3)*(pi/c)^(2/3);
%     dfact2=TFFT/sqrt(tobs)*sqrt(Factor);
%     dfact3=((p0*(1-p0))/N*p1^2)^(-1/4);
%     dfact4=theta_thr/(CR_thr-sqrt(2)*erfcinv(2*Gamma));
%     
%     ddmax(i)=dfact1*dfact2*dfact3*dfact4;
   
    ddmax(i)=calc_chirp_d(h00min(i),mc,f0s(i))*cc.units.kpc_to_m;
    Tobss(i)=tobs;
    TFFTs(i)=TFFT;
%     minfdot=1/(2*TFFT)^2;
%     maxfdot=1/TFFT^2;

    
end



end

