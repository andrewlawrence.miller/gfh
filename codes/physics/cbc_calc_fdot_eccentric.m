function [ fdot_ecc ] = cbc_calc_fdot_eccentric( mc,fgw,ecc )
%Calculates spin-up of a chirping gravitational-wave signal, eqn. 2
% 
% Mc: chirp mass (solar masses)
% fgw: gravitational-wave frequency (Hz)
% fdot: spin-up (Hz/s)
% 

if ~exist('ecc','var')
    ecc = 0;
end

fdot = calc_fdot_chirp(mc,fgw);

f_of_e = cbc_calc_f_of_ecc(ecc);

fdot_ecc = fdot .* f_of_e;

end

