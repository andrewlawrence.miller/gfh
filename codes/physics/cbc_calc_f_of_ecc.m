function [f_of_e] = cbc_calc_f_of_ecc(ecc)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
term1 = 1 ./ (1 - ecc.^2).^(7/2);
term2 = 1 + 73/24 * ecc.^2 + 37/96 * ecc.^4;

f_of_e = term1 .* term2;
end