function [r_isco] = calc_r_isco(M)
%UNTITLED18 M: m1+m2
%   Detailed explanation goes here
consts=constants;
G=consts.G;
c=consts.c;
Msun=consts.Msun;

r_isco=6*G*M*Msun/c^2;
end

