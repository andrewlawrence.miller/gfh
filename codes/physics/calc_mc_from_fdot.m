function [mc] = calc_mc_from_fdot(f,fdot)
%UNTITLED17 Summary of this function goes here
%   Detailed explanation goes here
consts=constants;
G=consts.G;
c=consts.c;
msun=consts.Msun;

const=5/(96*pi^(8/3));
fundconsts=(c^3/G)^(5/3);

parm=fdot./f.^(11/3);

mc=(const*fundconsts*parm).^(3/5);
mc=mc/msun;

end

