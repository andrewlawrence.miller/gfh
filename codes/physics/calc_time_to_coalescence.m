function [ tau ] = calc_time_to_coalescence( Mc,fgw )
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here

cc=constants;
G=cc.G;
c=cc.c;
Msun=cc.Msun;


% tau=2.18*(1.21./Mc).^(5/3).*(100./fgw).^(8/3);

tau=5/256 * (pi*fgw).^(-8/3) .* (G * Mc*Msun / c^3).^(-5/3);


end

