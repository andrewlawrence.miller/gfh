function [mccs,fdots,fsss ] = make_spinup_cw_plot( f0s,mcs,cw_maxsu )
%make_spinup_cw_plot
% This code creates a plot of spinup vs. chirp mass with frequency colored
% to be used in the paper to show the parameter range to which CW and tCW
% searches are sensitive to

%   -----------------------------INPUTS:-----------------------------------
%   f0s:            array of frequencies                               (Hz)
%   mcs:            array of chirp masses                            (Msun)
%   cw_maxsu:       maximum spinup considered in CW searches         (Hz/s)


%   -----------------------------OUTPUTS:----------------------------------

%   NONE

%   -----------------------------EXAMPLE:----------------------------------

% make_spinup_cw_plot()

if ~exist('f0s','var')
    f0s=20:1:2000;
end
if ~exist('mcs','var')
    mcs=logspace(-7,-2);
end

if ~exist('cw_maxsu','var')
    cw_maxsu=1e-8;
end

fdots=[];
mccs=[];
fsss=[];
tcoals=[];
tminobs=1000;
fmax=2048;
for i=1:length(mcs)
    mc=mcs(i);
    fdot=calc_fdot_chirp(mc,f0s);
    tcoal=calc_time_to_coalescence(mc,f0s);
    tmax=calc_time_to_coalescence(mc,fmax);
    inds=abs(tcoal-tmax)>tminobs;
    fdots=[fdots fdot(inds)];
    mccs=[mccs mc*ones(size(fdot(inds)))];
    fsss=[fsss f0s(inds)];
    tcoals=[tcoals tcoal(inds)];

end


plot_triplets(mccs,fdots,fsss,'.',1,1,1)
xlabel('chirp mass (M_{sun})'); ylabel('spin-up (Hz/s)'); cblabel('log oribtal frequency (Hz)')
hold on;
plot(mccs,cw_maxsu*ones(size(mccs)),'ksq','LineWidth',1.5)


% plot_triplets(fsss,fdots,tcoals/(525600*60),'.',1,1,1)
% xlabel('frequency (Hz)'); ylabel('spinup (Hz/s)'); cblabel('log t_{coal} (yr)')
% hold on;
% plot(fsss,2e-9*ones(size(fsss)),'ksq','LineWidth',1.5)


end