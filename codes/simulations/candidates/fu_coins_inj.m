function [ found,best_cand,false_alarm,mindist,dist,dist_each_parm_best_inj ] = fu_coins_inj(cand2,job_info,source,coin_dist)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

if ~exist('coin_dist','var')
    coin_dist=3;
end




theo_f0=source.f0;
theo_fdot0=source.df0;
theo_n=source.n;

% dx=job_info.dx;
%dksss=job_info.kbins;
%df=job_info.df;


cand_f0=cand2(1,:);
cand_sd=cand2(4,:);
cand_n=cand2(7,:);
cand_dsd=cand2(8,:);
% cand_dn=cand2(9,:);
% cand_dx=cand2(13,:);
cand_df=job_info.df;
cand_dsd=job_info.dsd;

diff_f0=abs(cand_f0-theo_f0);
diff_sd=abs(cand_sd-theo_fdot0);
diff_n=abs(cand_n-theo_n);

term1=(diff_f0./cand_df).^2;
term2=(diff_sd./cand_dsd).^2;
dist=sqrt(term1+term2);
[mindist,ind]=min(dist);
dist_each_parm_best_inj=sqrt([term1(ind) term2(ind)]); %x,k,n
best_cand=cand2(:,ind);

off=coin_dist-dist;
inds=find(off>0);
if length(inds)>0
    found=true;
else
    found=false;
end


%false alarm

N=length(cand_f0);
cand_rdn_f0=cand_f0+rand(1,N).*coin_dist/2.*sign(rand(1,N)-0.5);
cand_rdn_sd=cand_sd+rand(1,N).*coin_dist/2.*sign(rand(1,N)-0.5);
% cand_rdn_n=cand_n+rand(1,N).*coin_dist/2.*sign(rand(1,N)-0.5);

diff_rdn_x0=abs(cand_rdn_f0-theo_f0);
diff_rdn_kn=abs(cand_rdn_sd-theo_fdot0);
% diff_rdn_n=abs(cand_rdn_n-theo_n);

term1_rdn=(diff_rdn_x0./cand_df).^2;
term2_rdn=(diff_rdn_kn./cand_dsd).^2;
% term3_rdn=(diff_rdn_n./cand_dn).^2;
dist_rdn=sqrt(term1_rdn+term2_rdn);
[mindist_rdn,ind_rdn]=min(dist_rdn);

cand_false=cand2(:,ind_rdn);

off_rdn=coin_dist-dist_rdn;
inds_rdn=find(off_rdn>0);



if length(inds_rdn)>0
    false_alarm=true;
else
    false_alarm=false;
end


end

