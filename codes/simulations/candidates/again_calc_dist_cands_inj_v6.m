function [ found,best_cand,false_alarm,mindist,dist,dist_each_parm_best_inj ] = again_calc_dist_cands_inj_v6(cand2,job_info,source,coin_dist)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

if ~exist('coin_dist','var')
    coin_dist=3;
end




theo_x0=source.x0;
theo_kn=source.kn;
theo_n=source.n;

% dx=job_info.dx;
%dksss=job_info.kbins;
%df=job_info.df;


cand_x0=cand2(1,:);
cand_kn=cand2(4,:);
cand_n=cand2(7,:);
cand_dk=cand2(8,:);
cand_dn=1;%cand2(12,:);
cand_dx=cand2(13,:);


diff_x0=abs(cand_x0-theo_x0);
diff_kn=abs(cand_kn-theo_kn);
diff_n=abs(cand_n-theo_n);

term1=(diff_x0./cand_dx).^2;
term2=(diff_kn./cand_dk).^2;
term3=(diff_n./cand_dn).^2;
dist=sqrt(term1+term2+term3);
[mindist,ind]=min(dist);
dist_each_parm_best_inj=sqrt([term1(ind) term2(ind) term3(ind)]); %x,k,n
best_cand=cand2(:,ind);

off=coin_dist-dist;
inds=find(off>0);
if length(inds)>0
    found=true;
else
    found=false;
end


%false alarm

N=length(cand_x0);
cand_rdn_x0=cand_x0+rand(1,N).*coin_dist/2.*sign(rand(1,N)-0.5);
cand_rdn_kn=cand_kn+rand(1,N).*coin_dist/2.*sign(rand(1,N)-0.5);
cand_rdn_n=cand_n+rand(1,N).*coin_dist/2.*sign(rand(1,N)-0.5);

diff_rdn_x0=abs(cand_rdn_x0-theo_x0);
diff_rdn_kn=abs(cand_rdn_kn-theo_kn);
diff_rdn_n=abs(cand_rdn_n-theo_n);

term1_rdn=(diff_rdn_x0./cand_dx).^2;
term2_rdn=(diff_rdn_kn./cand_dk).^2;
term3_rdn=(diff_rdn_n./cand_dn).^2;
dist_rdn=sqrt(term1_rdn+term2_rdn+term3_rdn);
[mindist_rdn,ind_rdn]=min(dist_rdn);

cand_false=cand2(:,ind_rdn);

off_rdn=coin_dist-dist_rdn;
inds_rdn=find(off_rdn>0);



if length(inds_rdn)>0
    false_alarm=true;
else
    false_alarm=false;
end


end

