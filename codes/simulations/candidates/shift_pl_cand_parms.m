function [ cand2 ] = shift_pl_cand_parms( cand2,sour,ref_perc_time )
%UNTITLED24 Summary of this function goes here
%   Detailed explanation goes here

if ref_perc_time~=0  %%% shifts the candidate to t=0 reference time because sour is at t=0
    cand2(1,:)=cand2(1,:)+(sour.n-1)*cand2(4,:).*(sour.fepoch-cand2(9,:))*86400;
    cand2(14,:)=cand2(1,:).^(-1./(cand2(7,:)-1));
    cand2(9,:)=sour.fepoch;
end
    
end

