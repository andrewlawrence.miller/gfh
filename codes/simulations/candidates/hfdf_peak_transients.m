function [cand, job_info]=hfdf_peak_transients(hfdf,hm_job, kcand,deltaf,dn)
% andrew_hfdf_peak: finds candidates in the hough map  for each block of
% x/k, number of squares = kcand^2, one candidate from each square when
% mode=1, else can be more
%
%    Input parameters:
%    hfdf           hough map
%    patch          location of the source in ecliptic coordinates
%    hm_job         parameters used in the creation of the hough map
%    kcand          number of primary candidates to be found
%    braking_index  the braking index for which the hough was done
%    dx             the step in x (related to frequency) used in the hough
%    dk             the step in k (related to spindown) used in hough 
%    num_kbins      the number of bins in the paraemter k in the hough
%    deltaf         how much of the high end of the frequency range to
%                   exclude for candidate selection- this is done because the noise is
%                   nonuniform and is always worse at higher frequencies

%    Outputs
%   candidate matrices (N,8)
%         1.	x   ?
%         2.	ecl. long.  ?
%         3.	ecl. lat.   ?
%         4.	k   
%         5.	amplitude   A (hough count)
%         6.	CR (global)
%         7.    braking index
%         8.    step in k
%         9.    t0 (for the Hough), actual start time of signal >= this val
%         10.   duration (signal <= this duration)
%         11.   1/TFFT to be used later...
%         12.   dn
%         13.   dx
%         14.   Recovered f0 (calculated based on parms 1,4,7)
%         15.   Recovered fdot0 ((calculated based on parms 1,4,7,14)
%    job_info    job info structure: contains dx, etc.
tic;
braking_index=hm_job.n;
pow=braking_index-1;

mno=hm_job.frenh*3;%factor that tells you have far away you are in k (spindown)- if a candidate is sufficiently far away from another, select it
mode=2; %set so that we will always select candidates that are sufficiently far away from another, select it
frini=hm_job.minf;
frfin=hm_job.maxf;

xmin=1/(frfin-deltaf)^pow; %the minimum x increases when you cut a frequency region out with deltaf
xmax=1/frini^pow;


non_uni_sd_grid=hm_job.gridk;
dur=hm_job.dur;
num_kbins=length(non_uni_sd_grid);
dx=hm_job.dx;
try
    patch=hm_job.patch;
catch
    patch = [0 0];
end
tini=hm_job.epoch;


cand=zeros(15,kcand);

g=cut_gd2(hfdf,[xmin,xmax],[-1000000,1000000],1); %extract portion of hough map you want to select candidates on %was [frin,frfin]
%plot(g)
y=y_gd2(g);
frrrr=x_gd2(g); %only if fr grid is uniform

sdddd=x2_gd2(g);
if exist('non_uni_sd_grid','var')
    sdddd=non_uni_sd_grid;
    sd_steps=diff(sdddd);
    sd_steps=[sd_steps sd_steps(end)];
end

r1=size(y,1);
r2=length(sdddd);
df=r1/kcand;
dsd=r2/kcand;

ix_df=round(1:df:r1);
ix_dsd=round(1:dsd:r2);
ix_df=[ix_df r1];
ix_dsd=[ix_dsd r2];

robst=robstat(y(:),0.01);
job_info.robst=robst;
robmedtot=robst(1);
% robmed=zeros(1,kcand);
% robstd=robmed;

jj=0;
for jjj=1:length(ix_df)-1
    for lll=1:length(ix_dsd)-1
        block=y(ix_df(jjj):ix_df(jjj+1),ix_dsd(lll):ix_dsd(lll+1));
        fr=frrrr(ix_df(jjj):ix_df(jjj+1));
        sd=sdddd(ix_dsd(lll):ix_dsd(lll+1));
        these_sd_steps=sd_steps(ix_dsd(lll):ix_dsd(lll+1));
    [ym im]=max(block');
    %figure;plot(ym)
    robst=robstat(ym,0.01);
    robmed=robst(1);
    robstd=robst(2);
    
    
     if robmed > 0
%        ii=ix(i);
        yy=ym;%ym(ix(i):ix(i+1)-1);
        [ma,ima]=max(yy);
       if ma > robmed && ma > robmedtot/2
            jj=jj+1;
%             iii=ii+ima-1;
            iii=ima;
           %cand(1,jj)=fr(im(iii));
            cand(1,jj)=fr(iii); %x
            cand(2,jj)=patch(1);%lon
            cand(3,jj)=patch(2); %lat
%            cand(4,jj)=sd(iii);
            cand(4,jj)=sd(im(iii)); %y
%            cand(4,jj)=sd(iii);
            cand(5,jj)=ma; %amplitude (the number count)
            cand(6,jj)=(ma-robmed)/robstd; %critical ratio
            cand(7,jj)=braking_index;
 %           cand(8,jj)=these_sd_steps(iii);
            cand(8,jj)=these_sd_steps(im(iii));
            cand(9,jj)=tini;
            cand(10,jj)=dur;
            cand(11,jj)=hm_job.df;
            cand(12,jj)=dn;
            cand(13,jj)=dx;
            cand(14,jj)=cand(1,jj)^(-1/(cand(7,jj)-1));
            cand(15,jj)=cand(4,jj)*cand(14,jj)^cand(7,jj);
 

%             cand(7,jj)=0;%job_info.patch(3)/2;
%             cand(8,jj)=0;%abs(job_info.patch(4)-job_info.patch(5))/4;
%             cand(9,jj)=1;
            if mode == 2 %if mode ==2, selects another candidates sufficiently far away in k
                i1=max(ima-mno,1);
                i2=min(ima+mno,length(yy));
                yy(i1:i2)=0;
                [ma1,ima1]=max(yy);
                if abs(ima1-ima) > 2*mno 
                    if ma1 > robmed
                        jj=jj+1;
                        %iii=ii+ima1-1;
                        iii=ima1;
                        cand(1,jj)=fr(iii);
                        %cand(1,jj)=fr(im(iii));
                        cand(2,jj)=patch(1);
                        cand(3,jj)=patch(2);
%                        cand(4,jj)=sd(iii);
                        cand(4,jj)=sd(im(iii));
                        
                        cand(5,jj)=ma1;
                        cand(6,jj)=(ma-robmed)/robstd;
                        cand(7,jj)=braking_index;
                       % cand(8,jj)=these_sd_steps(iii);
                        cand(8,jj)=these_sd_steps(im(iii));
                        cand(9,jj)=tini;
                        cand(10,jj)=dur;
                        cand(11,jj)=hm_job.df;
                        cand(12,jj)=dn;
                        cand(13,jj)=dx;
                        cand(14,jj)=cand(1,jj)^(-1./(cand(7,jj)-1));
                        cand(15,jj)=cand(4,jj)*cand(14,jj)^cand(7,jj);


%                         cand(7,jj)=0;%job_info.patch(3)/2;
%                         cand(8,jj)=0;%abs(job_info.patch(5)-job_info.patch(4))/4;
%                         cand(9,jj)=2;
                    end
                end
            end
        end
    end
    
    end 
end

if braking_index==5 || braking_index == 3 || braking_index == 7
else
    cand(4,:)=-cand(4,:);
end

job_info.ncand=jj;
job_info.proc.F_hfdf_peak.duration=toc;
job_info.dx=dx;
%job_info.dk=dk;
job_info.num_kbins=num_kbins;
job_info.n=braking_index;
job_info.df=hm_job.df;job_info.ncand=jj;
job_info.proc.F_hfdf_peak.duration=toc;
job_info.dx=dx;
%job_info.dk=dk;
job_info.num_kbins=num_kbins;
job_info.n=braking_index;
job_info.df=hm_job.df;



end






