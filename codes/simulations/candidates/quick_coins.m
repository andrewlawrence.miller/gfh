function [found,best_dist,dist_each_parm_best_inj,best_cands]=quick_coins(cand2,job_info,source,dist_coin_with_inj)

best_cands=[];
for jj=1:length(source)
    sourr=source(jj);
    
    [found(jj),best_cand,false_alarm(jj),best_dist(jj),~,dist_each_parm_best_inj(jj,:)]=...
        again_calc_dist_cands_inj_v6(cand2,job_info,sourr,dist_coin_with_inj); %+1.5 for additional bins in n
    
    best_cands=[best_cands best_cand];
end