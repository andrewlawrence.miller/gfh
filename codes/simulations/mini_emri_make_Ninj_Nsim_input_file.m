function [] = mini_emri_make_Ninj_Nsim_input_file(outdir,where_am_i)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
if ~exist('outdir','var')
    outdir = '/dcache/gravwav/amiller/output/sim_mini_emri/';
end

if ~exist('where_am_i','var')
    where_am_i = 'nikhef';
end
Nsim = 100;
Ninj = 1; %%% for m1 = 10msun
fmins1 = 40:10:120;%[40 80 120 160 200 240 280 320 360];
fmaxs1 = fmins1 + 10;
fmins2 = 40:40:160;
fmaxs2 = fmins2 + 40;

fmins = [fmins1 fmins2];
fmaxs = [fmaxs1 fmaxs2];
Mc = 10;

white_noise = 1; %%%ampnoise=7.94e-24, almost exactly what it is at 4 Hz!
% amp = [1e-24 1e-23 1e-22];%[1e-24 5e-24 8e-24 1e-23 3e-23 5e-23];
% amp = 1;
amp = logspace(-24,-22,50);

sig_type = 'mini_emri';

fname = '~/Downloads/mini_emri_Ninjsim.txt';
ii = 1;
fid = fopen(fname,'wt');
for l = 1:length(fmins)
    fmin = fmins(l);
    fmax = fmaxs(l);

    str_fmin = sprintf( '%04d', fmin );
    str_fmax = sprintf( '%04d', fmax );

    if strcmp(sig_type,'mini_emri')
        outdir_new = strcat(outdir,num2str(Mc),'Msun/','f_range_',str_fmin,'_',str_fmax,'/');
    end


    for j=1:length(amp)
        for k = 1:length(Ninj)
            matr(ii,:)=[Ninj(k) Nsim fmin fmax Mc white_noise amp(j)];
            fprintf(fid,'%s\t',num2str(matr(ii,:)));
            fprintf(fid,'%s\t',sig_type,outdir_new,where_am_i);
            fprintf(fid,'\n');
            ii=ii+1;
        end
    
    end
end
fclose(fid);
end