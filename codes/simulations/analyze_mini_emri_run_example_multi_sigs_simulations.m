function [h0_95,h0_95_track,h0min,fmins,allamps,all_alleffs,all_alleffs_track] = ...
    analyze_mini_emri_run_example_multi_sigs_simulations(CR_thr,bin_thr,Gamma,indir)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

if ~exist('CR_thr','var')
    CR_thr = 5;
end

if ~exist('bin_thr','var')
    bin_thr = 4;
end

if ~exist('indir','var')
    indir = '~/Desktop/10Msun/f_ranges_10_Hz/';
end

if ~exist('Gamma','var')
    Gamma = 0.95;
end

subdirs = dir(strcat(indir,'/f_*'));
Sn = (7.94e-04 * 1e-20)^2;
theta_thr = 2.5;
sig_type = 'mini_emri';
% figure;
for onedir = 1:length(subdirs)
    files = dir(strcat(indir,subdirs(onedir).name,'/*.mat'));
    
    folder = files(1).folder;
    
    ind_22 = 1;
    ind_23 = ind_22;
    ind_24 = ind_22;
    ind_ninj = ind_22;
    
    allmcs = [];
    allf0s = [];
    allcrs = [];
    for file=1:length(files)
        oo = load(strcat(folder,'/',files(file).name));
        amp = oo.amp;
        fmin = oo.fmin;
        fmax = oo.fmax;
        Ninj = oo.Ninj;
        Nsim = oo.Nsim;
        M1 = oo.Mc;
        best_cand = oo.best_cand;
        try
            cr_track = oo.cr_track;
            cr_track = [cr_track{:}];
        catch
            cr_track = -1000;
        end
        dur = best_cand{1}(10);
        TFFT = 1/best_cand{1}(11);
        if Ninj<10 && Ninj~=1 && Ninj~=5
            continue;
        else
            fmax = oo.fmax;
            f0s = [oo.sour{1}.f0];
            allf0s = [allf0s f0s];
    %         mcs = [oo.sour{1}.mc];
    %         allmcs = [allmcs mcs];   
        end
        eff = oo.eff;
        alldistaways = [oo.distaway{:}];
        allcands = [oo.best_cand{:}];
        CRcands = allcands(6,:);
        found = alldistaways<bin_thr & CRcands>CR_thr;
        eff = sum(found) / length(found);
        if amp==1e-22
            effs_22(ind_22) = eff;
            ind_22 = ind_22 + 1;
        elseif amp==1e-23
            effs_23(ind_23) = eff;
            ind_23 = ind_23 + 1;
        elseif amp==1e-24
            effs_24(ind_24) = eff;
            ind_24 = ind_24 + 1;
        end
        Ninjs(ind_ninj) = Ninj;
        ind_ninj = ind_ninj + 1;
        alleffs(file) = eff;
        allamps(file) = amp;
        allcrs = [allcrs CRcands];
        
        if ~(cr_track==-1000)
            found_cr_track = cr_track>CR_thr;
            alleffs_track(file) = sum(found_cr_track) / length(found_cr_track);
        end
    end
    [allamps,index] = sort(allamps);
    alleffs = alleffs(index);
    alleffs_track = alleffs_track(index);
%     semilogx(allamps, alleffs,'.'); hold on
%     semilogx(allamps,eff_track,'s'); hold on;
    unique_Ninj = unique(Ninjs);
    
    Ntrials = Nsim * unique_Ninj;
    TFFTs(onedir) = TFFT;

%     [full_mini_ts,full_mini_fs] = mini_emri_load_tf(oo.sour{1}.m1);
%     times_of_mini_emri = full_mini_ts(full_mini_fs>=fmin & full_mini_fs<=fmax);
%     duration = max(times_of_mini_emri-min(times_of_mini_emri));
    duration = oo.best_cand{1}(10);

    durs(onedir) = duration;
    fmins(onedir) = fmin;
    fmaxs(onedir) = fmax; 

%     ft_hough = fit(log10(allamps)',alleffs','pchipinterp');
%     fun = @(x) Gamma - ft_hough(x);
%     h0_95(onedir) = 10^(fzero(fun, 0)); 
%     
    vals = 1e-10 * (1:length(alleffs));
    newamps = logspace(-24,-22,10000);
    neweffs = interp1(allamps,alleffs + vals,newamps,'pchip');
    neweffs_track = interp1(allamps,alleffs_track + vals,newamps,'pchip');
    h0_95(onedir) = newamps(find(neweffs>=Gamma,1,'first'));
    h0_95_track(onedir) = newamps(find(neweffs_track>=Gamma,1,'first'));

%     h0_95(onedir) = interp1(alleffs + vals,allamps,Gamma,'nearest');
%     h0_95_track(onedir) = interp1(alleffs_track + vals,allamps,Gamma);

    sour = oo.sour{1};
    kn = sour.kn;
    mc = calc_mc_with_k(-kn);
    n = sour.n;
    braking_inds(onedir) = oo.sour{1}.n;
    [h0min(onedir),dmax(onedir)] = mini_emri_sens(f0s,fmax,mc, ...
        TFFT,duration * 0.87,n,Sn,Gamma,CR_thr,theta_thr,sig_type); 
    %%% 0.87 is fudge fact b/c pm are shorter than injected dur
    all_alleffs(onedir,:) = alleffs;
    all_alleffs_track(onedir,:) = alleffs_track;
end                                                                 
% yline(conf_lvl)
% xlabel('h_0'); ylabel('efficiency')
% set(gca,'FontSize',14)
% xline(h0min)

figure; plot(fmins,h0_95./ h0min,'.-k')
hold on; plot(fmins,h0_95_track ./ h0min,'s-r')
xlabel('frequency (Hz)' ); ylabel('sensitivity loss w.r.t. theoretical formula')
set(gca,'FontSize',14)

figure; semilogy(fmins,h0_95,'.-k');hold on; semilogy(fmins,h0_95_track,'s-r')
semilogy(fmins,h0min,'b-')
xlabel('frequency (Hz)' ); ylabel('h_0')
set(gca,'FontSize',14)

% t1 = calc_time_to_coalescence(allmcs,allf0s);
% t2 = calc_time_to_coalescence(allmcs,fmax);
% alldurs = abs(t2-t1);
% figure;plot(unique(Ninjs),effs_22,'-o');
% hold on; plot(unique(Ninjs),effs_23,'-o');
% hold on; plot(unique(Ninjs),effs_24,'-o')
% 
% figure;hist(allmcs,50)
% figure;hist(allf0s,50)
% figure;hist(alldurs,50)

end