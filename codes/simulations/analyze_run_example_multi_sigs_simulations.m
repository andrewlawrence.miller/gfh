function [unique_Ninj,Ntrials,effs_22,effs_23,effs_24,...
    alldurs,allmcs,allf0s] = ...
    analyze_run_example_multi_sigs_simulations(CR_thr,bin_thr,indir)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

if ~exist('CR_thr','var')
    CR_thr = 3;
end

if ~exist('bin_thr','var')
    bin_thr = 3;
end

if ~exist('indir','var')
    indir = '~/Desktop/10Msun/f_range_0080_0120/';
end
files = dir(strcat(indir,'*.mat'));

folder = files(1).folder;

ind_22 = 1;
ind_23 = ind_22;
ind_24 = ind_22;
ind_ninj = ind_22;

allmcs = [];
allf0s = [];
allcrs = [];
for file=1:length(files)
    oo = load(strcat(folder,'/',files(file).name));
    amp = oo.amp;
    Ninj = oo.Ninj;
    Nsim = oo.Nsim;
    if Ninj<10 && Ninj~=1 && Ninj~=5
        continue;
    else
        fmax = oo.fmax;
        f0s = [oo.sour{1}.f0];
        allf0s = [allf0s f0s];
%         mcs = [oo.sour{1}.mc];
%         allmcs = [allmcs mcs];   
    end
    eff = oo.eff;
    alldistaways = [oo.distaway{:}];
    allcands = [oo.best_cand{:}];
    CRcands = allcands(6,:);
    found = alldistaways<bin_thr & CRcands>CR_thr;
    eff = sum(found) / length(found);
    if amp==1e-22
        effs_22(ind_22) = eff;
        ind_22 = ind_22 + 1;
    elseif amp==1e-23
        effs_23(ind_23) = eff;
        ind_23 = ind_23 + 1;
    elseif amp==1e-24
        effs_24(ind_24) = eff;
        ind_24 = ind_24 + 1;
    end
    Ninjs(ind_ninj) = Ninj;
    ind_ninj = ind_ninj + 1;
    alleffs(file) = eff;
    allamps(file) = amp;
    allcrs = [allcrs CRcands];
end
figure;semilogx(allamps, alleffs,'-o')

unique_Ninj = unique(Ninjs);

Ntrials = Nsim * unique_Ninj;

t1 = calc_time_to_coalescence(allmcs,allf0s);
t2 = calc_time_to_coalescence(allmcs,fmax);
alldurs = abs(t2-t1);
% figure;plot(unique(Ninjs),effs_22,'-o');
% hold on; plot(unique(Ninjs),effs_23,'-o');
% hold on; plot(unique(Ninjs),effs_24,'-o')
% 
% figure;hist(allmcs,50)
% figure;hist(allf0s,50)
% figure;hist(alldurs,50)

end