function [job_pack_0]=inject_power_law_signal(simsour,freq_band,t00,duration,TFFT,sfdbdir,sig_type,icwhite,thr,peak_flag,zero_flag)
%inject_power_law_signal 
% This code injects a power law signal into real data. It then creates a 
% time/frequency peakmap with the chosen TFFT. 
% If simsour is not a structure, no injection is done, only sfdb -> peakmap

%   -----------------------------INPUTS:-----------------------------------
%   simsour:        source parms; if not struct, no inj & sfdb --> peakmap
%   freq_band:      [min_freq max_freq]   
%   t00:            GPS time of when to start injection, or to load sfdb
%   duration:       duration of signal to inject
%   TFFT:           FFT length used to calculate the peakmap
%   sfdbdir:        directory where SFDBs are
%   icwhite:        specify 1 for white noise, else it's real noise
%   -----------------------------OUTPUTS:----------------------------------

%   job_pack_0:     structure containing peakmap and other information
%       .basic_info
%           .freq_band  frequency band [minf maxf]                      (Hz)              
%           .index      array of indices separating each time segment
%           .npeak_fft  number of peaks per fft
%           .npeaks     total number of peaks
%           .tfft       FFT duration                                    (s)
%           .dfr        frequency resolution , 1/tfft                   (Hz)
%           .vel        detector velocity (3 components at each time)/c 
%           .pos        detector position (3 components at each time)
%           .ntim       number of times;  
%           .tim        unique times in peakmap                         (MJD)
%       .peaks          collection of peaks (3,n) [time freq amp]
% 
%   -----------------------------EXAMPLE:----------------------------------

% sour=gen_N_power_law_sigs(1,140,150,4.24e-04,5.46e-04,11/3,11/3,1e-21);
% job_pack_0_thresh=inject_power_law_signal(sour,[140 150],1.238789856000000e+09,20520,32,'~/Desktop/sfdbs/H/');


format long

if ~exist('sig_type','var')
    sig_type='power_law';
end

if ~exist('icwhite','var')
    icwhite=0;
end

if ~exist('thr','var')
    thr=2.5;
end

if ~exist('peak_flag','var')
    peak_flag=1;
end

if ~exist('zero_flag','var')
    zero_flag=0; %% if peak_flag = 1, then this doesn't matter
end
fact=0; %%%% if you want to extra noise after signal, this is the number of FFTs (+1) that you want to add
tdiff=102400; %time between SFDBs in O2L;
SD=86164.09053083288; %sidereal day
nsid=10000;
   
dinfo = dir(strcat(sfdbdir,'*.SFDB09'));
filepia = {dinfo.name};

det=filepia{1}(1);

if strcmp(det,'L')
    antenna=ligol;
elseif strcmp(det,'H')
    antenna=ligoh;
end

all_peaks_thresh=[];
all_numpeaks_thresh=[];
all_times_thresh=[];

all_psss=[];
all_vsss=[];


%to determine # of files needed/ tfft
t0=gps2mjd(t00);
nfil=length(filepia);

if strcmp(sig_type,'mini_emri')
    [full_mini_ts,full_mini_fs] = mini_emri_load_tf(simsour.m1);
end


for i = 1:nfil %it's the number of files, not the number of FFTs,
    
%   ----------------------Choose correct SFDB------------------------------

    file=strcat(sfdbdir,filepia{i});
    [ yr,mo, day, ho,mi, sec ] = sfdb_name_to_time_vec( filepia{i} );
    sfdb_t0=utc2gps(yr,mo,day,ho,mi,sec);

    if sfdb_t0+tdiff<t00
        disp('skipping files just based on name')
        continue;
    else
        disp('chosen file')
        filepia{i}
    end
%   ----------------------Open SFDB---------------------------------------

    fidpia=fopen(file);

    if fidpia <= 0
        disp([file ' not opened'])
        return
    end
%   ----------------------Load first FFT in SFDB---------------------------

    [master_piahead,~,master_sps,master_sft]=pia_read_block_09(fidpia);

    if ~exist('dsfact','var')
        disp('calculating dsfact')
        [dsfact,NORM]=calc_dsfact(master_piahead.tsamplu,freq_band);
    end
    
%   -------If SFDB selected does not actually contain t00, skip file------- (but why would this happen?)

    if master_piahead.gps_sec<t00 && master_piahead.gps_sec+tdiff<t00
        disp('skipping files')
        fclose(fidpia);
        continue;
    end
    
%   --------Find first FFT whose t0 closest to desired signal's t00--------
    
    arb=0;
    num_tot_ffts=ceil(duration/(master_piahead.tbase/2)) %ceil b/c take additional fft even though won't need all of it

    if i==1 || ~exist('start_aaa','var')% the starting time of this sfdb is before t00i
          [ master_piahead,master_sps,master_sft,num_ffts_skipped,t00] = find_starting_fft( master_piahead,t00,TFFT,fidpia,master_sft,master_sps ); %finds FFT closest to t00d
          if master_piahead.eof==1
              continue; % advance to next file i--> i+1
          end  %ensures that the FFT you load starts at or contains t0
    end
    disp('chosen fft')
    master_piahead    
    
%   --------CORE: many FFTs are in each SFDB, as long as another FFT can be loaded, the loop continues--------------
    
    while master_piahead.eof==0
        
% ---------Change FFT length if TFFT<TFFT_{file}--------------------------
        
        if master_piahead.tbase~=TFFT
            % e.g. 1024 s changed to 128 s --> 1 FFT becomes 8 FFTs
            [sftsss, piaheadss,spsss]=change_fft_length(master_sft,master_piahead,TFFT);% put 1 for scramble, nothing for normal
        elseif master_piahead.tbase==TFFT
            sftsss{1}=master_sft;
            piaheadss{1}=master_piahead;
            spsss{1}=master_sps;
        else
            disp('not going to work')
            return;
        end
% ---------get indices of starting and ending fft--------------------------

        [ start_aaa,N_end ] = find_start_end_ffts( piaheadss,t00,duration,fact );
        
        if N_end==1
            disp('N_end = 1, getting out of while loop')
            break;
        end

% ----Simulate and inject signal into ffts between start_aaa and N_end-----

        for aaa=start_aaa:N_end

            sft=sftsss{aaa};
            piahead=piaheadss{aaa};
            sps=spsss{aaa};
            
            inan=check_nan(sft,1);
            sft(inan)=0;
            chzero=sum(abs(sft));
            
% ---------get indices of particular frequency band in SFT and sps and downsampled dt and lfft --------------------------
            
            if (i == 1 && aaa==start_aaa) || ~exist('k1','var')
                dfr=piahead.deltanu;
                dtfft=piahead.tbase/2;
                mjd1=gps2mjd(piahead.gps_sec)-dtfft/86400;                  %disp('Set of mjd1'); piahead.mjdtime

                [ k1,k2,fr1,fr2,kss1,kss2 ] = get_sft_sps_and_f_inds( freq_band,piahead,dsfact );

                if dsfact >1                                                %%%%%% frequency bins with respect to the integer start of the band%
                    
                    f0int=floor(fr1);
                    k1int=(fr1-f0int)/dfr+1;
                    k2int=round((fr2-f0int)/dfr)+1; %%%%%% changed this to "round" 24 May
                end
                [ dtori,lfftori ] = get_downsampled_times_samps( piahead,dsfact,sft );
            end

            sft=sft(k1:k2);
            sps=sps(kss1:kss2);
            
% ---------substitute white noise in sft and sps if ichwite==1 ------------

            if icwhite == 1 %
                ampnoise=7.94E-04;                                          % level of noise
                sft=sft_sub_whitenoise(sft,ampnoise,piahead);
                sps=(median(abs(sft))+sps.*0.)*piahead.normd*piahead.normw*sqrt(2);
                sps=sps*sqrt(sqrt(2)); %NORMALIZATION needed ! JAN 2017.
            end

            mjd=gps2mjd(piahead.gps_sec);% piahead.mjdtime;

            if chzero > 0                                                   % check to ensure that sft is not full of zeros
                for ss=1:length(simsour)                                    % for each injection in a particular f band you want to do
                    sour=simsour(ss);                                       % injection is done per FFT
                    if isstruct(simsour(ss))
                        Dt0=(mjd-t0)*86400;                                 % disp(mjd-t0)% time of FFT-refernece time*86400 seconds
                        tt=Dt0+dtori*(0:lfftori-1)+tdt2tdb(mjd);    % time conversion to SS barycenter
                        [ Hp,Hc ] = calc_Hp_Hc( sour );
                        pos = get_changing_source_det_position( sour,mjd,piahead,dtori,lfftori ); % \vec{v}\cdot\hat{n} varies with t b/c det moves
                                                                                                  % w.r.t the source
                        [~, ~, ~, ~, sid1, sid2]=check_ps_lf(sour,antenna,nsid,0);
                        st=gmst(mjd)+dtori*(86400/SD)*(0:lfftori-1)/3600;   % daily effect of earth's rotation
                        i1=mod(round(st*(nsid-1)/24),nsid-1)+1;
                        switch sig_type
                            case 'power_law'
                                [simamps, fsss]=pl_gen_fs_amps(tt,sour);
                            case 'mini_emri'
                                [simamps, fsss] = mini_emri_fevol(sour,freq_band(2),tt,full_mini_ts,full_mini_fs);
                            case 'PN'
                                [simamps, fsss,fcompare]=cbc_simulate_tf_power_law(tt,sour);
                               
                        end

                        if ~strcmp(sig_type,'dark_photon')
                            inject_fs=fsss-fr1;
                            ph1=mod(cumsum(inject_fs).*[0 diff(tt)],1)*2*pi;
    
                            if sour.n==0
                                ph2=fsss.*pos*2*pi;                             % Roemer
                            else
                                ph2=max(inject_fs).*pos*2*pi;
                            end
    
                            ph=mod(ph2+ph1,2*pi);                               % add phase due to second order s.d. and due to Roemer effect
                            e0=exp(1j*ph);
    
                            sim=simamps.*((Hp*sid1(i1)+Hc*sid2(i1)).*e0);
    
                            if aaa==start_aaa && arb==0
                                f_to_look_in_pm=max(inject_fs)+fr1
                            end

                        elseif strcmp(sig_type,'dark_photon')
                            tt=floor(Dt0)+dtori*(0:lfftori-1);
                            sim=dp_strain_complex_tvectorized(tt,sour,sour.epsilon*1e20,det );
                        end
                        sim=fft(sim)*NORM;


                        if dsfact >1                                        % just a check on size of sim, doesn't change anything
                            sim0=zeros(1,piahead.nsamples);
                            sim0(k1:k2)=sim(k1int:k2int);
                            sim2=sim0(k1:k2);
                            if length(sim) ~= length(sim2)
                                disp('====>>>>>ATTENTION ! Not going to work. EXIT')
                                return
                            end
                        else
                            sim2=sim(k1:k2);                                % era sim, PIA. OCT 2016
                        end
%                         figure; semilogy(fr1:dfr:fr2,abs(sim2)); xlabel('frequency (Hz)'); grid;
%                         
                        
                        if piahead.n_zeroes==0 || icwhite == 1
                            if max(inject_fs)+fr1<freq_band(2)
%                         if min(inject_fs)+fr1>freq_band(1) && piahead.n_zeroes==0
                                sft=sft+sim2.';
                            end
                        end

                        if sum(isnan(fsss))~=0 && strcmp(sig_type,'mini_emri')
                            sft = sft.*0.;
                        end

                    else %else no injection
                    end
                end                                                         % here loop over number of signals ends

                hole=((mjd-mjd1)*86400-dtfft);
                mjd1=mjd;
                ihole=round(hole);
                if abs(hole-ihole) > 1.e-6
                    fprintf(' *** ATTENTION ! gap of %f \n',hole-ihole);
                end
            end

            [peaks_signal_thresh, numpeaks_thresh]=create_peakmaps_andrew(sps,sft,thr,piahead,freq_band(1),peak_flag,zero_flag);    %1: threshold is 2.5

            all_peaks_thresh=[all_peaks_thresh peaks_signal_thresh]; 
            all_numpeaks_thresh=[all_numpeaks_thresh numpeaks_thresh];
            all_times_thresh=[all_times_thresh gps2mjd(piahead.gps_sec)];


            vx=piahead.vx_eq; vy=piahead.vy_eq; vz=piahead.vz_eq;
            px=piahead.px_eq; py=piahead.py_eq; pz=piahead.pz_eq;

            vsss=[vx; vy; vz]; all_vsss=[all_vsss vsss];
            psss=[px; py; pz]; all_psss=[all_psss psss];

        end     
        arb=arb+1;
        if arb>num_tot_ffts+fact
            disp('here')
            break;
        end

        [master_piahead,~,master_sps,master_sft]=pia_read_block_09(fidpia);
        disp(['finished master fft number ', num2str(arb), '/', num2str(num_tot_ffts)])
    end
    
    fclose(fidpia);

    disp(['finished with file ',num2str(i)])
    if arb>=num_tot_ffts+fact
        break;
    end

    if N_end == 1
        break;
    end

end

all_times_thresh=all_times_thresh+TFFT/86400;%/2; 
%shift all times by half an FFT to match the format of times in the input file %works for 01 data when it's not /2
if isempty(all_peaks_thresh) || isempty(all_numpeaks_thresh)
    disp('no peaks returned in this duration - probably many zeros, aborting');
    job_pack_0.abort = 1;
    return;
else
    job_pack_0=pl_save_peaks_andrew(all_peaks_thresh,all_numpeaks_thresh, all_times_thresh, TFFT,all_psss,all_vsss,freq_band);
    job_pack_0.abort = 0;
% toc;
end
end