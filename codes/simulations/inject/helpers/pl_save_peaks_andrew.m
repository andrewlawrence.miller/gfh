function [ job_pack_0 ] = pl_save_peaks_andrew(peakss,num_peaks, times, tfft,psss,vsss,freq_band)
%UNTITLED5 Once peaks/peakmaps are generated with simulated signal(s),
%replace peaks in input files with peaks containing signals. The code will
%be able to tell the bandwidth of the input file (?). Load input file,
%insert new peaks, and save new input file
%   Detailed explanation goes here

job_pack_0=struct(); %initalize


new_index(1)=1;

sum_peaks=1; %because num_peaks also starts from one in the create_peakmaps code, to be consistent
%j=1;
for i=1:length(num_peaks)

    sum_peaks=sum_peaks+num_peaks(i)-1; %subtract one because num_peaks is 1 greater than the actual number of peaks
                                        %done in such a way that
                                        %all_peaks(1:5,index(i):index(i+1)-1
                                        %gives all the peaks in one
                                        %timestep
    
    new_index(i+1)=sum_peaks;
   
    
end


job_pack_0.basic_info.freq_band=freq_band;
job_pack_0.basic_info.index=new_index;
job_pack_0.basic_info.npeak_fft=num_peaks; %number of peaks per fft
job_pack_0.basic_info.npeaks=new_index(length(num_peaks)+1);%total number of peaks
job_pack_0.basic_info.tfft=tfft;
job_pack_0.basic_info.dfr=1/tfft;
job_pack_0.basic_info.vel=vsss;
job_pack_0.basic_info.pos=psss;
job_pack_0.basic_info.ntim=length(job_pack_0.basic_info.index)-1;  %PIA
job_pack_0.basic_info.tim=times;

job_pack_0.peaks=peakss(1:3,:); %peaks matrix with injection

end