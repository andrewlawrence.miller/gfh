function [ t00,thedir,dirname ] = get_sci_seg_sfdbdir( det,run,where_am_i )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
if strcmp(run,'O3')
    
    if strcmp(where_am_i,'rome')
        romedir='/storage/users/andrew/next/O3_search/';
        if det==0
            sci_segs=load(strcat(romedir,'O3_L1_science_segments_april2019.txt'));
        elseif det==1
            sci_segs=load(strcat(romedir,'O3_H1_science_segments_april2019.txt'));
        elseif det==2
            sci_segs=load(strcat(romedir,'O3_V1_science_segments_april2019.txt'));
        end
    elseif strcmp(where_am_i,'cnaf')
        if det==0
            sci_segs=load('O3_L1_science_segments_april2019.txt');
        elseif det==1
            sci_segs=load('O3_H1_science_segments_april2019.txt');
        elseif det==2
            sci_segs=load('O3_V1_science_segments_april2019.txt');
        end
    elseif strcmp(where_am_i,'laptop')
        basedir='~/Documents/phd/O3_search/';
        if det==0
            sci_segs=load(strcat(basedir,'O3_L1_science_segments_april2019.txt'));
        elseif det==1
            sci_segs=load(strcat(basedir,'O3_H1_science_segments_april2019.txt'));
        elseif det==2
            sci_segs=load(strcat(basedir,'O3_V1_science_segments_april2019.txt'));
        end
    end
    
    count=1;
    t00=sci_segs(count,1);
    t00_end=sci_segs(count,2);
    if strcmp(where_am_i,'laptop') || strcmp(where_am_i,'cnaf')
        t00=1238777856+12000;
    end
    
    if strcmp(where_am_i,'rome')
        t00=1.238163457000000e+09;
    end
    count=max(find(sci_segs(:,1)<=t00));
    
    if det==0
        disp('livingston')
        dirname='L/';
        if strcmp(where_am_i,'cnaf')
            if t00<1.187732577000000e+09 %last time sfdb O2
                thedir='/storage/gpfs_virgo4/virgo4/virgo/RomePSS/L/sfdb/O2/C02_clean_noscience/2048/';
            else
                thedir='/storage/gpfs_virgo4/virgo4/virgo/RomePSS/L/sfdb/O3/C00/2048/';
            end
        elseif strcmp(where_am_i,'rome')
            if t00<1.187732577000000e+09
                thedir='/storage/users/andrew/next/sfdbs/L/';
            else
                thedir='/storage/users/andrew/next/O3_search/sfdb/';
            end
        end
        
    elseif det==1
        disp('hanford')
        dirname='H/';
        if strcmp(where_am_i,'cnaf')
            if t00<1.187732577000000e+09 %last time sfdb O2
                thedir='/storage/gpfs_virgo4/virgo4/virgo/RomePSS/H/sfdb/O2/C02_clean_noscience/2048/';
            else
                thedir='/storage/gpfs_virgo4/virgo4/virgo/RomePSS/H/sfdb/O3/C00/2048/';
            end
        elseif strcmp(where_am_i,'rome')
            thedir='/storage/users/andrew/next/sfdbs/H/';
        end
    end
    
elseif strcmp(run,'O2')
    if strcmp(where_am_i,'rome')
        romedir='/storage/users/andrew/RIGHT_SVN/Pss_Fr/trunk/src/pss/pss_sfdb/';
        if det==0
            sci_segs=load(strcat(romedir,'O2_L1_segments_CO2_4format.txt'));
        elseif det==1
            sci_segs=load(strcat(romedir,'O2_H1_segments_CO2_4format.txt'));
        elseif det==2
            sci_segs=load(strcat(romedir,'O2_V1_segments_science_v2_27jul2017_25aug2017_4c.txt'));
        end
    elseif strcmp(where_am_i,'cnaf')
        if det==0
            sci_segs=load('O2_L1_segments_CO2_4format.txt');
        elseif det==1
            sci_segs=load('O2_H1_segments_CO2_4format.txt');
        elseif det==2
            sci_segs=load('O2_V1_segments_science_v2_27jul2017_25aug2017_4c.txt');
        end
    end
    count=1;
    t00=sci_segs(count,2);
    t00_end=sci_segs(count,3);
    
    if strcmp(where_am_i,'cnaf')
        %%t00=1187008882; %%GW170817
        t00=1.167534141000000e+09+2000;
    end
    
    count=max(find(sci_segs(:,2)<=t00));
    
    if strcmp(where_am_i,'rome')
        if det==0
            t00=1.167536690000000e+09; %first sfdb livingston
            count=1;
        else
            t00=1.167534141000000e+09; %first sfdb hanford
            count=max(find(sci_segs(:,2)<=t00));
        end
    end
    
    if det==0
        disp('livingston')
        dirname='L/';
        if strcmp(where_am_i,'cnaf')
            thedir='/storage/gpfs_virgo4/virgo4/virgo/RomePSS/L/sfdb/O2/C02_clean_noscience/2048/';
        elseif strcmp(where_am_i,'rome')
            thedir='/storage/users/andrew/next/sfdbs/L/';
        end
        
    elseif det==1
        disp('hanford')
        dirname='H/';
        if strcmp(where_am_i,'cnaf')
            thedir='/storage/gpfs_virgo4/virgo4/virgo/RomePSS/H/sfdb/O2/C02_clean_noscience/2048/';
        elseif strcmp(where_am_i,'rome')
            thedir='/storage/users/andrew/next/sfdbs/H/';
        end
    end
    
end
if ~exist('thedir','var')
    thedir=[];
end

end

