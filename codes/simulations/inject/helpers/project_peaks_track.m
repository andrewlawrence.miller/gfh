function [fbins,counts  ] = project_peaks_track( peaks,plot_flag )
%project_peaks 
% This code projects a peakmap onto the frequency axis.

%   -----------------------------INPUTS:-----------------------------------
%   peaks:           peakmap array; peaks(2,:): frequencies                              
%   plot_flag:       1: plot projected peakmap     

%   -----------------------------OUTPUTS:----------------------------------


%   fbins:          the frequency bins in the peakmap
%   counts:         the number of peaks per frequency bin in the projected peakmap

FF_b4=unique(peaks(2,:));                                                   % get the frequency bins
[counts,fbins]=hist(peaks(2,:),FF_b4);                                      % histogram the frequencies into the unique frequeny bins
if plot_flag==1
    figure;plot(fbins,counts,'k*');
    grid on;
    set(gca,'FontSize',14)
    xlabel('frequency (Hz)');ylabel('count')
end

end

