function [ sours ] = gen_N_power_law_sigs(N,min_f0,max_f0,minfdot0,maxfdot0,min_n,max_n,amp,t0)
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here
int=(max_f0-min_f0)/N; %for N=10, and deltaf=10, an injection every 1 Hz.. when TFFT=2 s, 2 bins away in frequency
for kk=1:N  
    if min_n==5 || min_n == 3 || min_n == 7
        sours(kk)=gen_rand_pow_law_sig(max_f0-int*(kk-1),max_f0-int*(kk-1),minfdot0,maxfdot0,min_n,max_n,amp,amp,t0);
    else
        if N>1
            if kk == 1
                sours(kk)=gen_rand_pow_law_sig(min_f0+1/N,min_f0+1/N,minfdot0,maxfdot0,min_n,max_n,amp,amp,t0);                % ensures first inj not at edge of band
            else
                sours(kk)=gen_rand_pow_law_sig(min_f0+int*(kk-1),min_f0+int*(kk-1),minfdot0,maxfdot0,min_n,max_n,amp,amp,t0);
            end
        else
            sours(kk)=gen_rand_pow_law_sig(min_f0,min_f0,minfdot0,maxfdot0,min_n,max_n,amp,amp,t0);                % ensures first inj not at edge of band
        end
    end
end

end

