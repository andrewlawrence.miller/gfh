function [ k1,k2,fr1,fr2,kss1,kss2 ] = get_sft_sps_and_f_inds( freq,piahead,dsfact )
%UNTITLED15 Summary of this function goes here
%   Detailed explanation goes here'=

nsamp=piahead.nsamples;
dfr=piahead.deltanu;
inifr0=piahead.firstfrind*dfr;
finfr0=(nsamp-1)*dfr+inifr0;

k1=floor(freq(1)/dfr+0.0001)+1;
fr1=(k1-1)*dfr;% k1,freq(1),piahead.deltanu,fr1
k2=round(freq(2)/dfr);%+1; %took aout to make size(sft(k1:k2)==8192
if floor((k2-k1)/2)*2 == k2-k1
    k2=k2+1;
end

if dsfact >1
    if k2>nsamp
        k2=nsamp;
        if floor((k2-k1)/2)*2 == k2-k1
            k2=k2-1;
        end
    end
end

%fprintf('k1,k2= %d,%d %d \n',k1,k2,k2-k1)
fr2=(k2-1)*dfr;  %PIA add -1. come e' da Cristiano.Altrimenti muore nel sottocampionamento va fuori di 1

frss1=max(fr1,inifr0);
kss1=round(frss1/(dfr*piahead.red));%added +1 to align with sft, length(sps(kss1:kss2))=64
frss2=min(fr2,finfr0);
kss2=round(frss2/(dfr*piahead.red));

var1=kss2-kss1+1;
var2=k2-k1+1;
num=var2/var1;

if var1~=var2 && floor(num)~=num
    disp('but whyyyyyyyy?')
    kss1=kss1+1;
    var1=kss2-kss1+1;
    num=var2/var1;
    if floor(num)~=num
        disp('damn')
        kss1=kss1+1;
    end
end

end

