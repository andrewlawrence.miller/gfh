function [ pos ] = get_changing_source_det_position( simsour,mjd,piahead,dtori,lfftori )
%UNTITLED20 Summary of this function goes here
%   Detailed explanation goes here
teph=mjd+piahead.tbase/2/86400; % time at center+1 sample
sour1=new_posfr(simsour,teph);
r=astro2rect([sour1.a sour1.d],0);
%                    f0=sour1.f0;
%                    Dt0=(mjd-t0)*86400; % disp(mjd-t0)
v(1)=piahead.vx_eq; v(2)=piahead.vy_eq; v(3)=piahead.vz_eq;
p(1)=piahead.px_eq; p(2)=piahead.py_eq; p(3)=piahead.pz_eq;

pos1=p(1)+v(1)*dtori*(-lfftori/2:lfftori/2-1);
pos2=p(2)+v(2)*dtori*(-lfftori/2:lfftori/2-1);
pos3=p(3)+v(3)*dtori*(-lfftori/2:lfftori/2-1);
pos=pos1*r(1)+pos2*r(2)+pos3*r(3);

end

