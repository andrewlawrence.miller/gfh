function [ sft ] = sft_sub_whitenoise( sft,ampnoise,piahead )
%UNTITLED21 Summary of this function goes here
%   Detailed explanation goes here
% 
% ampnoise=7.94E-04; %;level of noise
nn=length(sft);
sft=(randn(1,nn)+1j*randn(1,nn))*(1/piahead.normd)*(1/piahead.normw)*ampnoise*(1/1.4142);
sft=sft';
end

