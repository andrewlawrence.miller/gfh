function [new_sft,new_piaheadsss,new_sps ] = change_fft_length(sft,piahead,new_tfft,overlap,win )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if ~exist('win','var')
    win=3; %2: hanning window; 3: flatcosine window
end 

if ~exist('overlap','var')
    overlap=2; %2: overlapping ffts; 1: no overlapping
end


if length(find(sft==0))>0
    disp('')
end

fmax=1/piahead.tsamplu/2;
old_tfft=piahead.tbase;

mjd0=piahead.mjdtime;
gps0=piahead.gps_sec;
%fmax=2048;
dividef=64;

Nold=piahead.nsamples*2;

%appending=sft.*0;
%appending=fliplr(sft);
%old_sft=[sft;appending]; %bigger coln vector

opposite=flipud(sft);

appending2=[0; conj(opposite(1:end-1))];

old_sft=[sft; appending2];

strains=ifft(old_sft);%*sqrt(Nold); %%% times sqrt(2) for normalization that you get from going back to time domain that now has complex data

if ~isreal(strains)
    if ismatlab == 0
        disp('in octave')
    else
        disp('imaginary')
        strains=strains*sqrt(2);
    end
end

% put zeros after
nsamps=length(strains); %0: N , then first one has to be 0; 2N goes with the second complex conj, ifft --> real data, no normalization

num1=nsamps/4;
num2=3*num1;

strains_to_fft=strains(num1+1:num2);
n1=length(strains_to_fft);



nfft_new=2*(new_tfft/old_tfft)*n1; %n1=old_TFFT/dt; n2= new_tfft/dt --> n1/n2 = old_tfft/new_tfft -->n1 = (old/new)*n2 , n2= (new/old)*n1 (new<old)
nover=nfft_new/2;
% if flag==2
%     dtnew=piahead.tsamplu*(2*fmax/(freq_band(2)-freq_band(1))); %so dtnew = 1/(freq_band(2)-freq_band(1))
% else
dtnew=piahead.tsamplu;
% end
normdnew=sqrt(dtnew/nfft_new); %only for after

if dtnew~=piahead.tsamplu
    piahead.tsamplu=dtnew;
end
piahead.normd=normdnew;
piahead.tbase=new_tfft;
piahead.deltanu=1/new_tfft;

%new_sps=reestimate_pow_spec(strains_to_fft,dtnew,nfft_new);


%i=0;
w=pswindow(win,nfft_new);
% new_sft=[];

%tsample=piahead.tsamplu;
% tt=0:tsample:old_tfft/2-piahead.tsamplu;
% phase=2*pi*10*tt';
% strains_to_fft=exp(1j*phase);%+2*(randn(size(tt))+1i*randn(size(tt)));
% 
% figure;plot(tt,real(strains_to_fft));
% 
% testing2=fft(real(strains_to_fft));
% testing=testing2(1:end/2);
% df=1/(old_tfft/2);
% xxxx2=0:df:2*fmax-df;
% xxxx=0:df:fmax-df;
% figure; semilogy(xxxx,abs(testing))



if overlap==1
    new_mjdtime=mjd0;
    for jj=1:n1/nfft_new
        if jj==1
            x=strains_to_fft(1:nfft_new);
        else
            x=strains_to_fft((jj-1)*nfft_new+1:nfft_new*jj);
        end
%         figure;plot(tt(1:nfft_new),real(x))
         
        x=x.*w.'; 
        xx=fft(real(x));%/sqrt(nfft_new);
%         figure;plot(tt(1:nfft_new),real(x))
        
% %         dfnow=1/new_tfft;
% %         fss=0:dfnow:2*fmax-dfnow;
% %         figure; semilogy(fss,abs(xx))
% %         
% %         figure;semilogy(fss(1:end/2),abs(xx(end/2+1:end)))
%         

        new_sft{jj}=xx; %%NOT OVERLAPPING here- don't do this> %xx(end/2+1:end); %b/c ffting real(x), the PS is symmetric, only need half of it
        
        new_mjdtime=new_mjdtime+(1/2)*new_tfft/86400;
        new_num_samples=length(new_sft{jj});
        
        
        piahead.mjdtime=new_mjdtime;
        piahead.nsamples=new_num_samples;
%         piahead.tbase=new_tfft;
%         piahead.deltanu=1/new_tfft;
%         piahead.normd=normdnew;
        new_piaheadsss{jj}=piahead;
        
        new_mjdtime=new_mjdtime+(1/2)*new_tfft/86400;
        
        %new_sft=[new_sft; x];
    end
end


%only takes half of ffted x each time, b/c of overlapp

reconstruction=[];

if overlap==2
    ind=0; %tells you number of overlaps
    new_mjdtime=mjd0;
    new_gps_sec=gps0;
    tot_ffts=ceil(2*n1/nfft_new);

    if rem(2*n1,nfft_new)==0
        cond2 = tot_ffts + 10000; %%%% makes this condition never met
    else
        cond2 = tot_ffts - 2; %%% accounts for last part of master FFT
    end                       %%% when n1/nfft is not integer:
                              %%% tot_ffts - 2: second-to-last FFT - it
                              %%% almost equals nfft (e.g. = 32000,
                              %%% nfft=36000), but not quite; but need
                              %%% another FFT that is halfway between end
                              %%% and starting of 32000 samples, so that
                              %%% one time is not missed
                              %%% this additional FFT (ceil() ) corresponds
                              %%% to the non-integer part of 2 * n1/nfft_new
    for jj=1:tot_ffts %equals n1/nover
        if jj==tot_ffts
            disp('')
        end
        if ind==0
            x=strains_to_fft(1:nfft_new);
        elseif ind==tot_ffts-1 || ind == cond2
            x=strains_to_fft(ind*nover+1:end); 
            if rem(2*n1,nfft_new)==0
                x=[x ; zeros(nfft_new/2,1)]; %adding 0s in time
            else
                x=[x; zeros(nfft_new-length(x),1)];
            end
        else
            x=strains_to_fft(ind*nover+1:nfft_new+nover*ind);  

        end
        dfnow=1/new_tfft;
        fss=0:dfnow:2*fmax-dfnow;
        needed_fs=fss(1:end/2);
        
        x=x.*w.'; 
       % xm=mean(x);
       % x=x-xm;
        xx=fft(x);%/sqrt(nfft_new);
%	if ind~=tot_ffts-1
        full_sft=xx(1:end/2);
%        else
%	    full_sft=xx;
%	end
        full_sps=USE_reestimate_pow_spec(full_sft,piahead.deltanu,piahead.normd,piahead.normw);
        
        
%         df=1/new_tfft;
%         num=length(0:df:dividef-df);
%         
%         test=reshape(abs(full_sft),num,[]);
%         tee=median(test)*piahead.normd*piahead.normw*sqrt(2);
%         full_sps=repelem(tee,num);

        
       % medd=median(abs(full_sft))*piahead.normd*piahead.normw*sqrt(2);
       % full_sps=full_sps.*0+medd;
        %crss=abs(full_sft).^2./full_sps;
        %figure;semilogy(crss)
      
        
        %sps=reestimate_pow_spec(full_sft,piahead.deltanu,piahead.normd,piahead.normw);
        %full_sps=sps(1:end/2);
        
       
%         if ~isreal(strains)
%             %disp('imag, taking second half')
%             full_sft=xx(end/2+1:end);
%         else
%             disp('real, taking first half')
%             full_sft=xx(1:end/2);
%         end
       
        dfnow=1/new_tfft;
        fss=0:dfnow:fmax-dfnow;
       % figure; semilogy(fss,abs(xx(1:end/2)))
        
        test3=ifft(xx);
        numm=length(xx)/4;
        numm2=3*numm;
        reconstruction=[reconstruction; test3(numm+1:numm2)];

        
        %halved_sft=full_sft(num11+1:num22);
        
        new_sft{jj}=full_sft;
        new_sps{jj}=full_sps;
        
        new_num_samples=length(full_sft);

        new_mjdtime=mjd0+(1/2)*ind*new_tfft/86400;
        new_gps_sec=gps0+(1/2)*ind*new_tfft;
        
        
        piahead.mjdtime=new_mjdtime;
        piahead.nsamples=new_num_samples;
        piahead.red=1;
        piahead.gps_sec=new_gps_sec;
       % piahead.n_zeroes=length(find(full_sft==0));
       % piahead.normd=normdnew;
        
        new_piaheadsss{jj}=piahead;
        ind=ind+1;
        %save('new_sft.mat','full_sft','piahead')

    end
    
%     if length(new_sft)*nfft_new/2~=length(sft)
%        % disp('ugh')
%         append1=zeros(nfft_new,1);
%         new_sft{jj+1}=append1;
%         piahead.mjdtime=mjd0+(1/2)*ind*new_tfft/86400;
%         piahead.nsamples=length(append1);
%         piahead.deltanu=1/new_tfft;
%         new_piaheadsss{jj+1}=piahead;
%         
%         append2=ifft(append1);
%         reconstruction=[reconstruction; append2(numm+1:numm2)];
%         
%        
%     end
    
end
% figure;plot(tt,reconstruction/piahead.normw) %the windowing introduces a factor of ~1.2 that needs to be divided out
% hold on;
% plot(tt,real(strains_to_fft))

% while i <= n1-nfft_new %n must be greater than nfft %nfft defines the time
%     x=strains_orig(i+1:i+nfft_new); %intial: i=0, so x=y(1:8192+2); y are the amplitudes of each sample over time
%     x=x.*w.'; %multiply by window weights
%     
%     %xm=mean(x); %just the mean because ir=1
%     %x=x-xm;
%     %    x(nt+1:nt1)=0; %seems to do nothing
%     
%     x=fft(x);
%     new_sft=[new_sft; x];
%     
%     %tfps(j,:)=abs(x(1:nf)).^2*dt/nt; %nf is 8192+2; dt/nt = dt/nover; dt is orig. sampling time; each row is the fft(x) at one time
%     
%     if overlap==2
%         i=i+nover; %nover small -> takes longer time for loop to finish
%     else
%         i=i+nfft_new;
%     end
% end


   



end

