function [ Hp,Hc ] = calc_Hp_Hc( simsour )
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here
     %   disp('real noise')
eta=simsour.eta;
psi=simsour.psi*pi/180;
Hp=sqrt(1/(1+eta^2))*(cos(2*psi)-1j*eta*sin(2*psi));
Hc=sqrt(1/(1+eta^2))*(sin(2*psi)+1j*eta*cos(2*psi));


end



