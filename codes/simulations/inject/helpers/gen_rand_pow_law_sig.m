function [ source ] = gen_rand_pow_law_sig...
    ( minf,maxf,minfdot,maxfdot,min_n,max_n,minamp,maxamp,mjdtime,mincosiota,maxcosiota,minpsi,maxpsi,min_del_n,max_del_n,minlon,maxlon,minlat,maxlat,parm)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

%rr=O1LH;
if ~exist('parm','var')
    minff=800;
    maxff=1000;
    minsd=-1e-3;
    maxsd=-1e-1;
%     mindd_f=-10^-15;
%     maxdd_f=-10^-14;
    %minalphasat=10^-2;
    %maxalphasat=10^-1;
    %mind=0.001;%0.117; %Mpc
    %maxd=0.5;%1.170;%1.755; %Mpc
    %minmjd=rr.ini;
    %maxmjd=rr.fin; 
    minlon=0;
    maxlon=360;
    minlat=-90;
    maxlat=90;
    minccosiota=-1;
    maxccosiota=1;
    minppsi=-90;
    maxppsi=90;
    min_amp=2.5e-23;%1e-25;
    max_amp=2.5e-23;%1e-24,1e-23;
    n_min=2;
    n_max=7;
    mindeln=0;
    maxdeln=0;
end


if ~exist('minf','var')
    
    minf=minff;
    maxf=maxff;
    
end

if ~exist('minfdot','var')
    
    minfdot=minsd;
    maxfdot=maxsd;
    
end


if ~exist('min_n','var')
    
    min_n=n_min;
    max_n=n_max;
    
end

if ~exist('minamp','var')
    minamp=min_amp;
    maxamp=max_amp;
end

if ~exist('mincosiota','var')
    mincosiota=minccosiota;
    maxcosiota=maxccosiota;
end

if ~exist('minpsi','var')
    minpsi=minppsi;
    maxpsi=maxppsi;
end

if ~exist('min_del_n','var')
    min_del_n=mindeln;
    max_del_n=maxdeln;
end

if ~exist('mjdtime','var')
    mjdtime=5.798261905092592e+04;%5.798252201388889e+04;%alpha=0; (for the time before the BNS in O2); 
end


f0=pick_one_uni_dist(minf,maxf); %for r-modes, initial rotation frequency
d_freq=pick_one_uni_dist(minfdot,maxfdot);
% dd_freq=pick_one_uni_dist(mindd_f,maxdd_f);
%mjdtime=pick_one_uni_dist(minmjd,maxmjd);
amp=pick_one_uni_dist(minamp,maxamp);
alpha=pick_one_uni_dist(minlon,maxlon);
delta=pick_one_uni_dist(minlat,maxlat); 
cosiota=pick_one_uni_dist(mincosiota,maxcosiota);
psi=pick_one_uni_dist(minpsi,maxpsi);
n=pick_one_uni_dist(min_n,max_n);
del_n=pick_one_uni_dist(min_del_n,max_del_n);

intt=randi([0,1],1);
if intt==1
    del_n=-del_n;
end

eta=-2*cosiota/(1+cosiota^2);

%%FIX PARAMS

dd_freq=0; %these three don't matter
%mjdtime=5.728359684027778e+04;%minmjd (for O1); %5.728360836805555e+04;%minmjd;%5.571543501157407e+04;
% mjdtime=5.798261905092592e+04;%5.798252201388889e+04;%alpha=0; (for the time before the BNS in O2); 
%%%NEED to change when dealing with TFFT>2 b/c start time of analysis will be different
% alpha=13.2231;
% delta=-23.0756;
%delta=90;
%f0=940;
%sat_amp=0.05;
%d=0.01; %mpc


[ecl_lon ecl_lat]=astro_coord('equ','ecl',alpha,delta);

location_ecl=[ecl_lon ecl_lat];

%kn=-((2*pi)^6*2*(7-1)*Q/(tau*(pi*G*rho)^3))*sat_amp^2; %const in df/dt = kn*f power law equation; (n-1)=6 divided out, so is really the const kn
%7 is breaking index

kn=abs(d_freq/f0^n);
if n~=1
    x0=1/f0^(n-1);
else
    x0=log(f0);
end
tau=1/((n-1)*kn*f0^(n-1));


source.a=alpha;
source.d=delta;
source.v_a=0;
source.v_d=0;
source.pepoch=mjdtime; %not really relevant, only relevant in the case where the position of the source is changing with time (has some velocity)
source.f0=f0;
if n == 5 || n == 3 || n == 7 || n == 0
    source.df0=-abs(d_freq);
else
    source.df0=abs(d_freq);
    if n == 11/3
        source.mc = calc_mc_with_k(kn);
    end
end
source.ddf0=dd_freq;
source.fepoch=mjdtime;
source.ecl=location_ecl;
source.t00=mjdtime;
source.eps=1;
source.eta=eta;
source.psi=psi;
source.cosiota=cosiota;
source.h0=amp;
source.h=amp;
source.snr=1;
source.coord=0;
source.chphase=0;
source.dfrsim=-0.200000000000000;
source.x0=x0;
source.kn=kn;
source.tau=tau;
source.n=n;
source.del_n=del_n;
end

