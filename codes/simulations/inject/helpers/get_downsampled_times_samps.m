function [ dtnew,lfftnew ] = get_downsampled_times_samps( piahead,dsfact,sft )
%UNTITLED17 Summary of this function goes here
%   Detailed explanation goes here
dtori=piahead.tsamplu;

lfftori=piahead.nsamples*2; %times 2 b/c extends to neg frequencies

if dsfact >1
    dtnew=dtori*dsfact;
    lfftnew=lfftori/dsfact;

    if floor(lfftnew)~=lfftnew
       lala=length(dtnew*(0:lfftnew-1));
       if lala<length(sft)
           lfftnew=ceil(lfftnew);
       elseif lala>length(sft)
           lfftnew=floor(lfftnew);
       end
    end
    %display('Downsampled lfft and sampling time')
    %lfftori
    %dtori
else
    lfftnew=lfftori;
    dtnew=dtori;
end

end

