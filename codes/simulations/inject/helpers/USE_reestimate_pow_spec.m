function [ xamed ] = USE_reestimate_pow_spec(new_sft,df,normd,normw,tau,maxage,twoway_flag)
% 
% This code estimates a short power spectrum of a particular FFT

%   -----------------------------INPUTS:-----------------------------------
%   new_sft:        the fft whose spectral density you want                       
%   df:             frequency resolution (1/tfft)               (Hz)
%   normd:          normalization for sampling
%   normw:          normalizaiton for window
%   tau:            (number of bins to 'remember')
%   maxage:         maximum number of bins to remember
%   twowayflag:     1: do the AR spectrum in both directions; else high->lo

%   -----------------------------OUTPUTS:----------------------------------


%   xamed: estimation of the amplitude spectral density


maxdin=2.5;%sqrt(2.5);
% III=ones(1,8192);
if ~exist('tau','var')
    %tau=0.02*df/0.001; %Hz %frequency range that you are considering in memory
    tau=1;
end

if ~exist('maxage','var')
    maxage=10;
end
if ~exist('twowayflag','var')
    twoway_flag=1; %1: do the AR spectrum in both directions; 0: do the AR spectrum only from high to low
end


x=abs(new_sft).^2;

N=length(x);

w=exp(-df/tau);
age=0;
k=N;
w_norm=0;
xw=0;
xamed=zeros(1,N);

xamed(k)=x(k);
% ratio=1;
k=k-1;
while k>0
%     if k~=N
%         if k>0 && k<=N-1
    if(xamed(k+1)~=0)
        ratio=x(k)/xamed(k+1);
    else 
        ratio=1.0;
    end
%         else
%             ratio=1.0;
%         end
%     end  
    
    if ratio<=maxdin
        
        age=0;
        ad=x(k);

        xw=ad+w*xw;
        w_norm=1+w*w_norm;
        xamed(k)=xw/w_norm;

    end
    if ratio>maxdin
        if age==0
            kinizio=k;
            ratio_max=ratio;
            %bin_max=0; 
        end
        age=age+1;
        if ratio>=ratio_max
            ratio_max=ratio;
        end
        ad=x(k);
        if ratio>=10*maxdin
            %bin_max=kinizio-k;
            ad=x(kinizio+1);
        end
        xw=ad+w*xw; 
        w_norm=1+w*w_norm;

        xamed(k)=xamed(k+1);

    end
    
    if age<=maxage
        k=k-1;
    end
    if age>maxage
        k=k+age-1;
        if k>N
            k=N;
        end
        if k<=N-1
            xamed(k+1)=x(k);
        end
        age=0;
    end

   
    
end
  xamedH2L=xamed;  
  
 %SECOND PART: estimation from low to high freq
 
 
if twoway_flag==1
    age=0;
    k=1;
    w_norm=0;
    xw=0;
    xamed(k)=x(k);
    ratio=1;
    while k<=N


        if k>1 && k<N
            if(xamed(k-1)~=0)
                ratio=x(k)/xamed(k-1);
            else 
                ratio=1.0;
            end
        else
            ratio=1.0;
        end
        if ratio<=maxdin

            age=0;
            ad=x(k);

            xw=ad+w*xw;
            w_norm=1+w*w_norm;
            xamed(k)=xw/w_norm;

        end
        if ratio>maxdin
            if age==0
                kinizio=k;
                ratio_max=ratio;
                %bin_max=0; 
            end
            age=age+1;
            if ratio>=ratio_max
                ratio_max=ratio;
            end
            ad=x(k);
            if ratio>=10*maxdin
                %bin_max=kinizio-k;
                ad=x(kinizio-1);
            end
            xw=ad+w*xw; 
            w_norm=1+w*w_norm;

            xamed(k)=xamed(k-1);

        end

        if age<=maxage
            k=k+1;
        end
        if age>maxage
            k=k-age+1;
            if k<=1
                k=1;
            end
            if k<=N
                xamed(k-1)=x(k);
            end
            age=0;
        end



    end

end


xamedL2H=xamed;

  
%xamed=(xamedH2L.*xamedL2H)./(xamedL2H+xamedH2L);
xamed=(xamedH2L+xamedL2H)./2; 

xamed=sqrt(xamed)*normd*normw*sqrt(2); %NORMALIZATION
%end

% figure;semilogy(abs(new_sft).^2); hold on; semilogy(abs(new_sft).^2,'.g'); hold on; semilogy(xamed.^2,'.r')
% rats=abs(new_sft).^2./(xamed.^2)'; hold on; semilogy(rats,'.'); semilogy(maxdin*ones(8192,1),'k')
% grid on
% xamedNORM=sqrt(xamed)*normd*normw*sqrt(2); %done so matches normalizations and
% fmax=(length(full_sft)-1)*df;
% xstep=0:df:fmax;
% figure
% semilogy(xstep,abs(new_sft).^2,'k')
% hold on
% semilogy(xstep,xamed.^2,'r')
% semilogy(xstep,xamed.^2,'.r')
% xlabel('Frequency Hz')
% ylabel('Squared FFt and xamed')
% grid


%format in sfdb files


end
           
