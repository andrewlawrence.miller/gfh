function [ dsfact,NORM ] = calc_dsfact( dt,freq )
%UNTITLED23 Summary of this function goes here
%   Detailed explanation goes here
f_band_you_want=freq(2)-freq(1)
dt_prime=1/f_band_you_want;
dsfact=dt_prime/dt;

if dsfact >1 %normalization for FFT beacuse of downsampling
    NORM=sqrt(dsfact*dsfact)/2;
    % is for the 2 complex versus
    % real. PIA 12 ottobre 2016
else
    NORM=1/2;
end

end

