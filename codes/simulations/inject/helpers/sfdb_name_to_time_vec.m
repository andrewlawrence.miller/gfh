function [ y m d h min s ] = sfdb_name_to_time_vec( sfdb_name )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
oo=strsplit(sfdb_name,'_');
numoo=length(oo);
ymd=oo{numoo-1};
try
    y=ymd(1:4);
    flag=0;
catch
    ymd=oo{numoo};
    y=ymd(1:4);
    flag=1;
end
m=ymd(5:6);
d=ymd(7:8);

hms=oo{numoo};
% if flag==0
%     hms=oo{5};
% elseif flag==1
%     hms=oo{6};
% end
h=hms(1:2);
min=hms(3:4);
s=hms(5:6);

y=str2num(y);
m=str2num(m);
d=str2num(d);

h=str2num(h);
min=str2num(min);
s=str2num(s);

end

