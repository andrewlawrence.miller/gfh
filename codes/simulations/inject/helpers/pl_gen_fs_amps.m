function [ amps, fsss ] = pl_gen_fs_amps(times, source_spec_parms )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here


f0=source_spec_parms.f0;
braking_index=source_spec_parms.n;
bi_var_fact=0;
if bi_var_fact==0
    pow=braking_index-1;
else
    bi_up=braking_index+bi_var_fact*times;
    pow=bi_up-1;
end
switch braking_index
%     case 0 %CW
%         fsss=f0;
%         amps=source_spec_parms.h0*1e20;
%     case 1 %exp decay, pulsar winds
%         kn=source_spec_parms.kn;
%         fsss=f0.*exp(-kn*times);
%         amps=source_spec_parms.h0*1e20;
%     case 2
%     case 3 %magnetar
%         Bfield=source_spec_parms.Bfield;
%         fsss=magnetar_gen_fs(f0,Bfield,times);
%         amps=source_spec_parms.h0*1e20;
%     case 4
%     case 5 %%GW dominated emission from magnetar
%         ellip=source_spec_parms.ellip;
%         fsss= GW_magnetar_gen_fs(f0,ellip,times);
%         amps=source_spec_parms.h0*1e20;
%         
%         
%     case 6
%     case 7 %rmode
%         satamp=source_spec_parms.satamp;
%         d=source_spec_parms.dist;
%         [amps,fsss]= rmode_gen_amps( f0,satamp,times, d);
%         %amps=amps*1e3;
    otherwise
        try
            kn=source_spec_parms.kn;
        catch
            kn=source_spec_parms.k;
        end
        const=kn.*pow.*f0.^pow;
        if braking_index==1
            fsss=f0*exp(-kn*times);
        elseif braking_index==11/3
%             disp('chirp,spinup')
            fsss=f0*(1-const.*times).^(-1./pow);
        else
            fsss=f0*(1+const.*times).^(-1./pow);
        end
        %fsss=f0*(1+kn*(breaking_index-1)*f0^(breaking_index-1)*times).^(breaking_index-1);
        h0=source_spec_parms.h0;
        if braking_index<6.8 && braking_index~=11/3
            amps=h0*(fsss.^2)/f0^2; %amplitude is constant that you set times the frequency variation squared as is in general eqn
        elseif braking_index>=6.8 && braking_index<7.2
            amps=h0*(fsss.^3)/f0^3;
        elseif braking_index==11/3
            amps=h0*fsss.^(2/3)/f0^(2/3);
        end
        amps=amps*1e20;
end

end
