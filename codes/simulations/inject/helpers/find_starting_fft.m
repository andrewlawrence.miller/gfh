function [ master_piahead,master_sps,master_sft,ccc,t0 ] = find_starting_fft( master_piahead,t0,TFFT,fidpia,input_sft,input_sps )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

gps_time=master_piahead.gps_sec;
tbasee=master_piahead.tbase;
%this loop loads ffts until you've found an fft that contains the start
%time you want to analyze
ccc=1; %which num fft you're reading
while t0>gps_time+(tbasee/2-TFFT/2) %so that if t0 is greater than the gps time of the file but less than the gps time of the next one, take the current one
    %the TFFT/2 is b/c of counting, was off by one FFT, not so important in grand scheme b/c analyzing >=5000 s of data, so 2/5000 ~ 0.04% missing
    [master_piahead,~,master_sps,master_sft]=pia_read_block_09(fidpia);
    if master_piahead.eof==1
	disp('endddd of filepia')
        break; %if you've reached the end of the file fidpia, break out of while loop
    end
    gps_time=master_piahead.gps_sec;
    ccc=ccc+1;
end
disp('shitt')
master_piahead
if ccc==1 % meaning while loop never entered
    master_sft=input_sft;
    master_sps=input_sps;
end
if master_piahead.eof~=1
    ratio_zeros=length(find(master_sft==0))/length(master_sft);
    chzero=sum(abs(master_sft))
    while chzero==0 %while ratio_zeros>0.8
        disp('too many zeros')
        [master_piahead,~,master_sps,master_sft]=pia_read_block_09(fidpia);
        if master_piahead.eof~=1
            t0=master_piahead.gps_sec;
        else
            t0=t0+tbasee/2;
            disp('end of file')
            break;
        end
        if master_piahead.n_zeroes==0
            disp('not zero')
            sum(master_sft)
        end
        chzero=sum(abs(master_sft));
    end
else
    disp('still end of filepia')
end
end

