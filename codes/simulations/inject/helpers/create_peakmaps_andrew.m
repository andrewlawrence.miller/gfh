function [ peakss,ind] = create_peakmaps_andrew(sps, sft, thresh, header_parms,f_begin, peak_flag,zero_flag )
%UNTITLED4 Given the FFT and AR spectrum from the simulation/sbl code,
%create the peakmap. Involves mod squaring FFT/ AR spec^2. Need to
%normalize the (AR spectrum?) by, sps=root(arspec)
%spectrum^2/(header_parm*normw*normd*bil2uni). Then pick threshold 2.5,
%check if the ratio is above threshold, if it is, check if it's local max
%(bigger than the one before and bigger than the one after). If it is, save
%the time, frequency, ratio in 'peaks' file- peaks(1,:) --> time, save(['in_VSR4_01_',str,'_.mat'],'job_pack_0','inj_parms')
%peaks(2,:) --> freqeuncy, ...
%dive
%   Detailed explanation goes here
%load('testing.mat','sps')
% length_ratio=length(sft)/length(sps);
% 
% 
% new_sps=[];
% 
% for i=1:length(sps)
%     
%     array=ones(1,floor(length_ratio))*sps(i);
%     new_sps=[new_sps array];
%     
% end
new_sps=sps'; 

% try
norm_arspec=new_sps/(header_parms.normd*header_parms.normw*sqrt(2)); %sqrt(2) is bil2uni parameter, need to normalize the arspec by parameters in the header
% catch
%     disp('shiiiiz')
% end
squared_norm=norm_arspec.^2;


fft_squared=abs(sft).^2;%real(ffts).^2+imag(ffts).^2;

ratio=fft_squared./(squared_norm);  %IMP the sqrt(sqrt(2)) for simulated noise

%%See the injection main code when icwhite=1 and sps is created
%AND NORMRATIO is no more needed.
% if ~exist('normRATIO','var') %signal only case. It is evaluated
%     normRATIO=mean(ratio);
% end
%ratio=ratio/normRATIO;  %%NO MORE NEEDED. JAN 2017. per essere intorno ad 1
% if ifig>=1 && ifig <3
%     figure
%     plot(ratio)
%     grid
%     ylabel('ratio')
%      normRATIO;
%      mean(ratio);
%      std(ratio);
%     %hist(ratio,500)
% end
peaks_index=1; %index for peaks

if peak_flag~=1
    peakss=zeros(3,length(ratio)-2);
end

% if peak_flag==1 || zero_flag==0

    for i=2:(length(ratio)-1)%1:length(ratio) 

        if peak_flag==1

            if ratio(i)>thresh %for all ratios calculated, check if it's first bigger than threshold %%% NEED THIS SETUP FOR THE HOUGH
                     if ratio(i)>ratio(i+1)
                         if ratio(i)>ratio(i-1) %check in middle of FFT, if bigger than both the one before and one after

                            peakss(1,peaks_index)=gps2mjd(header_parms.gps_sec);%header_parms.mjdtime;
                            peakss(2,peaks_index)=f_begin+header_parms.deltanu*(i-1); %adding frequency from beginning
                            peakss(3,peaks_index)=ratio(i);
                            %peakss(4,peaks_index)=1;% don't know what to put here for regular noise
                            %peakss(5,peaks_index)=0;

                           % peakss(4,peaks_index)=header_parms.vx_eq;
                           % peakss(5,peaks_index)=header_parms.vy_eq;
                           % peakss(6,peaks_index)=header_parms.vz_eq; %for the doppler

                            peaks_index=peaks_index+1;
                         end
                     end
            end     

        else
            peakss(1,peaks_index)=gps2mjd(header_parms.gps_sec);%   header_parms.mjdtime;
            peakss(2,peaks_index)=f_begin+header_parms.deltanu*(i-1); %adding frequency from beginning

            if zero_flag==0 %put zeros where the CR is not a local max, applied threshold, so that the size of each peakmap is the same

                if ratio(i)>thresh %for all ratios calculated, check if it's first bigger than threshold %%% NEED THIS SETUP FOR THE HOUGH
                    if ratio(i)>ratio(i+1)
                        if ratio(i)>ratio(i-1) %check in middle of FFT, if bigger than both the one before and one after
                            peakss(3,peaks_index)=ratio(i);
                        else
                            peakss(3,peaks_index)=0;
                        end
                    else
                        peakss(3,peaks_index)=0;
                    end
                else
                    peakss(3,peaks_index)=0;
                end

            else
                peakss(3,peaks_index)=ratio(i); %records CR if it's less than thresh, doesn't make it 0
            end
            %peakss(4,peaks_index)=header_parms.vx_eq;
            %peakss(5,peaks_index)=header_parms.vy_eq;
            %peakss(6,peaks_index)=header_parms.vz_eq; %for the doppler

            %peakss(4,peaks_index)=1;% don't know what to put here for regular noise
            %peakss(5,peaks_index)=0;

            peaks_index=peaks_index+1;
        end
            %if none of these are true, i = i+1, the loop advances
    end
% else
%     peakss(1,:)=header_parms.mjdtime*ones(length(ratio),1);
%     peakss(2,:)=f_begin+header_parms.deltanu*(0:length(ratio)-1);%(i-1);
%     peakss(3,:)=ratio;
% end
if ~exist('peakss','var')
  % disp('no peaks found at this time:')
   header_parms.mjdtime;
   peakss=[];
%    det_vel_pos=[];   
end

ind=peaks_index;

end




