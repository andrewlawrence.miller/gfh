function [ ii_start,ii_end ] = find_start_end_ffts( piaheadss,t00,duration,fact )
%UNTITLED14 Summary of this function goes here
%   Detailed explanation goes here
 %   --------for the new collection of FFTs, find the one whose t0 is closest to t00----------------
        
        for jjj=1:length(piaheadss)
            if t00<=piaheadss{jjj}.gps_sec %after the first iteration, this will never be true
                %disp('lala')
                ii_start=jjj; %this loop is so that you find, within TFFT/2 s, the start index of the new fft
                break;
            end
            %for example, t0 could be between 1e9 and 1.1e9, but t0 is
            %actually 1.5e9, and it is wasteful to analyze FFTs that are
            %actually before t0
        end

 %   --------for the new collection of FFTs, find the one whose t0 is closest to end of sig t00+duration----------------

        for lll=1:length(piaheadss)
            if piaheadss{lll}.gps_sec<=t00+duration %if the time is the gps file is less than the expected duration of the source,
                ii_end=lll;
            else
                if fact~=0
                    ii_end=length(piaheadss);
                else
                    ii_end=lll; %if the piaheads have gone beyond t0+duration, record last sft that will be used
                end

                break;

            end
            %for example, t0+duration could be between 1e9 and 1.1e9, but t0 is
            %actually 1.5e9, and it is wasteful to analyze FFTs that are
            %actually after t0+duration, 
        end

end

