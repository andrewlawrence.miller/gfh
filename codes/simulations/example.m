function [p_times,p_freqs,p_eq_ps,xgrid,kgrid,color_grid,found] = example(minf,maxf,mc,amp,which_hough,plot_flag,save_flag,outdir,where_am_i )
%example 
% The code creates a source, simulates it, creates a peakmap, runs the
% Hough, selects candidates, and does coincidences with injection and
% candidates. This is used to create the plots in the paper with minf=140,
% maxf=150, mc=1e-3, amp=1e-22 (plots= peakmap, Hough map)

%   -----------------------------INPUTS:-----------------------------------
%   minf:           minimum frequency                                  (Hz)
%   maxf:           maximum frequency                                  (Hz)
%   mc:             chirp mass                                       (Msun)
%   amp:            amplitude of signal
%   detname:        detector ('Livingston','Hanford','Virgo')
%   which_hough:    do either 'fh' or 'gfh'
%   plot_flag:      do plots (=1), else no
%   save_flag:      save table of parms (=1), else no
%   outdir:         output directory
%   where_am_i:     cluster on which you run ('laptop','louvain','cnaf')

%   -----------------------------OUTPUTS:----------------------------------

%   NONE

%   -----------------------------EXAMPLE:----------------------------------

% example()


if ~exist('minf','var')
    minf=140';
end
if ~exist('maxf','var')
    maxf=150;
end
if ~exist('mc','var')
    mc=1e-3;
end
if ~exist('amp','var')
    amp=1e-22;
end

if ~exist('which_hough','var')
    which_hough='gfh';
end

if ~exist('plot_flag','var')
    plot_flag=1;
end

if ~exist('save_flag','var')
    save_flag=1;
end

if ~exist('outdir','var')
   outdir='~/Desktop/Belgio/dark_matter/pbh_cw/paper/pbh-cw/parameters/';
end

if ~exist('where_am_i','var')
    where_am_i='laptop';
end



n=11/3;                                                                     % braking index for chirp signal, fixed for pbh signals
N=1;                                                                        % number of injections per frequency band to do (set to 1)
if strcmp(where_am_i,'louvain')
    sfdbdir='/nfs/user/gw/H/sfdb/O3/C00/NC/2048/';
    detname='Hanford';
elseif strcmp(where_am_i,'laptop') %% my laptop
    sfdbdir='~/Desktop/sfdbs/H/'; % directory where SFDBs are
    detname='Hanford';
end
t00=1238777856+12000;                                                       % start time of injection
coin_dist=3;                                                                % distance in bins allowed between injection parms and cand parms
rootkcand=5;                                                                % sqrt(num of candidates to select)
ref_perc_time=0.5;                                                          % reference time to use when constructing hough map: f0 is at t0


fdotmin=calc_fdot_chirp(mc,minf);                                           % fdot at minf and maxf
fdotmax=calc_fdot_chirp(mc,maxf);
TFFTmax=floor(1/sqrt(fdotmax));                                             % calculate TFFT using maximum spinup
TFFT=2^floor(log2(TFFTmax))                                                 % will work for TFFT not a factor of 2

t1=calc_time_to_coalescence(mc,minf);                                       % seconds until coalescence at min and max frequency injected
t2=calc_time_to_coalescence(mc,maxf);
dur=floor(t1-t2);                                                           % signal duration to simulate (s)


t0=gps2mjd(t00);                                                            % converts t00 (GPS time) to MJD time
sour=gen_N_power_law_sigs(N,minf,maxf,fdotmin,fdotmin,n,n,amp,t0);          % creates source structure, for chirp sigs, sig's f0 alwyas at minf
sour.dur=dur;

job_pack_0=inject_power_law_signal(sour,[minf maxf],t00,dur,TFFT,sfdbdir);  % does the injection of sour in freq band, makes peakmap with TFFT;
                                                                            % job_pack_0 contains the peakmap
basic_info=job_pack_0.basic_info;                                           % structure that contains basic information about the peakmap    


[SLong, SLat]=astro_coord('equ','ecl',sour.a,sour.d);                       % conversation of equatorial to ecliptic sky coordinates

p=job_pack_0.peaks;                                                         % the peaks in time/frequency, with corresponding amps
p_times=p(1,:);
p_freqs=p(2,:);
p_eq_ps=p(3,:);
if plot_flag==1
    plot_triplets(86400*(p(1,:)-p(1,1)),p(2,:),p(3,:))
    xlabel('time (s)'); ylabel('frequency (Hz)'); cblabel('equalized spectrum')
    set(gca,'FontSize',14)
end
pout=andrew_hfdf_patch(p,basic_info,[SLong SLat]);                          % Doppler correction

hm_job.minf=minf;                                                           % minimum frequency to do the Hough on
hm_job.maxf=maxf;                                                           % maximum frequency to do the Hough on
hm_job.df=1/TFFT;                                                           % step in frequency
hm_job.dur=dur;
hm_job.patch=[SLong SLat];
hm_job.n=n;
hm_job.ref_perc_time=ref_perc_time;

if strcmp(which_hough,'gfh')
    hm_job.frenh=1;
    [gridk,~]=andrew_long_transient_grid_k(TFFT,[minf maxf],[fdotmin fdotmax],dur,n);
    if length(gridk)>10000 
        gridk=shorten_gridk(gridk,sour.kn);                                 % restricts gridk to values around k of source
    end
    hm_job.gridk=gridk;
    tic;
    [hfdf,hm_job]=hfdf_hough_transients(pout,hm_job);                       % does the Generalized Hough with nonuniform k grid, uniform x grid
    toc;
                                                                            
    [cand2, job_info]=hfdf_peak_transients(hfdf,hm_job,rootkcand,0,0);      % selects candidates in every box of hough map of size rootkcand x rootkcand
    cand_orig=cand2;
    cand2=shift_pl_cand_parms(cand2,sour,ref_perc_time);                    % shifts candidate x0 / f0 to reference time of inj, which is at t=0
    if sour.n==11/3
        sour.kn=-sour.kn;                                                   % flip sign of k so that when doing coincidences with injections, things work
    end
    [found,distaway,dist_each_parm,best_cand]=quick_coins(cand2,job_info,sour,coin_dist); % coincidences between source and each candidate
    found
    
    x0=best_cand(1,1);
    t0=best_cand(9,1);
    kn=best_cand(4,1);
    f0=best_cand(14,1);
    best_cand_orig=cand_orig(:,x0==cand2(1,:));
%     fdot0=best_cand(15,1);
    fdot0=abs(kn*f0^best_cand(7,1));
    mc_found=calc_mc_with_k(abs(kn));
    
    xgrid=x_gd2(hfdf);
    kgrid=gridk;
    color_grid=y_gd2(hfdf);
    
elseif strcmp(which_hough,'fh')
    hm_job.oper='noadapt';                                                  % The Hough is nonadaptive, meaning that the antenna pattern is not considered
    kcand=20;                                                               % 20 candidates are selected from the Frequency-Hough map uniformaly in f and fdot
    hm_job.frenh=10;                                                        % Frequency-Hough uses overesolution factor of frequency to improve sensitivity
    hm_job.sd=construct_grid_sd_parms(sour.df0);                            % Creates spin-up or spin-down grid for Frequency-Hough
    [hfdf,~,~,hm_job]=fu_transients_hough(pout,hm_job,ref_perc_time);       % Does the Frequency-Hough
    [cand2, job_info]=fu_transients_hfdf_peak(hfdf,hm_job, kcand);          % selects candidate in Frequency-Hough map

 
    %%% shifts the candidate to t=0 reference time because sour is at t=0
    [ cand2 ] = shift_cand_parms( cand2,sour,ref_perc_time );               % shifts candidate f0 to reference time of inj, which is at t=0
    [ found,best_cand,false_alarm,distaway,dist,dist_each_parm_best_inj ] = fu_coins_inj(cand2,job_info,sour,coin_dist);  % coincidences between source and each candidate
    found
end

if plot_flag==1
    if strcmp(which_hough,'gfh')
        image_gd2_nonunigrid(hfdf,gridk,n)                            % plots the hough map, k vs. f, number count colored
        xlabel('frequency (Hz)'); ylabel('k (Hz^{-5/3})'); cblabel('number count')
    elseif strcmp(which_hough,'fh')
        plot(hfdf)
        xlabel('frequency (Hz)'); ylabel('spindown (Hz/s'); cblabel('number count')
    end
end

if save_flag==1
     %%%% shifting best_cand parms to appropriate reference time used in Hough map
    best_cand(1)=best_cand(1)+(sour.n-1)*sour.kn.*(-pout(1,1)+cand_orig(9,1))*86400; 
    best_cand(14)=best_cand(1)^(1/(1-n));
    cand_sour=andrew_cand2sour(best_cand,hm_job.epoch,detname);
    cand_sour.mc=mc_found;
    %%% this calculates the x and f a the reference time used in the Hough transform
    adjusted_source_x0=sour.x0+(sour.n-1)*sour.kn.*(-pout(1,1)+cand_orig(9,1))*86400;
    adjusted_source_f0=adjusted_source_x0^(1/(1-n));
    sour.lam=sour.ecl(1);
    sour.beta=sour.ecl(2);
    sour.mc=calc_mc_with_k(abs(sour.kn));
    sour.adj_x0=adjusted_source_x0;
    sour.adj_f0=adjusted_source_f0;
    sour.adj_t0=hm_job.epoch;
    sour=rmfield(sour,'ecl');
    sour.det=detname;
    
    orig_sourT=struct2table(sour);
    hm_jobT=struct2table(hm_job);
    cand_sourT=struct2table(cand_sour);
    cand_sourT=[cand_sourT table(distaway)];
    writetable(orig_sourT,strcat(outdir,'orig_sour.txt'));
    writetable(hm_jobT,strcat(outdir,'hm_job.txt'));
    writetable(cand_sourT,strcat(outdir,'cand_sour.txt'));
end


end

