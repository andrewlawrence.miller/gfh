function [cr_track,mu_noise,st_noise] = get_cr_track(p,counts_sig)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here

[~,counts_noise  ] = project_peaks_track( p,0 );
mu_noise = mean(counts_noise);
st_noise = std(counts_noise);

%%%% compute CR based on background
cr_track = (counts_sig - mu_noise) / st_noise;
end