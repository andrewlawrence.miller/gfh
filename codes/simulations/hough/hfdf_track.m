function [counts_sig] = hfdf_track(peakss,f_of_t,TFFT)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
n_of_peaks=length(peakss);
ii=find(diff(peakss(1,:)));  % find the different times
ii=[ii n_of_peaks];
nTimeSteps=length(ii); % number of times of the peakmap
ii0=1; 
counts_sig = 0;
for it = 1:nTimeSteps
    fs_at_time = peakss(2,ii0:ii(it));
    bins_off = abs(fs_at_time-f_of_t(it)) / (1 / TFFT );
    if sum(bins_off<1/2)>=1
        counts_sig = counts_sig + 1;
    end
    ii0=ii(it)+1;
end
end