function [ found,best_cand,dist_of_bestcand,dist,dist_each_parm_best_inj ] = coin_cand_inj(cand2,source,coin_dist)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

if ~exist('coin_dist','var')
    coin_dist=3;
end




theo_x0=source.x0;
theo_kn=source.kn;
theo_n=source.n;

cand_x0=cand2(1,:);
cand_kn=cand2(4,:);
cand_n=cand2(7,:);
cand_dk=cand2(8,:);
cand_dn=1;%cand2(12,:);
cand_dx=cand2(13,:);
cand_cr = cand2(6,:);


diff_x0=abs(cand_x0-theo_x0);
diff_kn=abs(cand_kn-theo_kn);
diff_n=abs(cand_n-theo_n);

term1=(diff_x0./cand_dx).^2;
term2=(diff_kn./cand_dk).^2;
term3=(diff_n./cand_dn).^2;
dist=sqrt(term1+term2+term3);

ind1 = find(dist<=coin_dist);
cand2_close_to_inj = cand2(:,ind1);
[maxcr,b] = max(cand2_close_to_inj(6,:));
best_cand = cand2_close_to_inj(:,b);

if ~isempty(best_cand)
    dist_of_bestcand = dist(ind1(b)); %%%% just distance of candidate with max cr, not smallest d
    dist_each_parm_best_inj=sqrt([term1(ind1(b)) term2(ind1(b)) term3(ind1(b))]); %x,k,n
else
    [dist_of_bestcand,ind]=min(dist);
    dist_each_parm_best_inj=sqrt([term1(ind) term2(ind) term3(ind)]); %x,k,n
    best_cand = cand2(:,ind);

end

% [mindist,ind]=min(dist);
off=coin_dist-dist;
inds=find(off>0);
if length(inds)>0
    found=true;
else
    found=false;
end




end

