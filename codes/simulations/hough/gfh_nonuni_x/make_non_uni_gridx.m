function [gridx,dx] = make_non_uni_gridx(minf0,maxf0,TFFT,n)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

pow = n - 1;
df = 1 / TFFT;
gridx = fliplr(1./(minf0:df:maxf0).^(pow));
dx = diff(gridx);
gridx = gridx(1:end-1); %% to keep same shape as dx
end