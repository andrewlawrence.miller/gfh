function [hfdf,hm_job]=hfdf_hough_transients_nonuni_xgrid(peaks,hm_job)
%hfdf_hough_transients  creates a x/k Hough map (based on 2011 version)
% This code takes as input a peakmap and first transforms t/f --> t/x
% according to the braking index, then maps t/x --> x/k using the Hough
%
%   -----------------------------INPUTS:-----------------------------------

%       peaks(3,n)          peaks of the peakmap as [t,fr,amp] (fr corrected for the Doppler effect)                    
%       hm_job              hough map structure 
%           .minf           minimum frequency of Hough map  (Hz)
%           .maxf           maximum frequency of Hough map  (Hz)
%           .df             frequency resolution (1/TFFT)   (Hz)
%           .dur            duration of peakmap             (s)
%           .patch          [Longitude Latitude] (ecliptic)
%           .n              braking index
%           .ref_perc_time  percentile of reference time    [0,1]     
%           .frenh          frequency enhancement           (1)
%           .gridk          grid on constant k parmater
%   -----------------------------OUTPUTS:-----------------------------------

%       hfdf                hough map (gd2)
%       hm_job              hough map structure, same as input with extra:
%           .epoch          reference time of Hough         (MJD)
%           .dx             spacing in x grid               (Hz^{1-n})

% Version 2.0 - October 2013 
% Part of Snag toolbox - Signal and Noise for Gravitational Antennas
% by Sergio Frasca - sergio.frasca@roma1.infn.it
% Department of Physics - Universit? "La Sapienza" - Rome

%tic;
% 
%peaks=job_pack_0.peaks;
%basic_info=job_pack_0.basic_info;

Day_inSeconds=86400;

gridk=hm_job.gridk;
braking_index=hm_job.n;

if braking_index==5 || braking_index == 3 || braking_index == 7
else
    disp('chirp, flipping spindowns to spinups')
    gridk=-gridk;
end

pow=braking_index-1;

if ~isfield(hm_job,'ref_perc_time')
    ref_perc_time=0.5;
    hm_job.ref_perc_time=ref_perc_time;
else
    ref_perc_time=hm_job.ref_perc_time;
end

if ~exist('ref_perc_time','var')
    ref_perc_time=0.5;
end


[n1,n2]=size(peaks);
peaks(4,:)=ones(1,n2);
    
epoch=quantile(peaks(1,:),ref_perc_time); %beginning, middle, or end of observing run

peaks(1,:)=peaks(1,:)-epoch;


minf0=hm_job.minf;
maxf0=hm_job.maxf;
df=hm_job.df;
enh=hm_job.frenh;


if braking_index==1 % case of pulsar winds
    xpeaks=log(peaks(2,:));
    minx=min(xpeaks);
    maxx=max(xpeaks);
    dx=df*1/maxf0;
    pow=1; %not physical, just to negate contribution of pow in each expression that follows, because in exp case, there is no 'pow'
else
    xpeaks=peaks(2,:).^-pow;
    minx=1/maxf0^pow;
    maxx=1/minf0^pow; 
    
    if maxx<minx
        temp1=minx;
        temp2=maxx;
        maxx=temp1;
        minx=temp2;
    end
    
    dx=(pow)*df*1./(peaks(2,:)).^(braking_index); %smallest possible step
    dx=abs(dx);
    opt_dx_spacing = fliplr((pow)*df*1./(minf0:df:maxf0).^(braking_index));
end

gridx = fliplr(1./(minf0:df:maxf0).^(pow));
% mindx = (pow)*df*1/(maxf0)^(braking_index);
opt_dx_spacing = diff(gridx);
% ddx=dx/enh; %refined step in x

inix=minx;
% finx=maxx;

I1000=0;
I500=I1000;


% nbin_x=ceil((abs(finx-inix))/mindx)+I1000; %was ceil()
nbin_x = length(opt_dx_spacing);
n_of_peaks=length(peaks);
ii=find(diff(peaks(1,:)));  % find the different times
ii=[ii n_of_peaks]; 
nTimeSteps=length(ii); % number of times of the peakmap


nbin_k=length(gridk);

ii0=1; %nbin_d,nbin_f0

nbin_k

nbin_x

% gridx = minx + opt_dx_spacing;

binh_df0=zeros(nbin_k,nbin_x);  %  HM matrix container
gridx = gridx(1:end-1);

oo = 0;
for it = 1:nTimeSteps
    x_lhs = xpeaks(ii0:ii(it));%./ddx(ii0:ii(it));  % normalized xs
    t_minus_t0 = peaks(1,ii0)*Day_inSeconds; % time conversion days to s

    for k = 1:nbin_k   % loop for the creation of half-differential map
        x0s = x_lhs - gridk(k) * t_minus_t0 * pow;

        num_bins_off = abs(gridx(:)-x0s(:).') ./opt_dx_spacing(:) ;
        number = sum(num_bins_off < 1,2);
        if ~isempty(find(number>1,1))
%             disp(['at one time, and one k bin, more than 1 x0 falls into ' ...
%                 'the same gridx bin, correcting...']) %%%% this really only happens at 2-8 Hz freqs
            number(number>1) = 1;
            oo = oo + 1;
        end
        binh_df0(k,:) = binh_df0(k,:) + number';

    end
    ii0=ii(it)+1;

end
disp('number of times that more than 1 x0 fell into same gridx bin:')
oo

hfdf=gd2(binh_df0.'); 

% hfdf=edit_gd2(hfdf,'dx',ddx,'ini',inix-ddx*I500,'capt','Histogram of y0-x0'); %inix-I500*ddf
hfdf = edit_gd2(hfdf,'x',gridx,'x2',gridk);
hm_job.epoch=epoch;
hm_job.dx=dx;
hm_job.gridx = gridx;
hm_job.dx = opt_dx_spacing;

end