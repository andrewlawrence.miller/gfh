function [hfdf,hm_job] = sum_uni_hfdf_to_get_nonunix_hfdf...
    (hfdf_orig,gridx_optimal,dx_optimal,gridx_used,gridk)
%UNTITLED15 Summary of this function goes here
%   Detailed explanation goes here
hm = y_gd2(hfdf_orig);
[~,numks] = size(hm);
hm_new = zeros(length(gridx_optimal),numks);
for k = 1:numks
    for x = 1:length(gridx_optimal)
        bins_off = abs(gridx_used-gridx_optimal(x)) / dx_optimal(x);
        hm_new(x,k) = sum(hm(bins_off<=1,k));
    end
end

hfdf = edit_gd2(hfdf_orig,'x',gridx_optimal,'x2',gridk,'y',hm_new);
% hm_job.epoch=epoch;
hm_job.gridx = gridx_optimal;
hm_job.dx = dx_optimal;
