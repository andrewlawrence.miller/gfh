function [ gridsd_parms ] = construct_grid_sd_parms( df0,fact_around )
%UNTITLED26 Summary of this function goes here
%   Detailed explanation goes here
if ~exist('fact_around','var')
    fact_around=10;
end

sdstep=abs(df0/fact_around);
if this_n==11/3
    dmin=abs(df0)/fact_around;
    dmax=abs(df0)*fact_around;
    num_steps=(dmax-dmin)/sdstep;
else
    dmin=-sdstep*fact_around;
    dmax=sdstep*fact_around;
    num_steps=(dmax-dmin)/sdstep;
end
gridsd_parms=[dmin sdstep num_steps];
end

