function [ts_to_query,fsss] = mini_emri_get_f_of_t(sour,maxf,p,t00)
%UNTITLED6 gets mini emri f(t) resolution to match that of the peakmap, so
%each time step is TFFT/2 in the resulting fsss capito?
[full_mini_ts,full_mini_fs] = mini_emri_load_tf(sour.m1);

%%% index signal model by time and freq ranges of peakmap and injection
tt = ( unique(p(1,:))-gps2mjd(t00) ) * 86400;
fs_full_range = full_mini_fs(full_mini_fs>=sour.f0 & full_mini_fs<=maxf);
ts_full_range = full_mini_ts(full_mini_fs>=sour.f0 & full_mini_fs<=maxf);
ts_to_query = max(ts_full_range)-tt;
fsss = index_noise_curve(ts_to_query,ts_full_range,fs_full_range);

end