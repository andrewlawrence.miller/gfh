function run_example_multi_sigs_simulations...
    (Ninj, Nsim, fmin, fmax, Mc, white_noise, amp, sig_type,outdir, where_am_i)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if isdeployed                                                               % if running compiled version of code on Louvain or CNAF
    disp('job parms:')
    Ninj = str2num(Ninj)
    Nsim = str2num(Nsim)
    fmin = str2num(fmin)
    fmax = str2num(fmax)
    Mc = str2num(Mc)
%     v0=str2num(v0)
    white_noise=str2num(white_noise)
    amp = str2num(amp)
%     run
    sig_type
    where_am_i
    outdir
end

% t00 = 1.238738194000000e+09;
t00 = 1238777856+12000;
ref_perc_time = 0;
% Nsim = 20;
% Ninj = [1:10 20:10:100];
% fmin = 4;
% fmax = 7;
% Mc = 1.15;
% white_noise = 1;
% amp = [1e-24 1e-23 1e-22];
plot_flag = 0;
% % for l = 1:length(amp)
%     for j = 1:length(Ninj)
        allfound = 0;
        for k = 1:Nsim
            [found{k},distaway{k},best_cand{k},sour{k},cr_track{k}] = example_multi_sigs(...
                Ninj,fmin,fmax,Mc,white_noise,amp,sig_type,'gfh',plot_flag,...
                where_am_i,ref_perc_time,t00);
            allfound = allfound + found{k};
        end
        eff = sum(allfound) / (Ninj * Nsim);
%     end
% end

str_fmin = sprintf( '%04d', fmin );
str_fmax = sprintf( '%04d', fmax );
str_Ninj = sprintf('%03d', Ninj );
str_Nsim = sprintf('%03d', Nsim );
str_Mc = num2str(Mc);
str_amp = num2str(log10(amp));

fname=strcat('Ninj',str_Ninj,'_',str_Nsim,'_',str_amp,'_',str_fmin,'_',str_fmax,'-',str_Mc,'_.mat'); 
save(fname,'eff','found','allfound','distaway','best_cand','sour','t00','cr_track',...
    'Ninj','Nsim','fmin','fmax','Mc','white_noise','amp','outdir','where_am_i')
disp('saved file, now copying....')
copy_file(fname,outdir,0,where_am_i);


end