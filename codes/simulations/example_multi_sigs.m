% function [p_times,p_freqs,p_eq_ps,xgrid,kgrid,color_grid,found] = example_multi_sigs...
function [found,distaway,best_cand,sour,cr_track] = example_multi_sigs...
    (N, minf,maxf,mc,white_noise,amp,sig_type,which_hough,plot_flag,where_am_i,ref_perc_time,t00 )
%example 
% The code creates a source, simulates it, creates a peakmap, runs the
% Hough, selects candidates, and does coincidences with injection and
% candidates. This is used to create the plots in the paper with minf=140,
% maxf=150, mc=1e-3, amp=1e-22 (plots= peakmap, Hough map)

%   -----------------------------INPUTS:-----------------------------------
%   minf:           minimum frequency                                  (Hz)
%   maxf:           maximum frequency                                  (Hz)
%   mc:             chirp mass                                       (Msun)
%   amp:            amplitude of signal
%   detname:        detector ('Livingston','Hanford','Virgo')
%   which_hough:    do either 'fh' or 'gfh'
%   plot_flag:      do plots (=1), else no
%   save_flag:      save table of parms (=1), else no
%   outdir:         output directory
%   where_am_i:     cluster on which you run ('laptop','louvain','cnaf')

%   -----------------------------OUTPUTS:----------------------------------

%   NONE

%   -----------------------------EXAMPLE:----------------------------------

% example_multi_sigs(1,150,160,1e-3,1,1e-22,'power_law','gfh')
% example_multi_sigs(1,20,30,100,1,1e-22,'mini_emri','track')

% reset(RandStream.getGlobalStream,sum(100*clock));
% rng(sum(100*clock))
CR_thr = 5;
cr_track =[];

if ~exist('N','var')
    N = 1;
end

if ~exist('minf','var')
    minf=140';
end
if ~exist('maxf','var')
    maxf=150;
end
if ~exist('mc','var')
    mc=1e-3;
end
if ~exist('amp','var')
    amp=1e-22;
end

if ~exist('sig_type','var')
    sig_type='power_law';
end

if ~exist('which_hough','var')
    which_hough='gfh_nonuni';
end

if ~exist('plot_flag','var')
    plot_flag=1;
end

if ~exist('save_flag','var')
    save_flag=1;
end

if ~exist('white_noise','var')
    white_noise = 0;
end

if ~exist('outdir','var')
   outdir='~/Desktop/Belgio/dark_matter/pbh_cw/paper/pbh-cw/parameters/';
end

if ~exist('where_am_i','var')
    where_am_i='laptop';
end

if strcmp(sig_type,'mini_emri')
    m1 = mc; %%% in the case of mini-EMRI, mc as input is the chirp mass
    q = 1e-5;
    m2 = m1 * q;
    mc = (m1 * m2)^(3/5) / (m1+m2)^(1/5);
    test_flag = 0;
    [n,k_onefit, rsq_onefit] = mini_emri_fit_unk_pl(minf,maxf,m1,test_flag)
elseif strcmp(sig_type,'power_law')
    n = 11/3
end
n_pn0 = 11/3;

fap = 0.001;

% N=1;                                                                        % number of injections per frequency band to do (set to 1)
if strcmp(where_am_i,'louvain')
    sfdbdir='/nfs/user/gw/H/sfdb/O3/C00/NC/2048/';
elseif strcmp(where_am_i,'laptop') %% my laptop
    sfdbdir='~/Desktop/sfdbs/H/'; % directory where SFDBs are
elseif strcmp(where_am_i,'nikhef')
    det = 'H';
    run = 'O3';
    det_run_string=[det '/sfdb/' run '/C01/GAT_CL60/'];
    basedir = '/dcache/gravwav/amiller/';
    f_str = '2048';
    sfdbdir=strcat(basedir,det_run_string,f_str,'/') ;  
elseif strcmp(where_am_i,'google')
    sfdbdir = './';
end

if ~exist('ref_perc_time','var')
    ref_perc_time = 0.5;
end

if ~exist('t00','var')
    t00=1238777856+12500;                                                       % start time of injection
end
coin_dist=3;                                                                % distance in bins allowed between injection parms and cand parms
% rootkcand=10;                                                                % sqrt(num of candidates to select)
% ref_perc_time=0;                                                            % reference time to use when constructing hough map: f0 is at t0

if N>1
    fdotmin=calc_fdot_chirp(mc,minf+1/N);                                           % fdot at minf and maxf
else
    fdotmin=calc_fdot_chirp(mc,minf);
end
fdotmax=calc_fdot_chirp(mc,maxf);
% TFFT=2^floor(log2(TFFTmax))                                                 % will work for TFFT not a factor of 2

% t1=calc_time_to_coalescence(mc,minf+1/N);                                       % seconds until coalescence at min and max frequency injected
% t2=calc_time_to_coalescence(mc,maxf);


t0=gps2mjd(t00);                                                            % converts t00 (GPS time) to MJD time

if strcmp(sig_type,'mini_emri')
    [fs,Cf] = mini_emri_get_Ch_Cf(m1);
    [~,bb] = min(abs(fs-minf));
    [~,cc] = min(abs(fs-maxf));
    fdotmin = fdotmin * Cf(bb);
    fdotmax = fdotmax * Cf(cc);
end

sour=gen_N_power_law_sigs(N,minf,minf,fdotmin,fdotmin,n,n,amp,t0);          % creates source structure, for chirp sigs, sig's f0 alwyas at minf
% sour=generate_truly_random_pl_sig(N,minf,maxf,fdotmin,fdotmin,n,n,amp,t0);
% theo_mc = calc_mc_with_k(-sour.kn / Cf(fs == sour.f0) * sour.f0^sour.n/sour.f0^(n_pn0));
                                                         % signal duration to simulate (s)

if strcmp(sig_type,'mini_emri')
    sour.m1 = m1;
    sour.spin = 0;
    sour.q = 1e-5;

    [full_mini_ts,full_mini_fs] = mini_emri_load_tf(m1);
    times_of_mini_emri = full_mini_ts(full_mini_fs>=minf & full_mini_fs<=maxf);
    dur = max(times_of_mini_emri-min(times_of_mini_emri))

else
    t1=calc_time_to_coalescence(mc,min([sour(:).f0]));      % seconds until coalescence at min and max frequency injected
    t2=calc_time_to_coalescence(mc,maxf);           

    dur=floor(t1-t2);
end

TFFTmax = floor(1 / sqrt(fdotmax));                                             % calculate TFFT using maximum spinup

if TFFTmax == 0
    TFFTmax = 1;
end

TFFT = 2^floor(log2(rndeven(1/sqrt(max([sour.df0])))));
if strcmp(sig_type,'mini_emri')
    TFFT = TFFTmax;
end

% ampnoise=7.94E-04 * 1e-20;
% these_ts = 0:TFFT/2:dur;
% [simamps, fsss]=pl_gen_fs_amps(these_ts,sour);
% cbc_calc_mf_snr(fsss,ampnoise^2,sour.mc,sour.h0)

job_pack_0=inject_power_law_signal...
    (sour,[minf maxf],t00,dur,TFFT,sfdbdir,sig_type,white_noise);           % does the injection of sour in freq band, makes peakmap with TFFT;
% job_pack_0 contains the peakmap
basic_info=job_pack_0.basic_info;                                           % structure that contains basic information about the peakmap    


% [SLong, SLat]=astro_coord('equ','ecl',sour.a,sour.d);                       % conversation of equatorial to ecliptic sky coordinates

p=job_pack_0.peaks;                                                         % the peaks in time/frequency, with corresponding amps

if plot_flag==1
    plot_triplets(86400*(p(1,:)-p(1,1)),p(2,:),p(3,:))
    xlabel('time (s)'); ylabel('frequency (Hz)'); 
    if ismatlab
        cblabel('equalized spectrum')
    end
    set(gca,'FontSize',14)
end
pout=p;%andrew_hfdf_patch(p,basic_info,[SLong SLat]);                          % Doppler correction

hm_job.minf=minf;                                                           % minimum frequency to do the Hough on
hm_job.maxf=maxf;                                                           % maximum frequency to do the Hough on
hm_job.df=1/TFFT;                                                           % step in frequency
hm_job.dur=dur;
try
    hm_job.patch=[SLong SLat];
catch
    hm_job.patch = [0 0];
end
hm_job.n=n;
hm_job.ref_perc_time=ref_perc_time;

if strcmp(which_hough,'gfh') || strcmp(which_hough,'gfh_nonuni')
    hm_job.frenh=1;
    fdotmin_for_k = calc_fdot_chirp(mc,minf);
    [gridk,~]=andrew_long_transient_grid_k(TFFT,[minf maxf],[fdotmin_for_k fdotmax],dur,n);

    if n == 11/3 || strcmp(sig_type,'mini_emri')
        kss_inj = [sour.kn]; mink = min(kss_inj); maxk = max(kss_inj);
        gridk = cbc_shorten_gridk(gridk,mink,maxk);
    end
    if length(gridk)>10000 
        gridk=shorten_gridk(gridk,sour.kn);                                 % restricts gridk to values around k of source
    end

    hm_job.gridk=gridk;
    
    if strcmp(which_hough,'gfh')
        tic; [hfdf,hm_job]=hfdf_hough_transients(pout,hm_job);  toc;            % does the Generalized Hough with nonuniform k grid, uniform x grid
%         hfdf_summed_over = sum_uni_hfdf_to_get_nonunix_hfdf...            %%%      this actually works for 2-8 Hz
%           (hfdf3,hm_job.gridx,hm_job.dx,hm_job3.gridx,gridk)
        
        Nparmspace = numel(y_gd2(hfdf));
        rootkcand = ceil(sqrt(fap * Nparmspace));                                          % sqrt(num of candidates to select)                                                                        
        [cand2, job_info] =hfdf_peak_transients(hfdf,hm_job,rootkcand,0,0);      % selects candidates in every box of hough map of size rootkcand x rootkcand
    else
        tic;[hfdf,hm_job]=hfdf_hough_transients_nonuni_xgrid(pout,hm_job);toc;

        Nparmspace = numel(y_gd2(hfdf));
        rootkcand = ceil(sqrt(fap * Nparmspace));  
        [cand2, job_info] = hfdf_peak_transients_nonuni_xgrid(hfdf,hm_job,rootkcand,0,0);
    end 
    hm_map = y_gd2(hfdf);
    mu_noise = mean(mean(hm_map));
    std_dev = mean(max(hm_map)); %%%% max based on what's done in hfdf_peak_transients
    cand_orig=cand2;
    
    for ss = 1:length(sour)
        
        x0_sour_shifted = shift_x0_by_time(sour(ss).x0,-sour(ss).kn,...
            -(sour(ss).fepoch-hm_job.epoch)*86400,sour(ss).n);      %%% the minus sign shifts "forward in time.. to higher frequencies/lower x
 
        sour_shifted(ss) = sour(ss);
        sour_shifted(ss).x0 = x0_sour_shifted;
        sour_shifted(ss).f0 = get_f0_from_x0(x0_sour_shifted,n);
        if sour(ss).n==5 || sour(ss).n == 3 || sour(ss).n == 7
        else
            sour(ss).kn=-sour(ss).kn;                                                   % flip sign of k so that when doing coincidences with injections, things work
            sour_shifted(ss).kn = -sour_shifted(ss).kn;
        end

        if strcmp(which_hough,'gfh')
            [found(ss),distaway(ss),dist_each_parm(ss,:),best_cand(:,ss)]=quick_coins(cand2,job_info,sour_shifted(ss),coin_dist); % coincidences between source and each candidate
        elseif strcmp(which_hough,'gfh_nonuni') 
            %%% if multiple cands within coin_dist of inj, this function
            %%% takes the one with the maximum CR
            [ found(ss),best_cand(:,ss),distaway(ss),~,dist_each_parm ]...
                = coin_cand_inj(cand2,sour_shifted(ss),coin_dist);
        end
        if best_cand(6,ss)<CR_thr
            found(ss) = 0;
        end

        if strcmp(sig_type,'mini_emri')
            [~,f_of_t] = mini_emri_get_f_of_t(sour,maxf,p,t00);
            counts_sig = hfdf_track(p,f_of_t,TFFT);
            [cr_track,~,~] = get_cr_track(p,counts_sig);
            
        end
    end
    
    
elseif strcmp(which_hough,'fh')
    hm_job.oper='noadapt';                                                  % The Hough is nonadaptive, meaning that the antenna pattern is not considered
    kcand=20;                                                               % 20 candidates are selected from the Frequency-Hough map uniformaly in f and fdot
    hm_job.frenh=10;                                                        % Frequency-Hough uses overesolution factor of frequency to improve sensitivity
    hm_job.sd=construct_grid_sd_parms(sour.df0);                            % Creates spin-up or spin-down grid for Frequency-Hough
    [hfdf,~,~,hm_job]=fu_transients_hough(pout,hm_job,ref_perc_time);       % Does the Frequency-Hough
    [cand2, job_info]=fu_transients_hfdf_peak(hfdf,hm_job, kcand);          % selects candidate in Frequency-Hough map

    %%% shifts the candidate to t=0 reference time because sour is at t=0
    [ cand2 ] = shift_cand_parms( cand2,sour,ref_perc_time );               % shifts candidate f0 to reference time of inj, which is at t=0
    [ found,best_cand,false_alarm,distaway,dist,dist_each_parm_best_inj ] = fu_coins_inj(cand2,job_info,sour,coin_dist);  % coincidences between source and each candidate
%     found

elseif strcmp(which_hough,'track')
  
    [~,f_of_t] = mini_emri_get_f_of_t(sour,maxf,p,t00); 
    counts_sig = hfdf_track(p,f_of_t,TFFT);
    [cr_track,~,~] = get_cr_track(p,counts_sig);
    if cr_track>=CR_thr
        found = true;
    else
        found = false;
    end
    distaway = []; best_cand = [];
end
if strcmp(which_hough,'gfh') || strcmp(which_hough,'gfh_nonuni') || strcmp(which_hough,'fh')
    if plot_flag==1
        if strcmp(which_hough,'gfh') || strcmp(which_hough,'gfh_nonuni') %%% it's better to use x and k to plot
            image_gd2_nonunigrid(hfdf,gridk)%,calc_mc_with_k(gridk),n)                            % plots the hough map, k vs. f, number count colored
    %         xlabel('frequency (Hz)'); ylabel('chirp mass (M_{sun})'); cblabel('number count')
        elseif strcmp(which_hough,'fh')
            plot(hfdf)
            xlabel('frequency (Hz)'); ylabel('spindown (Hz/s'); cblabel('number count')
        end
    end
    if strcmp(sig_type,'mini_emri')
        theo_mc = calc_mc_with_k(-sour.kn / Cf(fs == sour.f0) * sour.f0^sour.n/sour.f0^(n_pn0));
    % recovered_f0 = best_cand(14)
    end
    
    x0_on_hm_at_ref_time = shift_x0_by_time([sour.x0],[sour.kn],-([sour.fepoch]-hm_job.epoch)*86400,[sour.n])%%% the minus sign shifts "forward in time.. to higher frequencies/lower x
    num_count_best_cand = best_cand(5,:)
    cr_best_cand = best_cand(6,:)
    x0_best_cand = best_cand(1,:)
    kn_best_cand = best_cand(4,:)
    if strcmp(sig_type,'mini_emri')
        best_cand(10) = max(86400*(p(1,:)-p(1,1))); %%% for now, because mini emri simulation not sampled enough
    end                                             %%% and inject nans --> need to shorten duration a bit
    found
    distaway
    dist_each_parm
    
    if plot_flag == 1
       hold on; plot(x0_on_hm_at_ref_time,-[sour.kn],'g.','MarkerSize',10)
       if ismatlab
            yline(-[sour.kn],'g--'); xline(x0_on_hm_at_ref_time,'g--')
       end
       for sss = 1:length(sour)
            oneind = num_count_best_cand(sss)==cand2(5,:) & cr_best_cand ==cand2(6,:);
            cand_x0_on_hm(sss) = cand_orig(1,oneind);
            hold on; plot(cand_x0_on_hm(sss),-kn_best_cand(sss),'bo','MarkerSize',10)
            
            if ismatlab
                yline(-kn_best_cand(sss),'b--'); xline(cand_x0_on_hm(sss),'b--')
            end
       end
    end
    
    % max_Npeaks = 2 * floor(dur / TFFT); %% I think it's the max..
    
    frac_tot_npeaks_of_bestcand = num_count_best_cand / basic_info.ntim;
    
    % calc_CR_nc_theo()
    
    best_cand(16) = mu_noise;
    best_cand(17) = std_dev;
end
end

    % For BSDs in GFH PBH search, I guess
    % if tfft_db<=TFFT || (maxf-minf)<=bw                                           %% if true, use BSDs
    %     disp('using BSDs')
    %     data_struct='bsd';
    %
    %     if dur<=tobs
    %         tini=gps2mjd(t00);
    %         tfin=gps2mjd(t00+dur);
    %         this_bsd=bsd_lego(basedir,det_for_bsd,run,[tini tfin],[minf maxf],2);
    %         this_bsd=cut_bsd(this_bsd,[tini tfin]);
    %     else
    %         this_bsd=bsd_lego(basedir,det_for_bsd,run,1,[minf maxf],2);
    %     end
    %
    %     dt=dx_gd(this_bsd);
    %     lfft=rndeven(TFFT/dt);
    %     tfstr=bsd_peakmap(this_bsd,lfft);
    %
    %
    %     save(fname,'tfstr','data_struct','minf','maxf','t00','dur','TFFT','Mc','det','run','outdir','where_am_i')
    % else
