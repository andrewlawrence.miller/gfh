function [outputArg1,outputArg2] = pbh_determine_search_parmspace(inputArg1,inputArg2)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

T=readtable('./paper/pbh-cw/federico/1e-5_1e-3/sensitivity/data_linspace_cluster.txt');
cc=constants;
Mc=T.M_C;
f0=T.f0;
TFFT=T.T_FFT_best;
tcoal=T.t_coal;
t_fdotbest=T.t_f_dot_best;
fdotbest=T.f_dot_best;

time_till_coal_after_obs=tcoal-t_fdotbest; % seconds

f_fin=calc_fgw_as_coalescing(Mc,time_till_coal_after_obs);

run='O3L';
n=11/3;

fdot_min=calc_fdot_chirp(Mc,f0);
fdot_max=calc_fdot_chirp(Mc,f_fin);

for i=1:length(Mc)
    fmin=f0(i);
    fmax=f_fin(i);
    tobs=time_till_coal_after_obs(i);
    mc=Mc(i);
    tfft=TFFT(i);
    tobs=t_fdotbest(i);
    [hmin(i),dmax(i)]=power_law_sens(fmin,mc,tfft,tobs,n,run);


end

skypts=ceil(4*pi*calc_ND(f0,TFFT).^2);

skyres=2*pi./sqrt(skypts);

deg_skyres=skyres*180/pi;
%size_GC=1;
%N_sky_for_GC=skyres/size_GC;

dmax_kpc=dmax/cc.units.kpc_to_m;
dmin=1;% kpc ; % assuming a factor of 2 increase in sensitivity

points_to_search=dmax_kpc>dmin;

figure;scatter(f0(points_to_search),Mc(points_to_search),10,dmax(points_to_search)/cc.units.kpc_to_m','o','filled'); colormap('parula'); colorbar; hold on;
xlabel('frequency (Hz)'); ylabel('chirp mass (Msun)'); cblabel('distance (kpc)'); set(gca,'FontSize',18)
set(gca,'YScale','log');  set(gca,'ColorScale','log')


figure;scatter(f0(points_to_search),Mc(points_to_search),10,fdotbest(points_to_search)','o','filled'); colormap('winter'); colorbar; hold on;
xlabel('frequency (Hz)'); ylabel('chirp mass (Msun)'); cblabel('f dot (Hz/s)'); set(gca,'FontSize',18)
set(gca,'YScale','log');  set(gca,'ColorScale','log');  set(gca,'XScale','log')



figure;scatter(f0(points_to_search),Mc(points_to_search),10,t_fdotbest(points_to_search)','o','filled'); colormap('winter'); colorbar; hold on;
xlabel('frequency (Hz)'); ylabel('chirp mass (Msun)'); cblabel('T_{obs} (s)'); set(gca,'FontSize',18)
set(gca,'YScale','log');  set(gca,'ColorScale','log'); set(gca,'XScale','log')

figure;scatter(f0(points_to_search),Mc(points_to_search),10,skypts(points_to_search)','o','filled'); colormap('winter'); colorbar; hold on;
xlabel('frequency (Hz)'); ylabel('chirp mass (Msun)'); cblabel('number of sky points'); set(gca,'FontSize',18)
set(gca,'YScale','log'); set(gca,'XScale','log'); set(gca,'ColorScale','log')

figure;scatter(f0(points_to_search),Mc(points_to_search),10,TFFT(points_to_search)','o','filled'); colormap('winter'); colorbar; hold on;
xlabel('frequency (Hz)'); ylabel('chirp mass (Msun)'); cblabel('T_{FFT} (s)'); set(gca,'FontSize',18)
set(gca,'YScale','log'); set(gca,'XScale','log'); set(gca,'ColorScale','log')



% figure;scatter(f0(skypts>1),Mc(skypts>1),10,N_sky_for_GC','o','filled'); colormap('parula'); colorbar; hold on;
% xlabel('frequency (Hz)'); ylabel('chirp mass (Msun)'); cblabel('number of sky points'); set(gca,'FontSize',18)
% set(gca,'YScale','log'); set(gca,'XScale','log');set(gca,'ColorScale','log')

end